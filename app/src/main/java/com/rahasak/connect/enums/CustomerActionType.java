package com.rahasak.connect.enums;

public enum CustomerActionType {
    NEW_CHEQUE,
    CUSTOMER_LIST,
    NEW_MESSAGE,
}
