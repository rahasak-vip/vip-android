package com.rahasak.connect.util;

import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Bank;
import com.rahasak.connect.pojo.BankAccount;
import com.rahasak.connect.pojo.BankAccountResponse;
import com.rahasak.connect.pojo.Branch;
import com.rahasak.connect.pojo.ConnectNotificationMessage;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.pojo.NotificationMessage;
import com.rahasak.connect.pojo.Qualification;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.pojo.TokenReply;
import com.rahasak.connect.pojo.Transaction;
import com.rahasak.connect.pojo.TransactionResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class JsonUtil {

    public static String toJsonContract(HashMap<String, Object> contractMap) throws JSONException {
        JSONObject jsonParam = new JSONObject(contractMap);
        return jsonParam.toString();
    }

    public static String toOrderedString(HashMap<String, Object> contractMap) {
        JSONObject jsonParam = new JSONObject(contractMap);
        char[] tmp = jsonParam.toString()
                .replaceAll("[\\[\\](){}]", "")
                .replaceAll("\\/", "")
                .replaceAll("\\\\", "")
                .replaceAll("\n", "")
                .replaceAll("\r", "")
                .replaceAll(",", "")
                .replaceAll(":", "")
                .replaceAll(" ", "")
                .replaceAll("\"", "")
                .toLowerCase()
                .toCharArray();
        Arrays.sort(tmp);

        return new String(tmp);
    }

    public static StatusReply toStatusReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            StatusReply statusReply = new StatusReply();
            statusReply.setCode(jsonObj.getInt("code"));
            statusReply.setMsg(jsonObj.getString("msg"));

            return statusReply;
        }

        throw new JSONException("Invalid JSON");
    }

    public static TokenReply toTokenReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            TokenReply tokenReply = new TokenReply();
            tokenReply.setStatus(jsonObj.getInt("status"));
            tokenReply.setToken(jsonObj.getString("token"));

            return tokenReply;
        }

        throw new JSONException("Invalid JSON");
    }

    public static NotificationMessage toNotificationMessage(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            NotificationMessage msg = new NotificationMessage();
            msg.setPromizeId(jsonObj.getString("promizeId"));
            msg.setPromizeUser(jsonObj.getString("promizeUser"));
            msg.setPromizeAmount(jsonObj.getString("promizeAmount"));
            msg.setPromizeSalt(jsonObj.getString("promizeSalt"));
            msg.setPromizeStatus(jsonObj.getString("promizeStatus"));

            return msg;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ConnectNotificationMessage toConnectNotificationMessage(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            ConnectNotificationMessage msg = new ConnectNotificationMessage();
            msg.setTraceId(jsonObj.getString("traceId"));
            msg.setTracerDid(jsonObj.getString("tracerDid"));
            msg.setTracerOwnerDid(jsonObj.getString("tracerOwnerDid"));
            msg.setTracerName(jsonObj.getString("tracerName"));
            msg.setSalt(jsonObj.getString("salt"));

            return msg;
        }

        throw new JSONException("Invalid JSON");
    }

    public static Account toAccountReply(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            Account account = new Account();
            account.setId(jsonObj.getString("id"));
            account.setName(jsonObj.getString("name"));
            account.setNic(jsonObj.getString("nic"));
            account.setNo(jsonObj.getString("no"));
            account.setPhone(jsonObj.getString("phone"));

            return account;
        }

        throw new JSONException("Invalid JSON");
    }

    public static TransactionResponse toTransactionResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("transactions");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                transaction.setDate(obj.getString("date"));
                transaction.setDescription(obj.getString("description"));
                transaction.setAmount(obj.getString("amount"));

                transactions.add(transaction);
            }

            return new TransactionResponse(balance, transactions);
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toPromizeResponse(String jsonStr, String acc) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("promizes");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String from = obj.getString("promizeFrom");
                String to = obj.getString("promizeTo");
                String fromAcc = obj.getString("fromAcc");
                String toAcc = obj.getString("toAcc");
                String fromName = obj.getString("promizeFromName");
                String toName = obj.getString("promizeToName");
                String typ = obj.getString("typ");
                String time = obj.getString("timestamp");
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setId(id);
                transaction.setAmount(obj.getString("amount"));
                transaction.setStatus(obj.getString("status"));

                if (typ.equalsIgnoreCase("PAY")) {
                    transaction.setUser(toName);
                    transaction.setAccount(toAcc);
                    transaction.setFrom(fromAcc);
                    transaction.setType("Pay");
                    transaction.setDescription("Payment");
                } else {
                    if (from.equalsIgnoreCase(acc)) {
                        // sent
                        transaction.setUser(toName);
                        transaction.setAccount(toAcc);
                        transaction.setType("Sent");
                        transaction.setFrom(fromAcc);
                        transaction.setDescription("Sent Promize");
                    } else {
                        // received
                        transaction.setUser(fromName);
                        transaction.setAccount(fromAcc);
                        transaction.setType("Received");
                        transaction.setFrom(toAcc);
                        transaction.setDescription("Received Promize");
                    }
                }

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toTransferResponse(String jsonStr, String acc) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("transfers");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String from = obj.getString("fromAcc");
                String to = obj.getString("toAcc");
                String toName = obj.getString("toName");
                String time = obj.getString("timestamp");
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setId(id);
                transaction.setFrom(from);
                transaction.setAccount(to);
                transaction.setUser(toName);
                transaction.setAmount(obj.getString("amount"));
                transaction.setStatus(obj.getString("status"));
                transaction.setBank(obj.getString("toBank"));
                transaction.setBranch(obj.getString("toBranch"));

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }

    public static BankAccountResponse toAccountResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray accountsArr = jsonObject.getJSONArray("accounts");

            ArrayList<String> promizes = new ArrayList<>();
            JSONArray promizeArr = jsonObject.getJSONArray("promizes");
            for (int i = promizeArr.length() - 1; i >= 0; i--) {
                promizes.add(promizeArr.getString(i));
            }

            ArrayList<String> transfers = new ArrayList<>();
            JSONArray transferArr = jsonObject.getJSONArray("transfers");
            for (int i = transferArr.length() - 1; i >= 0; i--) {
                transfers.add(transferArr.getString(i));
            }

            ArrayList<BankAccount> accounts = new ArrayList<>();
            for (int i = 0; i < accountsArr.length(); i++) {
                JSONObject obj = accountsArr.getJSONObject(i);
                String no = obj.getString("no");
                String name = obj.getString("name");
                String typ = obj.getString("typ");
                String abal = obj.getString("availableBalance");
                String cbal = obj.getString("currentBalance");
                boolean isPromizeAccount = obj.getBoolean("isPromizeAccount");

                BankAccount account = new BankAccount();
                account.setNo(no);
                account.setName(name);
                account.setTyp(typ);
                account.setAvailableBalance(abal);
                account.setCurrentBalance(cbal);
                accounts.add(account);
                account.setPromizeAccount(isPromizeAccount);
            }

            BankAccountResponse response = new BankAccountResponse();
            response.setAccounts(accounts);
            response.setPromizes(promizes);
            response.setTransfers(transfers);

            return response;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Bank> toBankList(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray banksArr = jsonObject.getJSONArray("OUTRESPONSEDATA");

            ArrayList<Bank> banks = new ArrayList<>();
            for (int i = 0; i < banksArr.length(); i++) {
                JSONObject obj = banksArr.getJSONObject(i);
                String code = obj.getString("OUTBANKCODE");
                String name = obj.getString("OUTBANKNAME");

                // add non empty banks
                if (code != null && name != null && !code.equalsIgnoreCase("") && !name.equalsIgnoreCase("")) {
                    Bank bank = new Bank(code, name);
                    banks.add(bank);
                }
            }

            return banks;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Branch> toBranchList(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            JSONArray banksArr = jsonObject.getJSONArray("OUTRESPONSEDATA");

            ArrayList<Branch> branches = new ArrayList<>();
            for (int i = 0; i < banksArr.length(); i++) {
                JSONObject obj = banksArr.getJSONObject(i);
                String code = obj.getString("OUTBRANCHCODE");
                String name = obj.getString("OUTBRANCHNAME");

                // add non empty banks
                if (code != null && name != null && !code.equalsIgnoreCase("") && !name.equalsIgnoreCase("")) {
                    String[] br = name.split("/");
                    Branch branch = new Branch(br.length > 1 ? br[1] : br[0], code);
                    branches.add(branch);
                }
            }

            return branches;
        }

        throw new JSONException("Invalid JSON");
    }

    public static Map toProvinces(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONArray jsonArr = new JSONArray(jsonStr);
            Map provinces = new HashMap<String, ArrayList<String>>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                String province = obj.getString("province");
                ArrayList districts = new ArrayList<String>();
                JSONArray jsonArr1 = obj.getJSONArray("districts");
                for (int j = 0; j < jsonArr1.length(); j++) {
                    districts.add(jsonArr1.getString(j));
                }

                provinces.put(province, districts);
            }

            return provinces;
        }

        throw new JSONException("Invalid JSON");
    }

    public static Identity toIdentityResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            Identity identity = new Identity();
            identity.setDid(jsonObj.getString("did"));
            identity.setOwner(jsonObj.getString("owner"));
            identity.setNic(jsonObj.getString("nic"));
            identity.setName(jsonObj.getString("name"));
            identity.setDob(jsonObj.getString("dob"));
            identity.setPhone(jsonObj.getString("phone"));
            identity.setEmail(jsonObj.getString("email"));
            identity.setProvince(jsonObj.getString("province"));
            identity.setDistrict(jsonObj.getString("district"));
            identity.setAddress(jsonObj.getString("address"));
            identity.setBlob(jsonObj.getString("blob"));
            identity.setVerified(jsonObj.getBoolean("activated"));

            return identity;
        }

        throw new JSONException("Invalid JSON");
    }

    public static Qualification toQualificationResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObj = new JSONObject(jsonStr);

            Qualification qualification = new Qualification();
            qualification.setId(jsonObj.getString("id"));
            qualification.setHolder(jsonObj.getString("holder"));

            JSONObject ol = jsonObj.getJSONObject("olResult");
            qualification.setOlMethamatics(ol.getString("olMathematics"));
            qualification.setOlEnglishg(ol.getString("olEnglish"));
            qualification.setOlScience(ol.getString("olScience"));
            qualification.setOlTamil(ol.getString("olReligion"));
            //qualification.setOlHealthScience(ol.getString("olHealthScience"));

            JSONObject eq = jsonObj.getJSONObject("educationalQualification");
            qualification.setHasDiploma(eq.getBoolean("hasDiploma"));
            qualification.setNameDiploma(eq.getString("nameDiploma"));

            JSONObject pq = jsonObj.getJSONObject("professionalQualification");
            qualification.setHasExperience(pq.getBoolean("hasExperience"));
            qualification.setNameExperience(pq.getString("nameExperience"));
            qualification.setDurationExperience(pq.getString("durationExperience"));
            qualification.setProfessionArea(pq.getString("professionArea"));
            qualification.setProfessionName(pq.getString("professionName"));
            //qualification.setStatus(jsonObj.getBoolean("status"));
            qualification.setStatus("Pending");
            qualification.setTimestamp(jsonObj.getString("timestamp"));

            return qualification;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toActivityResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("traces");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String tracer = obj.getString("accountTracerName");
                String address = obj.getString("accountTracerAddress");
                String verified = obj.getBoolean("verified") ? "Verified" : "Unverified";
                String owner = obj.getString("accountTracerOwner");
                String time = obj.getString("timestamp");

                transaction.setId(id);
                transaction.setUser(tracer);
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setAmount(address);
                transaction.setDescription(owner);

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }

    public static ArrayList<Transaction> toConsentResponse(String jsonStr) throws JSONException {
        if (jsonStr != null && !jsonStr.isEmpty()) {
            JSONObject jsonObject = new JSONObject(jsonStr);
            //String balance = jsonObject.getString("balance");

            JSONArray jsonArr = jsonObject.getJSONArray("consents");
            ArrayList<Transaction> transactions = new ArrayList<>();
            for (int i = 0; i < jsonArr.length(); i++) {
                JSONObject obj = jsonArr.getJSONObject(i);
                Transaction transaction = new Transaction();
                String id = obj.getString("id");
                String tracer = obj.getString("accountConsenterName");
                String address = obj.getString("accountConsenterAddress");
                String status = obj.getString("status");
                String time = obj.getString("timestamp");
                String traceId = obj.getString("id");
                String tracerDid = obj.getString("accountConsenterDid");
                String tracerOwnerDid = obj.getString("accountConsenterDid");

                transaction.setId(id);
                transaction.setUser(tracer);
                if (time != null && !time.isEmpty())
                    transaction.setDate(time.substring(0, time.length() - 7));
                transaction.setAmount(address);
                transaction.setDescription(status);
                transaction.setTraceId(traceId);
                transaction.setTracerDid(tracerDid);
                transaction.setTracerOwnerDid(tracerOwnerDid);

                transactions.add(transaction);
            }

            return transactions;
        }

        throw new JSONException("Invalid JSON");
    }
}
