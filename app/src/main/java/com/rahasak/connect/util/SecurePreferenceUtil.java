package com.rahasak.connect.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Account;
import com.securepreferences.SecurePreferences;

public class SecurePreferenceUtil {
    private static SecurePreferenceUtil instance;
    private static SharedPreferences prefs;

    private SecurePreferenceUtil() {
    }

    public static SecurePreferenceUtil getInstance(Context context) {
        if (instance == null) {
            instance = new SecurePreferenceUtil();
            prefs = new SecurePreferences(context, "rahasak", context.getString(R.string.secure_preference_file_key));
        }

        return instance;
    }

    public Account getAccount() {
        Account account = new Account();
        account.setId(prefs.getString(PreferenceUtil.ACCOUNT_ID, ""));
        account.setPassword(prefs.getString(PreferenceUtil.ACCOUNT_PASSWORD, ""));
        account.setNic(prefs.getString(PreferenceUtil.ACCOUNT_NIC, ""));
        account.setNo(prefs.getString(PreferenceUtil.ACCOUNT_NO, ""));
        account.setName(prefs.getString(PreferenceUtil.ACCOUNT_NAME, ""));
        account.setPhone(prefs.getString(PreferenceUtil.ACCOUNT_PHONE, ""));
        account.setState(prefs.getString(PreferenceUtil.ACCOUNT_STATE, "PENDING"));

        return account;
    }

    public void put(String key, String value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void put(String key, int value) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public String get(String key) {
        return prefs.getString(key, "");
    }

    public int get(String key, int defVal) {
        return prefs.getInt(key, defVal);
    }
}
