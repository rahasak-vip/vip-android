package com.rahasak.connect.util;

import com.rahasak.connect.pojo.Bank;

import java.util.ArrayList;

/**
 * Created by eranga on 11/3/18.
 */

public class PaymentSelectionUtil {
    public static ArrayList<Bank> getElectracityList() {
        ArrayList<Bank> l = new ArrayList<>();
        l.add(new Bank("32341212", "Lanka electric"));
        l.add(new Bank("32341212", "Lecco"));

        return l;
    }

    public static ArrayList<Bank> getWaterList() {
        ArrayList<Bank> l = new ArrayList<>();
        l.add(new Bank("32341212", "Ceylon water board"));

        return l;
    }

    public static ArrayList<Bank> getInternetList() {
        ArrayList<Bank> l = new ArrayList<>();
        l.add(new Bank("32341212", "Dialog broadband"));
        l.add(new Bank("32341212", "Mobilel broadband"));
        l.add(new Bank("32341212", "Airtel broadband"));
        l.add(new Bank("32341212", "Hutch broadband"));
        l.add(new Bank("32341212", "Sri lanka telecom"));

        return l;
    }

    public static ArrayList<Bank> getTelephoneList() {
        ArrayList<Bank> l = new ArrayList<>();
        l.add(new Bank("32341212", "Dialog broadband"));
        l.add(new Bank("32341212", "Mobilel broadband"));
        l.add(new Bank("32341212", "Airtel broadband"));
        l.add(new Bank("32341212", "Hutch broadband"));
        l.add(new Bank("32341212", "Sri lanka telecom"));

        return l;
    }

    public static ArrayList<Bank> getTelivisionList() {
        ArrayList<Bank> l = new ArrayList<>();
        l.add(new Bank("32341212", "Dialog TV"));
        l.add(new Bank("42323232", "PEO TV"));

        return l;
    }
}
