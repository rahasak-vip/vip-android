package com.rahasak.connect.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.view.inputmethod.InputMethodManager;

import com.rahasak.connect.exceptions.*;

/**
 * Utility class to handle activity related common functions
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class ActivityUtil {

    private static ProgressDialog progressDialog;

    /**
     * Hide keyboard
     * Need to hide soft keyboard in following scenarios
     * 1. When starting background task
     * 2. When exit from activity
     * 3. On button submit
     */
    public static void hideSoftKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getApplicationContext().getSystemService(activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public static void showProgressDialog(Context context, String message) {
        progressDialog = ProgressDialog.show(context, null, message, true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setCancelable(true);

        progressDialog.show();
    }

    public static void cancelProgressDialog() {
        if (progressDialog != null) {
            progressDialog.cancel();
        }
    }

    public static void validatePersonalInfo(String nic, String name, String gender, String age, String dob) throws InvalidInputFieldsException, InvalidNicNumberException {
        if (nic.isEmpty() || name.isEmpty() || gender.isEmpty() || age.isEmpty() || dob.isEmpty()) {
            throw new InvalidInputFieldsException();
        }
        if (!(nic.length()==10 ||nic.length()==12)){
            throw new InvalidNicNumberException() ;
        }
        String patternOld = "^[0-9]{9}[vVxX]$";
        String patternNew= "^[0-9]{7}[0][0-9]{4}$";
        if (!(nic.matches(patternOld) ||nic.matches(patternNew))){
            throw new InvalidNicNumberException();
        }
        if (Integer.parseInt(age)>100 || Integer.parseInt(age) <15 ){
            throw new NumberFormatException();
        }
    }

    public static void validateContactInfo(String phone, String email, String province, String district, String address) throws InvalidPhoneNumberException, InvalidInputFieldsException, InvalidEmailException {
        if (phone.isEmpty() || email.isEmpty() || province.isEmpty() || district.isEmpty() || address.isEmpty()) {
            throw new InvalidInputFieldsException();
        }
//        String patternEmail = "^[\\w!#$%&'*+/=?`{|}~^-]+(?:\\.[\\w!#$%&'*+/=?`{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
        String patternEmail  = "^(.+)@(.+)$" ;
        if (!(email.matches(patternEmail))){
            throw new InvalidEmailException() ;
        }
        // phone
        if (phone.length() != 10) {
            throw new InvalidPhoneNumberException();
        } else {
            phone.length();
            String pattern = "^(0)[0-9]*$";
            if (!phone.matches(pattern)) {
                throw new InvalidPhoneNumberException();
            }

            if (!phone.startsWith("0")) {
                throw new InvalidPhoneNumberException();
            }
        }
    }

    public static void validateOccupationInfo(String taxId, String occupation, String employeeName, String employeePhone, String employeeAddress) throws InvalidPhoneNumberException, InvalidInputFieldsException {
        if (taxId.isEmpty() || occupation.isEmpty() || employeeName.isEmpty() || employeePhone.isEmpty() || employeeAddress.isEmpty()) {
            throw new InvalidInputFieldsException();
        }

        // phone
        if (employeePhone.length() != 10) {
            throw new InvalidPhoneNumberException();
        } else {
            employeePhone.length();
            String pattern = "^(0)[0-9]*$";
            if (!employeePhone.matches(pattern)) {
                throw new InvalidPhoneNumberException();
            }

            if (!employeePhone.startsWith("0")) {
                throw new InvalidPhoneNumberException();
            }
        }
    }

    public static void validatePasswordInfo(String password, String confirmPassword) throws InvalidPasswordException, MisMatchFieldException, InvalidInputFieldsException {
        if (password.isEmpty() || confirmPassword.isEmpty()) {
            throw new InvalidInputFieldsException();
        }

        // password
        if (password.length() < 8)
            throw new InvalidPasswordException();
        else {
            password.length();
            String pattern = "^(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
            if (!password.matches(pattern)) {
                throw new InvalidPasswordException();
            }
        }

        // confirm password
        if (!password.equals(confirmPassword))
            throw new MisMatchFieldException();
    }

    public static void validateOlResult(String methematics, String english, String science, String religion) throws InvalidInputFieldsException {
        if (methematics.isEmpty() || english.isEmpty() || science.isEmpty() || religion.isEmpty()) {
            throw new InvalidInputFieldsException();
        }
    }

    public static void isValidRegistrationFields(String nic, String name, String phone, String acc, String password, String confirmPassword) throws InvalidPhoneNumberException, InvalidPasswordException, MisMatchFieldException, InvalidInputFieldsException {
        if (nic.isEmpty() || name.isEmpty() || phone.isEmpty() || acc.isEmpty() || password.isEmpty() || confirmPassword.isEmpty()) {
            throw new InvalidInputFieldsException();
        }

        // phone
        if (phone.length() != 10) {
            throw new InvalidPhoneNumberException();
        } else {
            phone.length();
            String pattern = "^(0)[0-9]*$";
            if (!phone.matches(pattern)) {
                throw new InvalidPhoneNumberException();
            }

            if (!phone.startsWith("07")) {
                throw new InvalidPhoneNumberException();
            }
        }

        // password
        if (password.length() < 8)
            throw new InvalidPasswordException();
        else {
            password.length();
            String pattern = "^(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
            if (!password.matches(pattern)) {
                throw new InvalidPasswordException();
            }
        }

        // confirm password
        if (!password.equals(confirmPassword))
            throw new MisMatchFieldException();
    }

    public static void isValidChangePassword(String currentPassword, String newPassword, String newConfirmPassword) throws InvalidPasswordException, MisMatchFieldException, SamePasswordException {
        if (currentPassword.isEmpty())
            throw new InvalidPasswordException();

        if (newPassword.isEmpty() || newPassword.length() < 8)
            throw new InvalidPasswordException();

        else if (newPassword.length() >= 8) {
            String pattern = "^(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
            if (!newPassword.matches(pattern)) {
                throw new InvalidPasswordException();
            }
        }

        if (currentPassword.equalsIgnoreCase(newPassword))
            throw new SamePasswordException();

        if (!newPassword.equals(newConfirmPassword))
            throw new MisMatchFieldException();
    }

    public static void isValidResetPasswordInfo(String nic, String acc, String phone, String q1, String q2, String q3) throws InvalidPhoneNumberException, InvalidAccountException, InvalidAnswerException {
        if (phone.isEmpty() || phone.length() != 10) {
            throw new InvalidPhoneNumberException();
        } else if (phone.length() == 10) {
            String pattern = "^(0)[0-9]*$";
            if (!phone.matches(pattern)) {
                throw new InvalidPhoneNumberException();
            }

            if (!phone.startsWith("07")) {
                throw new InvalidPhoneNumberException();
            }
        }

        if (acc.isEmpty()) {
            throw new InvalidAccountException();
        }

        int empty = 0;
        if (q1.isEmpty())
            empty = empty + 1;
        if (q2.isEmpty())
            empty = empty + 1;
        if (q3.isEmpty())
            empty = empty + 1;

        if (empty > 1) {
            throw new InvalidAnswerException();
        }
    }

    public static void isValidResetPassword(String newPassword, String newConfirmPassword, String salt) throws InvalidPasswordException, MisMatchFieldException, InvalidInputFieldsException {
        if (newPassword.isEmpty() || newPassword.length() < 8)
            throw new InvalidPasswordException();

        else if (newPassword.length() >= 8) {
            String pattern = "^(?=.*[@#$%^&+=])(?=\\S+$).{8,}$";
            if (!newPassword.matches(pattern)) {
                throw new InvalidPasswordException();
            }
        }

        if (salt.isEmpty())
            throw new InvalidInputFieldsException();

        if (!newPassword.equals(newConfirmPassword))
            throw new MisMatchFieldException();
    }

    public static void isValidLoginFields(String phone, String givenPassword) throws InvalidInputFieldsException {
        if (phone.isEmpty())
            throw new InvalidInputFieldsException();

        if (givenPassword.isEmpty())
            throw new InvalidInputFieldsException();
    }

    public static void isValidFundTrans(String bankCode, String acc, String confirmAcc, String amount, String name) throws InvalidInputFieldsException, MisMatchFieldException {
        if (acc.isEmpty() || confirmAcc.isEmpty() || amount.isEmpty() || name.isEmpty())
            throw new InvalidInputFieldsException();

        if (!acc.equals(confirmAcc))
            throw new MisMatchFieldException();
    }

    public static void isValidPay(String acc, String amount) throws InvalidInputFieldsException {
        if (acc.isEmpty() || amount.isEmpty())
            throw new InvalidInputFieldsException();
    }

}
