package com.rahasak.connect.async;

import android.os.AsyncTask;
import android.util.Log;

import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.util.JsonUtil;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class AccountContractExecutor extends AsyncTask<String, String, Response> {

    private static final String TAG = AccountContractExecutor.class.getName();

    public static final String OWNER = "ism";

    // dev
//    public static final String IDENTITY_API = "http://192.168.64.6:7654/api/identities";
//    public static final String QUALIFICATION_API = "http://192.168.64.6:7654/api/qualifications";
//    public static final String TRACE_API = "http://52.237.109.134:7654/api/traces";
//    public static final String PROMIZE_API = "http://52.237.109.134:7654/api/traces";
//    public static final String TRANSFER_API = "http://52.237.109.134:7654/api/transfers";

    // test
    public static final String IDENTITY_API = "http://3.80.164.202:7654/api/identities";
    public static final String QUALIFICATION_API = "http://3.80.164.202:7654/api/qualifications";
    public static final String TRACE_API = "http://3.80.164.202:7654/api/traces";
    public static final String PROMIZE_API = "http://3.80.164.202:7654/api/traces";
    public static final String TRANSFER_API = "http://3.80.164.202:7654/api/transfers";

    // uat
    //public static final String ACCOUNT_API = "https://uatpromize.mbslbank.com/api/accounts";
    //public static final String PROMIZE_API = "https://uatpromize.mbslbank.com/api/promizes";
    //public static final String TRANSFER_API = "https://uatpromize.mbslbank.com/api/transfers";

    // prod
    //public static final String ACCOUNT_API = "https://promize.mbslbank.com/api/accounts";
    //public static final String PROMIZE_API = "https://promize.mbslbank.com/api/promizes";
    //public static final String TRANSFER_API = "https://promize.mbslbank.com/api/transfers";

    private IContractExecutorListener listener;
    private HashMap<String, Object> contractMap;

    public AccountContractExecutor(HashMap<String, Object> contractMap, IContractExecutorListener listener) {
        this.contractMap = contractMap;
        this.listener = listener;
    }

    @Override
    protected Response doInBackground(String... params) {
        return doPostNoSsl(params[0], params[1]);
    }

    private Response doPostNoSsl(String api, String token) {
        try {
            // connection
            URL url = new URL(api);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Authorization", "Bearer " + token);
            conn.setDoOutput(true);
            conn.setDoInput(true);

            // post
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(JsonUtil.toJsonContract(contractMap));
            os.flush();
            os.close();

            // read response
            InputStream stream = conn.getResponseCode() / 100 == 2 ? conn.getInputStream() : conn.getErrorStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(stream));
            StringBuilder responseBuilder = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                responseBuilder.append(inputLine);
            }
            in.close();

            Log.i(TAG, "StatusCode: " + conn.getResponseCode());
            Log.i(TAG, "Response: " + responseBuilder.toString());

            int status = conn.getResponseCode();
            String resp = responseBuilder.toString();
            return new Response(status, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Response doPostSsl(String api, String token) {
        try {
            // ssl config
            URL url = new URL(api);
            SSLContext sslcontext = SSLContext.getInstance("TLSv1.2");
            sslcontext.init(null, null, null);
            SSLSocketFactory NoSSLv3Factory = new NoSSLv3Factory(sslcontext.getSocketFactory());
            HttpsURLConnection.setDefaultSSLSocketFactory(NoSSLv3Factory);
            HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            // connection
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Bearer", token);
            conn.setDoOutput(true);
            conn.setDoInput(true);

            // post
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(JsonUtil.toJsonContract(contractMap));
            os.flush();
            os.close();

            // read response
            InputStream stream = conn.getResponseCode() / 100 == 2 ? conn.getInputStream() : conn.getErrorStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(stream));
            StringBuilder responseBuilder = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                responseBuilder.append(inputLine);
            }
            in.close();

            Log.i(TAG, "StatusCode: " + conn.getResponseCode());
            Log.i(TAG, "Response: " + responseBuilder.toString());

            int status = conn.getResponseCode();
            String resp = responseBuilder.toString();
            return new Response(status, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private Response doPostSelfSignedSsl(String api, String token) {
        try {
            // ssl config
            URL url = new URL(api);

            // trust self signed certificate
            TrustManager localTrustManager = new X509TrustManager() {
                @Override
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }

                @Override
                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                @Override
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }
            };

            // https with self signed certificate
            SSLContext sslcontext = SSLContext.getInstance("TLS");
            sslcontext.init(null, new TrustManager[]{localTrustManager}, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sslcontext.getSocketFactory());
            HostnameVerifier hostnameVerifier = org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;
            HttpsURLConnection.setDefaultHostnameVerifier(hostnameVerifier);
            HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();

            // connection
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Bearer", token);
            conn.setDoOutput(true);
            conn.setDoInput(true);

            // post
            DataOutputStream os = new DataOutputStream(conn.getOutputStream());
            os.writeBytes(JsonUtil.toJsonContract(contractMap));
            os.flush();
            os.close();

            // read response
            InputStream stream = conn.getResponseCode() / 100 == 2 ? conn.getInputStream() : conn.getErrorStream();
            BufferedReader in = new BufferedReader(new InputStreamReader(stream));
            StringBuilder responseBuilder = new StringBuilder();
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                responseBuilder.append(inputLine);
            }
            in.close();

            Log.i(TAG, "StatusCode: " + conn.getResponseCode());
            Log.i(TAG, "Response: " + responseBuilder.toString());

            int status = conn.getResponseCode();
            String resp = responseBuilder.toString();
            return new Response(status, resp);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onPostExecute(Response response) {
        super.onPostExecute(response);

        // parse and find result, status back
        listener.onFinishTask(response);
    }

}
