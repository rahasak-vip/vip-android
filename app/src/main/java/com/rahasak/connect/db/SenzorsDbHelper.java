package com.rahasak.connect.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Perform creating tables here
 *
 * @author erangaeb@gmail.com(eranga herath)
 */
class SenzorsDbHelper extends SQLiteOpenHelper {

    private static final String TAG = SenzorsDbHelper.class.getName();

    // we use singleton database
    private static SenzorsDbHelper senzorsDbHelper;

    // If you change the database schema, you must increment the database version
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "Promize.db";

    // data types, keywords and queries
    private static final String TEXT_TYPE = " TEXT";
    private static final String INT_TYPE = " INTEGER";

    private static final String SQL_CREATE_PAYEE =
            "CREATE TABLE IF NOT EXISTS " + SenzorsDbContract.Payee.TABLE_NAME + " (" +
                    SenzorsDbContract.Payee._ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + "," +
                    SenzorsDbContract.Payee.COLUMN_NAME_PAYEE_NAME + TEXT_TYPE + ", " +
                    SenzorsDbContract.Payee.COLUMN_NAME_BANK + TEXT_TYPE + ", " +
                    SenzorsDbContract.Payee.COLUMN_NAME_BANK_CODE + TEXT_TYPE + ", " +
                    SenzorsDbContract.Payee.COLUMN_NAME_BRANCH + TEXT_TYPE + ", " +
                    SenzorsDbContract.Payee.COLUMN_NAME_BRANCH_CODE + TEXT_TYPE + ", " +
                    SenzorsDbContract.Payee.COLUMN_NAME_ACCOUNT_NO + TEXT_TYPE + ", " +
                    "UNIQUE(" + SenzorsDbContract.Payee.COLUMN_NAME_BANK + ", " + SenzorsDbContract.Payee.COLUMN_NAME_ACCOUNT_NO + ")" +
                    " )";

    private static final String SQL_CREATE_BILLER =
            "CREATE TABLE IF NOT EXISTS " + SenzorsDbContract.Biller.TABLE_NAME + " (" +
                    SenzorsDbContract.Biller._ID + " INTEGER PRIMARY KEY AUTOINCREMENT" + "," +
                    SenzorsDbContract.Biller.COLUMN_NAME_BILL_TYPE + TEXT_TYPE + "," +
                    SenzorsDbContract.Biller.COLUMN_NAME_BILLER_NAME + TEXT_TYPE + " UNIQUE NOT NULL" + ", " +
                    SenzorsDbContract.Biller.COLUMN_NAME_BILLER_ACCOUNT + TEXT_TYPE +
                    " )";

    /**
     * Init context
     * Init database
     *
     * @param context application context
     */
    private SenzorsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * We are reusing one database instance in all over the app for better memory usage
     *
     * @param context application context
     * @return db helper instance
     */
    synchronized static SenzorsDbHelper getInstance(Context context) {
        if (senzorsDbHelper == null) {
            senzorsDbHelper = new SenzorsDbHelper(context.getApplicationContext());
        }
        return (senzorsDbHelper);
    }

    /**
     * {@inheritDoc}
     */
    public void onCreate(SQLiteDatabase db) {
        Log.d(TAG, "OnCreate: creating db helper, db version - " + DATABASE_VERSION);
        Log.d(TAG, SQL_CREATE_PAYEE);
        Log.d(TAG, SQL_CREATE_BILLER);

        db.execSQL(SQL_CREATE_PAYEE);
        db.execSQL(SQL_CREATE_BILLER);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onConfigure(SQLiteDatabase db) {
        super.onConfigure(db);

        // enable foreign key constraint here
        Log.d(TAG, "OnConfigure: Enable foreign key constraint");
        db.setForeignKeyConstraintsEnabled(true);
    }

    /**
     * {@inheritDoc}
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "OnUpgrade: updating db helper, db version - " + DATABASE_VERSION);

        db.execSQL("DROP TABLE IF EXISTS PAYEE;");
        db.execSQL("DROP TABLE IF EXISTS PROMIZE;");
        db.execSQL("DROP TABLE IF EXISTS BILLER;");
        onCreate(db);
    }

    /**
     * {@inheritDoc}
     */
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

}
