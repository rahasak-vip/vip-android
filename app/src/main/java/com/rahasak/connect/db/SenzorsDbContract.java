package com.rahasak.connect.db;

import android.provider.BaseColumns;

/**
 * Keep database table attributes
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
class SenzorsDbContract {

    public SenzorsDbContract() {
    }

    /* Inner class that defines the account table contents */
    static abstract class Payee implements BaseColumns {
        static final String TABLE_NAME = "payee";
        static final String COLUMN_NAME_PAYEE_NAME = "name";
        static final String COLUMN_NAME_BANK = "bank";
        static final String COLUMN_NAME_BANK_CODE = "bank_code";
        static final String COLUMN_NAME_BRANCH = "branch";
        static final String COLUMN_NAME_BRANCH_CODE = "branch_code";
        static final String COLUMN_NAME_ACCOUNT_NO = "no";
    }

    /* Inner class that defines the promize table contents */
    static abstract class Promize implements BaseColumns {
        static final String TABLE_NAME = "promize";
        static final String COLUMN_NAME_PROMIZE_ID = "pid";
        static final String COLUMN_NAME_USER = "user";
        static final String COLUMN_NAME_ACCOUNT = "account";
        static final String COLUMN_NAME_AMOUNT = "amount";
        static final String COLUMN_NAME_SALT = "salt";
        static final String COLUMN_NAME_DIRECTION = "direction";
        static final String COLUMN_NAME_TIMESTAMP = "timestamp";
    }

    /* Inner class that defines the biller table contents */
    static abstract class Biller implements BaseColumns {
        static final String TABLE_NAME = "biller";
        static final String COLUMN_NAME_BILL_TYPE = "bill_type";
        static final String COLUMN_NAME_BILLER_NAME = "biller_name";
        static final String COLUMN_NAME_BILLER_ACCOUNT = "biller_account";
    }

}
