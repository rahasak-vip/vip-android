package com.rahasak.connect.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class TransactionResponse implements Parcelable {
    private String balance;
    private ArrayList<Transaction> transactions;

    public TransactionResponse(String balance, ArrayList<Transaction> transactions) {
        this.balance = balance;
        this.transactions = transactions;
    }

    protected TransactionResponse(Parcel in) {
        balance = in.readString();
        transactions = in.createTypedArrayList(Transaction.CREATOR);
    }

    public static final Creator<TransactionResponse> CREATOR = new Creator<TransactionResponse>() {
        @Override
        public TransactionResponse createFromParcel(Parcel in) {
            return new TransactionResponse(in);
        }

        @Override
        public TransactionResponse[] newArray(int size) {
            return new TransactionResponse[size];
        }
    };

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(balance);
        dest.writeTypedList(transactions);
    }
}