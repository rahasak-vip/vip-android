package com.rahasak.connect.pojo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class BankAccountResponse implements Parcelable {
    private ArrayList<BankAccount> accounts;
    private ArrayList<String> promizes;
    private ArrayList<String> transfers;

    public BankAccountResponse() {
    }

    protected BankAccountResponse(Parcel in) {
        accounts = in.createTypedArrayList(BankAccount.CREATOR);
        promizes = in.createStringArrayList();
        transfers = in.createStringArrayList();
    }

    public static final Creator<BankAccountResponse> CREATOR = new Creator<BankAccountResponse>() {
        @Override
        public BankAccountResponse createFromParcel(Parcel in) {
            return new BankAccountResponse(in);
        }

        @Override
        public BankAccountResponse[] newArray(int size) {
            return new BankAccountResponse[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeTypedList(accounts);
        parcel.writeStringList(promizes);
        parcel.writeStringList(transfers);
    }

    public ArrayList<BankAccount> getAccounts() {
        return accounts;
    }

    public void setAccounts(ArrayList<BankAccount> accounts) {
        this.accounts = accounts;
    }

    public ArrayList<String> getPromizes() {
        return promizes;
    }

    public void setPromizes(ArrayList<String> promizes) {
        this.promizes = promizes;
    }

    public ArrayList<String> getTransfers() {
        return transfers;
    }

    public void setTransfers(ArrayList<String> transfers) {
        this.transfers = transfers;
    }
}
