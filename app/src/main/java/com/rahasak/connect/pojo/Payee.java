package com.rahasak.connect.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Payee implements Parcelable {
    private String name;
    private String bankName;
    private String bankCode;
    private String accountNo;
    private String branch;
    private String branchCode;

    public Payee() {
    }

    protected Payee(Parcel in) {
        name = in.readString();
        bankName = in.readString();
        bankCode = in.readString();
        accountNo = in.readString();
        branch = in.readString();
        branchCode = in.readString();
    }

    public static final Creator<Payee> CREATOR = new Creator<Payee>() {
        @Override
        public Payee createFromParcel(Parcel in) {
            return new Payee(in);
        }

        @Override
        public Payee[] newArray(int size) {
            return new Payee[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    public String getAccountNo() {
        return accountNo;
    }

    public void setAccountNo(String accountNo) {
        this.accountNo = accountNo;
    }

    public String getBranch() {
        return branch;
    }

    public void setBranch(String branch) {
        this.branch = branch;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(bankName);
        dest.writeString(bankCode);
        dest.writeString(accountNo);
        dest.writeString(branch);
        dest.writeString(branchCode);
    }
}
