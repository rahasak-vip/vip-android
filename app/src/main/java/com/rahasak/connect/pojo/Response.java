package com.rahasak.connect.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Response implements Parcelable {
    private int status;
    private String payload;

    public Response(int status, String payload) {
        this.status = status;
        this.payload = payload;
    }

    protected Response(Parcel in) {
        status = in.readInt();
        payload = in.readString();
    }

    public static final Creator<Response> CREATOR = new Creator<Response>() {
        @Override
        public Response createFromParcel(Parcel in) {
            return new Response(in);
        }

        @Override
        public Response[] newArray(int size) {
            return new Response[size];
        }
    };

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(status);
        dest.writeString(payload);
    }
}
