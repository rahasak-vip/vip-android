package com.rahasak.connect.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class BankAccount implements Parcelable {
    String no;
    String name;
    String typ;
    String availableBalance;
    String currentBalance;
    boolean isPromizeAccount;

    public BankAccount() {
    }

    protected BankAccount(Parcel in) {
        no = in.readString();
        name = in.readString();
        typ = in.readString();
        availableBalance = in.readString();
        currentBalance = in.readString();
        isPromizeAccount = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(no);
        dest.writeString(name);
        dest.writeString(typ);
        dest.writeString(availableBalance);
        dest.writeString(currentBalance);
        dest.writeByte((byte) (isPromizeAccount ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<BankAccount> CREATOR = new Creator<BankAccount>() {
        @Override
        public BankAccount createFromParcel(Parcel in) {
            return new BankAccount(in);
        }

        @Override
        public BankAccount[] newArray(int size) {
            return new BankAccount[size];
        }
    };

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }

    public String getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(String currentBalance) {
        this.currentBalance = currentBalance;
    }

    public boolean isPromizeAccount() {
        return isPromizeAccount;
    }

    public void setPromizeAccount(boolean promizeAccount) {
        isPromizeAccount = promizeAccount;
    }
}
