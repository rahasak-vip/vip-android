package com.rahasak.connect.pojo;

public class TokenReply {

    private int status;
    private String token;

    public TokenReply() {
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}

