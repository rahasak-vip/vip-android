package com.rahasak.connect.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class Qualification implements Parcelable {
    private String id;
    private String holder;
    private String olMethamatics;
    private String olEnglishg;
    private String olSinhala;
    private String olTamil;
    private String olScience;
    private String olHealthScience;
    private String olReligion;
    private String olArt;
    private String olHistory;
    private String olGeography;

    private Boolean hasDiploma;
    private String nameDiploma;

    private Boolean hasExperience;
    private String nameExperience;
    private String durationExperience;
    private String professionArea;
    private String professionName;

    private String status;
    private String timestamp;

    public Qualification() {

    }

    protected Qualification(Parcel in) {
        id = in.readString();
        holder = in.readString();
        olMethamatics = in.readString();
        olEnglishg = in.readString();
        olSinhala = in.readString();
        olTamil = in.readString();
        olScience = in.readString();
        olHealthScience = in.readString();
        olReligion = in.readString();
        olArt = in.readString();
        olHistory = in.readString();
        olGeography = in.readString();
        byte tmpHasDiploma = in.readByte();
        hasDiploma = tmpHasDiploma == 0 ? null : tmpHasDiploma == 1;
        nameDiploma = in.readString();
        byte tmpHasExperience = in.readByte();
        hasExperience = tmpHasExperience == 0 ? null : tmpHasExperience == 1;
        nameExperience = in.readString();
        durationExperience = in.readString();
        professionArea = in.readString();
        professionName = in.readString();
        status = in.readString();
        timestamp = in.readString();
    }

    public static final Creator<Qualification> CREATOR = new Creator<Qualification>() {
        @Override
        public Qualification createFromParcel(Parcel in) {
            return new Qualification(in);
        }

        @Override
        public Qualification[] newArray(int size) {
            return new Qualification[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(holder);
        parcel.writeString(olMethamatics);
        parcel.writeString(olEnglishg);
        parcel.writeString(olSinhala);
        parcel.writeString(olTamil);
        parcel.writeString(olScience);
        parcel.writeString(olHealthScience);
        parcel.writeString(olReligion);
        parcel.writeString(olArt);
        parcel.writeString(olHistory);
        parcel.writeString(olGeography);
        parcel.writeByte((byte) (hasDiploma == null ? 0 : hasDiploma ? 1 : 2));
        parcel.writeString(nameDiploma);
        parcel.writeByte((byte) (hasExperience == null ? 0 : hasExperience ? 1 : 2));
        parcel.writeString(nameExperience);
        parcel.writeString(durationExperience);
        parcel.writeString(professionArea);
        parcel.writeString(professionName);
        parcel.writeString(status);
        parcel.writeString(timestamp);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public String getOlMethamatics() {
        return olMethamatics;
    }

    public void setOlMethamatics(String olMethamatics) {
        this.olMethamatics = olMethamatics;
    }

    public String getOlEnglishg() {
        return olEnglishg;
    }

    public void setOlEnglishg(String olEnglishg) {
        this.olEnglishg = olEnglishg;
    }

    public String getOlSinhala() {
        return olSinhala;
    }

    public void setOlSinhala(String olSinhala) {
        this.olSinhala = olSinhala;
    }

    public String getOlTamil() {
        return olTamil;
    }

    public void setOlTamil(String olTamil) {
        this.olTamil = olTamil;
    }

    public String getOlScience() {
        return olScience;
    }

    public void setOlScience(String olScience) {
        this.olScience = olScience;
    }

    public String getOlHealthScience() {
        return olHealthScience;
    }

    public void setOlHealthScience(String olHealthScience) {
        this.olHealthScience = olHealthScience;
    }

    public String getOlReligion() {
        return olReligion;
    }

    public void setOlReligion(String olReligion) {
        this.olReligion = olReligion;
    }

    public String getOlArt() {
        return olArt;
    }

    public void setOlArt(String olArt) {
        this.olArt = olArt;
    }

    public String getOlHistory() {
        return olHistory;
    }

    public void setOlHistory(String olHistory) {
        this.olHistory = olHistory;
    }

    public String getOlGeography() {
        return olGeography;
    }

    public void setOlGeography(String olGeography) {
        this.olGeography = olGeography;
    }

    public Boolean getHasDiploma() {
        return hasDiploma;
    }

    public void setHasDiploma(Boolean hasDiploma) {
        this.hasDiploma = hasDiploma;
    }

    public String getNameDiploma() {
        return nameDiploma;
    }

    public void setNameDiploma(String nameDiploma) {
        this.nameDiploma = nameDiploma;
    }

    public Boolean getHasExperience() {
        return hasExperience;
    }

    public void setHasExperience(Boolean hasExperience) {
        this.hasExperience = hasExperience;
    }

    public String getNameExperience() {
        return nameExperience;
    }

    public void setNameExperience(String nameExperience) {
        this.nameExperience = nameExperience;
    }

    public String getDurationExperience() {
        return durationExperience;
    }

    public void setDurationExperience(String durationExperience) {
        this.durationExperience = durationExperience;
    }

    public String getProfessionArea() {
        return professionArea;
    }

    public void setProfessionArea(String professionArea) {
        this.professionArea = professionArea;
    }

    public String getProfessionName() {
        return professionName;
    }

    public void setProfessionName(String professionName) {
        this.professionName = professionName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }
}

