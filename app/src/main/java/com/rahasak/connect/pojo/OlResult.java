package com.rahasak.connect.pojo;

import android.os.Parcel;
import android.os.Parcelable;

public class OlResult implements Parcelable {
    private String olMethamatics;
    private String olEnglishg;
    private String olSinhala;
    private String olTamil;
    private String olScience;
    private String olHealthScience;
    private String olReligion;
    private String olArt;
    private String olHistory;
    private String olGeography;

    public OlResult() {
    }

    protected OlResult(Parcel in) {
        olMethamatics = in.readString();
        olEnglishg = in.readString();
        olSinhala = in.readString();
        olTamil = in.readString();
        olScience = in.readString();
        olHealthScience = in.readString();
        olReligion = in.readString();
        olArt = in.readString();
        olHistory = in.readString();
        olGeography = in.readString();
    }

    public static final Creator<OlResult> CREATOR = new Creator<OlResult>() {
        @Override
        public OlResult createFromParcel(Parcel in) {
            return new OlResult(in);
        }

        @Override
        public OlResult[] newArray(int size) {
            return new OlResult[size];
        }
    };

    public String getOlMethamatics() {
        return olMethamatics;
    }

    public void setOlMethamatics(String olMethamatics) {
        this.olMethamatics = olMethamatics;
    }

    public String getOlEnglishg() {
        return olEnglishg;
    }

    public void setOlEnglishg(String olEnglishg) {
        this.olEnglishg = olEnglishg;
    }

    public String getOlSinhala() {
        return olSinhala;
    }

    public void setOlSinhala(String olSinhala) {
        this.olSinhala = olSinhala;
    }

    public String getOlTamil() {
        return olTamil;
    }

    public void setOlTamil(String olTamil) {
        this.olTamil = olTamil;
    }

    public String getOlScience() {
        return olScience;
    }

    public void setOlScience(String olScience) {
        this.olScience = olScience;
    }

    public String getOlHealthScience() {
        return olHealthScience;
    }

    public void setOlHealthScience(String olHealthScience) {
        this.olHealthScience = olHealthScience;
    }

    public String getOlReligion() {
        return olReligion;
    }

    public void setOlReligion(String olReligion) {
        this.olReligion = olReligion;
    }

    public String getOlArt() {
        return olArt;
    }

    public void setOlArt(String olArt) {
        this.olArt = olArt;
    }

    public String getOlHistory() {
        return olHistory;
    }

    public void setOlHistory(String olHistory) {
        this.olHistory = olHistory;
    }

    public String getOlGeography() {
        return olGeography;
    }

    public void setOlGeography(String olGeography) {
        this.olGeography = olGeography;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(olMethamatics);
        parcel.writeString(olEnglishg);
        parcel.writeString(olSinhala);
        parcel.writeString(olTamil);
        parcel.writeString(olScience);
        parcel.writeString(olHealthScience);
        parcel.writeString(olReligion);
        parcel.writeString(olArt);
        parcel.writeString(olHistory);
        parcel.writeString(olGeography);
    }
}
