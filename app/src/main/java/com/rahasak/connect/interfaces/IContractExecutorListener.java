package com.rahasak.connect.interfaces;

import com.rahasak.connect.pojo.Response;

public interface IContractExecutorListener {
    void onFinishTask(String string);
    void onFinishTask(Response response);
}
