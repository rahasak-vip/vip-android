package com.rahasak.connect.application;

import android.app.Application;
import android.util.Log;

import com.rahasak.connect.pojo.Identity;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Application class to hold shared attributes
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class SenzApplication extends Application {

    private static final String TAG = SenzApplication.class.getName();

    private static Timer timer;

    private static boolean login = false;

    private static boolean refreshWallet = true;

    private static boolean refreshPromize = true;

    private static boolean refreshConsents = true;

    private static boolean refreshSettings = true;

    private static Identity identity = null;

    private static String identityOwner = "ism";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public static void startLogoutTimer() {
        Log.i(TAG, "invoke logout timer");

        // cancel timer first
        cancelLogoutTimer();

        // auto logout in 60 minutes (1000 * 60 * 60)
        timer = new Timer();
        LogOutTimerTask logoutTimeTask = new LogOutTimerTask();
        //timer.schedule(logoutTimeTask, 1000 * 60 * 60);
        timer.schedule(logoutTimeTask, 1000 * 60 * 60);
    }

    public static void cancelLogoutTimer() {
        if (timer != null) {
            Log.i(TAG, "cancel logout timer");

            timer.cancel();
            timer = null;
        }
    }

    public static boolean isLogin() {
        return login;
    }

    public static void setLogin(boolean login) {
        SenzApplication.login = login;
    }

    public static boolean isRefreshWallet() {
        return refreshWallet;
    }

    public static void setRefreshWallet(boolean refreshWallet) {
        SenzApplication.refreshWallet = refreshWallet;
    }

    public static boolean isRefreshPromize() {
        return refreshPromize;
    }

    public static void setRefreshPromize(boolean refreshPromize) {
        SenzApplication.refreshPromize = refreshPromize;
    }

    public static boolean isRefreshConsents() {
        return refreshConsents;
    }

    public static void setRefreshConsents(boolean refreshConsents) {
        SenzApplication.refreshConsents = refreshConsents;
    }

    public static boolean isRefreshSettings() {
        return refreshSettings;
    }

    public static void setRefreshSettings(boolean refreshSettings) {
        SenzApplication.refreshSettings = refreshSettings;
    }

    public static Identity getIdentity() {
        return identity;
    }

    public static void setIdentity(Identity identity) {
        SenzApplication.identity = identity;
    }

    public static String getIdentityOwner() {
        return identityOwner;
    }

    public static void setIdentityOwner(String identityOwner) {
        SenzApplication.identityOwner = identityOwner;
    }

    private static class LogOutTimerTask extends TimerTask {
        @Override
        public void run() {
            //redirect user to login screen
            Log.i(TAG, "fire logout timer");
            SenzApplication.login = false;
        }
    }
}
