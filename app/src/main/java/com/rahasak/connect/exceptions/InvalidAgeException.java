package com.rahasak.connect.exceptions;

public class InvalidAgeException extends Exception {
    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Invalid age";
    }
}

