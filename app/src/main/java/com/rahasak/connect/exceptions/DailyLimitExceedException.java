package com.rahasak.connect.exceptions;

public class DailyLimitExceedException extends Exception {

    @Override
    public String toString() {
        return "daily promize limit exceeded";
    }

}
