package com.rahasak.connect.exceptions;

/**
 * Created by mauran on 5/13/18.
 */

public class InvalidPhoneNumberException extends Exception {

    @Override
    public String toString() {
        return "Invalid phone number";
    }
}
