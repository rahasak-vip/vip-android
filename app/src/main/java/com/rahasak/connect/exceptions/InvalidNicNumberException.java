package com.rahasak.connect.exceptions;

public class InvalidNicNumberException extends Exception  {

    private static final long serialVersionUID = 1L;

    @Override
    public String toString() {
        return "Invalid Nic No . Nic should contain 9 digits followed by a V/X or 12 digits";
    }
}


