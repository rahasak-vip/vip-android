package com.rahasak.connect.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.pojo.Category;
import com.rahasak.connect.pojo.Qualification;
import com.rahasak.connect.util.ActivityUtil;

import java.util.ArrayList;

public class OlResultActivity extends BaseActivity {

    // ui controls
    private EditText methametics;
    private EditText english;
    private EditText sinhala;
    private EditText tamil;
    private EditText science;
    private EditText health;
    private EditText religion;
    private EditText art;
    private EditText history;
    private EditText geography;
    private Button nextButton;

    private String methameticsr;
    private String englishr;
    private String sinhalar;
    private String tamilr;
    private String sciencer;
    private String healthr;
    private String religionr;
    private String artr;
    private String historyr;
    private String geographyr;

    private Qualification qualification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ol_result_activity);

        initUi();
        initToolbar();
        initActionBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Education Qualifications(O/L)");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        methametics = (EditText) findViewById(R.id.methamatics);
        english = (EditText) findViewById(R.id.english);
        sinhala = (EditText) findViewById(R.id.sinhala);
        tamil = (EditText) findViewById(R.id.tamil);
        science = (EditText) findViewById(R.id.science);
        health = (EditText) findViewById(R.id.health);
        religion = (EditText) findViewById(R.id.religion);
        art = (EditText) findViewById(R.id.art);
        history = (EditText) findViewById(R.id.history);
        geography = (EditText) findViewById(R.id.geo);

        methametics.setTypeface(typeface, Typeface.NORMAL);
        english.setTypeface(typeface, Typeface.NORMAL);
        sinhala.setTypeface(typeface, Typeface.NORMAL);
        tamil.setTypeface(typeface, Typeface.NORMAL);
        science.setTypeface(typeface, Typeface.NORMAL);
        health.setTypeface(typeface, Typeface.NORMAL);
        religion.setTypeface(typeface, Typeface.NORMAL);
        art.setTypeface(typeface, Typeface.NORMAL);
        history.setTypeface(typeface, Typeface.NORMAL);
        geography.setTypeface(typeface, Typeface.NORMAL);

        setFocusListener("Mathematics", methameticsr, methametics);
        setFocusListener("English", englishr, english);
        setFocusListener("Sinhala", sinhalar, sinhala);
        setFocusListener("Tamil", tamilr, tamil);
        setFocusListener("Science", sciencer, science);
        setFocusListener("Health Science", healthr, health);
        setFocusListener("Religion", religionr, religion);
        setFocusListener("Art", artr, art);
        setFocusListener("History", historyr, history);
        setFocusListener("Geography", geographyr, geography);

        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setTypeface(typeface, Typeface.BOLD);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickNext();
            }
        });
    }

    private void setFocusListener(final String titleText, final String var, final EditText editText) {
        editText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusResult(titleText, editText);
            }
        });
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusResult(titleText, editText);
                }
            }
        });
    }

    private void onFocusResult(final String titleText, final EditText editText) {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("A/D", ""));
        list.add(new Category("B", ""));
        list.add(new Category("C", ""));
        list.add(new Category("S", ""));
        list.add(new Category("F/W", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                editText.setText(titleText + " - " + category.getName());

                if (titleText.equalsIgnoreCase("Mathematics"))
                    methameticsr = category.getName();
                else if (titleText.equalsIgnoreCase("English"))
                    englishr = category.getName();
                else if (titleText.equalsIgnoreCase("Sinhala"))
                    sinhalar = category.getName();
                else if (titleText.equalsIgnoreCase("Tamil"))
                    tamilr = category.getName();
                else if (titleText.equalsIgnoreCase("Science"))
                    sciencer = category.getName();
                else if (titleText.equalsIgnoreCase("Health Science"))
                    healthr = category.getName();
                else if (titleText.equalsIgnoreCase("Religion"))
                    religionr = category.getName();
                else if (titleText.equalsIgnoreCase("Art"))
                    artr = category.getName();
                else if (titleText.equalsIgnoreCase("History"))
                    historyr = category.getName();
                else if (titleText.equalsIgnoreCase("Geography"))
                    geographyr = category.getName();

                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText(titleText);

        dialog.show();
    }

    private void onClickNext() {
        try {
            String m = methametics.getText().toString().trim();
            String e = english.getText().toString().trim();
            String s = science.getText().toString().trim();
            String r = religion.getText().toString().trim();
            String a = art.getText().toString().trim();
            ActivityUtil.validateOlResult(m, e, s, r);

            qualification = new Qualification();
            qualification.setOlMethamatics(methameticsr);
            qualification.setOlEnglishg(englishr);
            qualification.setOlSinhala(sinhalar);
            qualification.setOlTamil(tamilr);
            qualification.setOlScience(sciencer);
            qualification.setOlHealthScience(healthr);
            qualification.setOlReligion(religionr);
            qualification.setOlArt(artr);
            qualification.setOlHistory(historyr);
            qualification.setOlGeography(geographyr);

            navigateNext();
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all required fields.");
        }
    }

    private void navigateNext() {
        ActivityUtil.hideSoftKeyboard(this);
        Intent intent = new Intent(OlResultActivity.this, EductionQualificationQuestionActivity.class);
        intent.putExtra("QUALIFICATION", qualification);
        OlResultActivity.this.startActivity(intent);
    }

}
