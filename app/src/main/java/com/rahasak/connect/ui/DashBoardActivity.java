package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.enums.CustomerActionType;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class DashBoardActivity extends BaseActivity implements IContractExecutorListener {

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.activity_dash_board);
        initUi();

        // update config
        if (PreferenceUtil.get(this, PreferenceUtil.UPDATE_FIREBASE_TOKEN).equalsIgnoreCase("yes")) {
            updateConfig();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!SenzApplication.isLogin()) {
            displayConfirmationMessageDialog("Session timeout", "Your session has expired, please login again", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(DashBoardActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    overridePendingTransition(R.anim.right_in, R.anim.stay_in);
                    finishAffinity();
                }
            });
        }
    }

    private void initUi() {
        ((TextView) findViewById(R.id.send_promize_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.receive_promize_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.payments_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.fund_transfer_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.transactions_text)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.settings_text)).setTypeface(typeface, Typeface.BOLD);

        onClickSendPromize();
        onClickReceivePromize();
        onClickPayments();
        onClickFundTransfer();
        onClickTransactions();
        onClickSettings();
    }

    private void onClickSendPromize() {
        findViewById(R.id.send_promize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, SendPromizeActivity.class);
                intent.putExtra("ACTION", CustomerActionType.CUSTOMER_LIST.toString());
                startActivity(intent);
            }
        });
    }

    private void onClickReceivePromize() {
        findViewById(R.id.receive_promize).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, ReceivePromizeScanActivity.class);
                intent.putExtra("ACTION", CustomerActionType.CUSTOMER_LIST.toString());
                startActivity(intent);
            }
        });
    }

    private void onClickPayments() {
        findViewById(R.id.payments).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashBoardActivity.this, PaymentListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void onClickFundTransfer() {
        findViewById(R.id.fund_transfer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // goto bank select
                Intent intent = new Intent(DashBoardActivity.this, PayeeListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void onClickTransactions() {
        findViewById(R.id.transactions).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // navigate to customer list
                Intent intent = new Intent(DashBoardActivity.this, TransactionListActivity.class);
                intent.putExtra("ACTION", CustomerActionType.CUSTOMER_LIST.toString());
                startActivity(intent);
            }
        });
    }

    private void onClickSettings() {
        findViewById(R.id.settings).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // navigate to chat activity
                Intent intent = new Intent(DashBoardActivity.this, SettingsActivity.class);
                startActivity(intent);
            }
        });
    }

    private void updateConfig() {
        try {
            String accountId = PreferenceUtil.get(this, PreferenceUtil.ACCOUNT_ID);
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "updateDevice");
            createMap.put("execer", accountId);
            createMap.put("id", accountId + System.currentTimeMillis());
            createMap.put("accountId", accountId);
            createMap.put("deviceType", "android");
            createMap.put("deviceToken", PreferenceUtil.get(this, PreferenceUtil.FIREBASE_TOKEN));

            ActivityUtil.showProgressDialog(this, "Update configurations...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String string) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 200) {
                    Toast.makeText(this, "Configuration updated", Toast.LENGTH_LONG).show();
                    PreferenceUtil.put(this, PreferenceUtil.UPDATE_FIREBASE_TOKEN, "no");
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", "Something went wrong while update configuration.");
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while update configuration.");
        }
    }
}
