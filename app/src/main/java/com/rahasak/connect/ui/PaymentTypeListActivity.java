package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Bank;
import com.rahasak.connect.util.PaymentSelectionUtil;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class PaymentTypeListActivity extends BaseActivity {

    private EditText searchView;
    private ListView bankListView;
    private BankListAdapter adapter;
    private String billType;
    private static ArrayList<Bank> bankList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);

        setupToolbar();
        setupActionBar();
        setupSearchView();
        initPrefs();
        initList();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Choose biller");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setupSearchView() {
        searchView = (EditText) findViewById(R.id.inputSearch);
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initPrefs() {
        if (getIntent().hasExtra("BILL_TYPE")) {
            this.billType = getIntent().getStringExtra("BILL_TYPE");
            if (billType.equalsIgnoreCase("ELECTERCITY"))
                bankList = PaymentSelectionUtil.getElectracityList();
            else if (billType.equalsIgnoreCase("Water"))
                bankList = PaymentSelectionUtil.getWaterList();
            else if (billType.equalsIgnoreCase("Internet"))
                bankList = PaymentSelectionUtil.getInternetList();
            else if (billType.equalsIgnoreCase("Telephone"))
                bankList = PaymentSelectionUtil.getTelephoneList();
            else if (billType.equalsIgnoreCase("Television"))
                bankList = PaymentSelectionUtil.getTelivisionList();
        }

        Collections.sort(bankList, new Comparator<Bank>() {
            public int compare(Bank o1, Bank o2) {
                return o1.getBankName().compareTo(o2.getBankName());
            }
        });
    }

    private void initList() {
        bankListView = (ListView) findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        adapter = new BankListAdapter(this, bankList);
        bankListView.setAdapter(adapter);

        // click listener
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Bank bank = (Bank) adapter.getItem(position);
                Intent intent = new Intent(PaymentTypeListActivity.this, PaymentActivity.class);
                intent.putExtra("BILL_TYPE", billType);
                intent.putExtra("BILLER", bank);
                startActivity(intent);
                PaymentTypeListActivity.this.finish();
            }
        });
    }

}
