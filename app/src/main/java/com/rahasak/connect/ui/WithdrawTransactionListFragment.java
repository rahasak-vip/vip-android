package com.rahasak.connect.ui;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Promize;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.Transaction;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;

public class WithdrawTransactionListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, IContractExecutorListener {

    private ArrayList<Promize> transList;
    private TransactionListAdapter adapter;
    private ListView listView;
    private RelativeLayout emptyView;
    private boolean transLoaded = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transaction_list_fragment_layout, container, false);
        initEmptyView(view);
        initListView(view);

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !transLoaded) {
            transLoaded = true;
            fetchTrans("0", "10");
        }
    }

    private void initEmptyView(View view) {
        Typeface typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");
        TextView emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);
        emptyText.setText("You have not received an igift yet. Swipe down to check for new igifts");
    }

    private void initListView(View view) {
        listView = (ListView) view.findViewById(R.id.cheque_list_view);
        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);

        listView.setOnItemClickListener(this);
        listView.setOnItemLongClickListener(this);

        transList = new ArrayList<>();
        adapter = new TransactionListAdapter(getActivity(), new ArrayList<Transaction>());
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    private void refreshList(ArrayList<Promize> promizes) {
        transList.addAll(promizes);
        adapter.notifyDataSetChanged();

        if (transList.size() == 0) {
            emptyView.setVisibility(View.GONE);
            listView.setEmptyView(emptyView);
        } else {
            emptyView.setVisibility(View.GONE);
        }
    }

    private void fetchTrans(String offset, String limit) {
        try {
            Account account = PreferenceUtil.getAccount(this.getActivity());

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "history");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("offset", offset);
            createMap.put("limit", limit);

            ActivityUtil.showProgressDialog(getActivity(), "Fetching transactions...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(getActivity(), PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Promize promize = transList.get(position);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        final Promize promize = transList.get(position);
        return true;
    }

    @Override
    public void onFinishTask(String response) {
        ActivityUtil.cancelProgressDialog();
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
    }
}
