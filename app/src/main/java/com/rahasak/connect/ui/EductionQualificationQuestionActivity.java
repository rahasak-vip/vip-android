package com.rahasak.connect.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Category;
import com.rahasak.connect.pojo.Qualification;

import java.util.ArrayList;

public class EductionQualificationQuestionActivity extends BaseActivity {

    // UI fields
    private TextView question1Text;
    private TextView question2Text;

    private EditText question1;
    private EditText question2;

    private String answer1;
    private String answer2;

    private Qualification qualification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.education_qualification_question_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        //account = getIntent().getParcelableExtra("ACCOUNT");
        this.qualification = getIntent().getParcelableExtra("QUALIFICATION");
    }

    private void initUi() {
        question1Text = (TextView) findViewById(R.id.question1_text);
        question2Text = (TextView) findViewById(R.id.question2_text);

        question1 = (EditText) findViewById(R.id.question1);
        question2 = (EditText) findViewById(R.id.question2);

        question1Text.setTypeface(typeface, Typeface.NORMAL);
        question2Text.setTypeface(typeface, Typeface.NORMAL);

        question1.setTypeface(typeface, Typeface.NORMAL);
        question2.setTypeface(typeface, Typeface.NORMAL);

        question1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusEducationalExperience();
            }
        });
        question1.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusEducationalExperience();
                }
            }
        });

        Button yes = (Button) findViewById(R.id.register_btn);
        yes.setTypeface(typeface, Typeface.BOLD);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickNext();
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Educational Qualifications");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void onFocusEducationalExperience() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("Yes", ""));
        list.add(new Category("No", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                question1.setText(category.getName());
                if (category.getName().equalsIgnoreCase("No")) {
                    question2.setEnabled(false);
                    question2Text.setEnabled(false);
                    question2.setText("NA");
                } else {
                    question2.setText("");
                    question2.setEnabled(true);
                    question2Text.setEnabled(true);
                }
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Educational Qualification");

        dialog.show();
    }

    private void onClickNext() {
        answer1 = question1.getText().toString().trim().toLowerCase();
        answer2 = (answer1.equalsIgnoreCase("yes")) ? question2.getText().toString().trim().toLowerCase() : "NA";

        if (answer1.isEmpty() || answer2.isEmpty()) {
            displayInformationMessageDialog("Error", "You need to answer for all questions to complete the profile.");
        } else {
            qualification.setHasDiploma(answer1.equalsIgnoreCase("Yes"));
            qualification.setNameDiploma(answer2);

            navigateNext();
        }
    }

    private void navigateNext() {
        Intent intent = new Intent(EductionQualificationQuestionActivity.this, ProfessionalQualificationQuestionActivity.class);
        intent.putExtra("QUALIFICATION", qualification);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}

