package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.PowerManager;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.ImageUtil;


public class NewPromizeActivity extends BaseActivity {
    protected static final String TAG = NewPromizeActivity.class.getName();

    // camera
    private Camera camera;
    private CameraPreview cameraPreview;
    private boolean isCameraOn;

    // layouts
    private FrameLayout previewLayout;
    private ImageView capturedPhoto;
    private RelativeLayout infoPanel;

    // buttons
    private FloatingActionButton capture;
    private FloatingActionButton send;
    private ImageView camClose;

    private PowerManager.WakeLock wakeLock;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_promize_activity_layout);

        // init
        initUi();
        initCamera();
    }

    @Override
    protected void onResume() {
        super.onResume();

        // init camera with front
        acquireWakeLock();
        initCameraPreview(Camera.CameraInfo.CAMERA_FACING_FRONT);
    }

    @Override
    protected void onPause() {
        super.onPause();

        releaseWakeLock();
        releaseCameraPreview();
        ActivityUtil.cancelProgressDialog();
    }

    private void initUi() {
        isCameraOn = false;
        previewLayout = (FrameLayout) findViewById(R.id.preview_frame);
        capturedPhoto = (ImageView) findViewById(R.id.captured_photo);
        infoPanel = (RelativeLayout) findViewById(R.id.info_panel);

        capture = (FloatingActionButton) findViewById(R.id.fab);
        capture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtil.hideSoftKeyboard(NewPromizeActivity.this);
                capture();
            }
        });

        send = (FloatingActionButton) findViewById(R.id.send_i);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtil.hideSoftKeyboard(NewPromizeActivity.this);
                send();
            }
        });

        camClose = (ImageView) findViewById(R.id.cam_close);
        camClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtil.hideSoftKeyboard(NewPromizeActivity.this);
                closeCam();
            }
        });

        capture.setVisibility(View.GONE);
        camClose.setVisibility(View.GONE);
        infoPanel.setVisibility(View.VISIBLE);
    }

    private void initCamera() {
        isCameraOn = true;
        initCameraPreview(Camera.CameraInfo.CAMERA_FACING_BACK);

        capture.setVisibility(View.VISIBLE);
        camClose.setVisibility(View.VISIBLE);

        infoPanel.setVisibility(View.GONE);
        Animation a = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_out);
        infoPanel.startAnimation(a);
    }

    private void acquireWakeLock() {
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "SenzWakeLock");
        wakeLock.acquire();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void releaseWakeLock() {
        wakeLock.release();

        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    private void initCameraPreview(int camFace) {
        if (isCameraOn) {
            try {
                camera = Camera.open(camFace);
                cameraPreview = new CameraPreview(this, camera, camFace);
                previewLayout.addView(cameraPreview);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void releaseCameraPreview() {
        if (isCameraOn) {
            try {
                if (camera != null) {
                    cameraPreview.surfaceDestroyed(cameraPreview.getHolder());
                    cameraPreview.getHolder().removeCallback(cameraPreview);
                    cameraPreview.destroyDrawingCache();
                    previewLayout.removeView(cameraPreview);

                    camera.stopPreview();
                    camera.release();
                    camera = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void capture() {
        camera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] bytes, Camera camera) {
                byte[] resizedImage = ImageUtil.compressImg(bytes, true, true);
                releaseCameraPreview();
                isCameraOn = false;

                capturedPhoto.setVisibility(View.VISIBLE);
                Bitmap bitmap = ImageUtil.bytesToBmp(resizedImage);
                capturedPhoto.setImageBitmap(bitmap);

                capture.setVisibility(View.GONE);
                infoPanel.setVisibility(View.VISIBLE);
                Animation a = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_in);
                infoPanel.startAnimation(a);
            }
        });
    }

    private void send() {
        Toast.makeText(this, "Successfully uploaded the document", Toast.LENGTH_LONG).show();
        this.finish();
    }

    private void closeCam() {
        releaseCameraPreview();
        isCameraOn = false;

        capture.setVisibility(View.GONE);
        camClose.setVisibility(View.GONE);

        infoPanel.setVisibility(View.VISIBLE);
        Animation a = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.bottom_in);
        infoPanel.startAnimation(a);
    }

}