package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.db.BillerSource;
import com.rahasak.connect.enums.CustomerActionType;
import com.rahasak.connect.pojo.Biller;

import java.util.ArrayList;

public class MbslPaymentListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ArrayList<Biller> billerList;
    private PaymentListAdapter paymentListAdapter;
    private RelativeLayout emptyView;
    private ListView accountListView;
    private Typeface typeface;

    private CustomerActionType actionType = CustomerActionType.CUSTOMER_LIST;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.payment_list_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        initNewButton(view);
        initListView(view);
        refreshList();
        initSearchView(view);

        return view;
    }

    private void initNewButton(View view) {
        // new
        FloatingActionButton newCustomer = (FloatingActionButton) view.findViewById(R.id.new_account);
        newCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // navigate to contact list
                Intent intent = new Intent(getActivity(), PaymentTypeActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initSearchView(View view) {
        LinearLayout search = (LinearLayout) view.findViewById(R.id.search_layout);
        EditText searchView = (EditText) view.findViewById(R.id.inputSearch);
        if (billerList.size() == 0) {
            search.setVisibility(View.GONE);
        } else {
            search.setVisibility(View.VISIBLE);
            searchView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    paymentListAdapter.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    private void initListView(View view) {
        accountListView = (ListView) view.findViewById(R.id.customer_list_view);
        accountListView.setOnItemClickListener(this);
        accountListView.setOnItemLongClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        TextView emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);
    }

    private void refreshList() {
        billerList = BillerSource.getBillers(getActivity());
        if (billerList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            accountListView.setEmptyView(emptyView);
        } else {
            emptyView.setVisibility(View.GONE);
        }

        paymentListAdapter = new PaymentListAdapter(getActivity(), billerList);
        accountListView.setAdapter(paymentListAdapter);
        paymentListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Biller biller = billerList.get(position);

        // TODO navigate to func trans
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        if (actionType == CustomerActionType.CUSTOMER_LIST) {
            final Biller biller = billerList.get(position);
//            displayConfirmationMessageDialog("Confirm", "Are you sure your want to remove the biller?", new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    // delete item
//                    billerList.remove(position);
//                    paymentListAdapter.notifyDataSetChanged();
//
//                    // TODO delete from db
//                }
//            });
        }

        return true;
    }

}
