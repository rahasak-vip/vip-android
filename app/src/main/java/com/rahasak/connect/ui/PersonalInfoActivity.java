package com.rahasak.connect.ui;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.exceptions.InvalidNicNumberException;
import com.rahasak.connect.pojo.Category;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.util.ActivityUtil;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class PersonalInfoActivity extends BaseActivity implements DatePickerDialog.OnDateSetListener {

    // ui controls
    private EditText nic;
    private EditText name;
    private EditText gender;
    private EditText age;
    private EditText dob;
    private Button nextButton;

    private Identity identity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_info_layout);

        initUi();
        initToolbar();
        initActionBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Personal details");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        nic = (EditText) findViewById(R.id.registering_nic);
        name = (EditText) findViewById(R.id.registering_name);
        gender = (EditText) findViewById(R.id.registering_gender);
        age = (EditText) findViewById(R.id.registering_age);
        dob = (EditText) findViewById(R.id.registering_dob);

        nic.setTypeface(typeface, Typeface.NORMAL);
        name.setTypeface(typeface, Typeface.NORMAL);
        gender.setTypeface(typeface, Typeface.NORMAL);
        age.setTypeface(typeface, Typeface.NORMAL);
        dob.setTypeface(typeface, Typeface.NORMAL);

        dob.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusDate();
                }
            }
        });
        dob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusDate();
            }
        });

        dob.addTextChangedListener(new TextWatcher() {
            int prevL = 0;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                prevL = dob.getText().toString().length();
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int length = editable.length();
                if ((prevL < length) && (length == 2 || length == 5)) {
                    editable.append("-");
                }
            }
        });

        gender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusGender();
            }
        });
        gender.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusGender();
                }
            }
        });

        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setTypeface(typeface, Typeface.BOLD);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickNext();
            }
        });
    }

    private void onFocusGender() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("Male", ""));
        list.add(new Category("Female", ""));
        list.add(new Category("Other", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                gender.setText(category.getName());
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Select Gender");

        dialog.show();
    }

    private void onFocusDate() {
        DatePickerFragment fragment = new DatePickerFragment();
        fragment.show(getSupportFragmentManager(), "date");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(year, month, day);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        String selectedDate = sdf.format(cal.getTime());
        dob.setText(selectedDate);
    }

    private void onClickNext() {
        ActivityUtil.hideSoftKeyboard(this);

        final String ni = nic.getText().toString().trim().toUpperCase();
        final String nm = name.getText().toString().trim();
        final String ge = gender.getText().toString().trim();
        final String ag = age.getText().toString().trim();
        final String d = dob.getText().toString().trim();

        try {
            ActivityUtil.validatePersonalInfo(ni, nm, ge, ag, d);
            identity = new Identity();
            identity.setDid(ni);
            identity.setOwner(AccountContractExecutor.OWNER);
            identity.setNic(ni);
            identity.setName(nm);
            identity.setGender(ge);
            identity.setAge(Integer.parseInt(ag));
            identity.setDob(d);

            navigateNext(identity);
        } catch (NumberFormatException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid age. Please enter valid age.");
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all fields to complete the registration.");
        } catch (InvalidNicNumberException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error",e.toString()) ;
    }
    }

    private void navigateNext(Identity identity) {
        Intent intent = new Intent(PersonalInfoActivity.this, ContactInfoActivity.class);
        intent.putExtra("IDENTITY", identity);
        PersonalInfoActivity.this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}
