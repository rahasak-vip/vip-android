package com.rahasak.connect.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.ConnectNotificationMessage;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;


public class HomeActivity extends BaseActivity implements IContractExecutorListener {

    private TextView titleTextView;

    private BroadcastReceiver msgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("NOTIFICATION_MESSAGE")) {
                ConnectNotificationMessage msg = intent.getExtras().getParcelable("NOTIFICATION_MESSAGE");
                handleNotificationMessage(msg);
            }
        }
    };

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.home_layout);
        //initToolbar();
        //initActionBar();
        initBottomFragment();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //initToolbar();
        //initActionBar();
        initBottomFragment();

        ConnectNotificationMessage msg = intent.getExtras().getParcelable("NOTIFICATION_MESSAGE");
        handleNotificationMessage(msg);
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (!SenzApplication.isLogin()) {
//            displayInformationMessageDialogConfirm("Session timeout", "Your session has expired, please login again", new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
//                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                    startActivity(intent);
//                    overridePendingTransition(R.anim.right_in, R.anim.stay_in);
//                    finishAffinity();
//                }
//            });
//        } else {
//            registerReceiver(msgReceiver, new IntentFilter(IntentProvider.ACTION_SENZ));
//        }
    }

    @Override
    public void onPause() {
        super.onPause();
        //if (msgReceiver != null) unregisterReceiver(msgReceiver);
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        titleTextView = (TextView) findViewById(R.id.title);
        titleTextView.setTypeface(typeface, Typeface.BOLD);
        titleTextView.setText("Identity");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initBottomFragment() {
        // first show wallet
        changeFragment(new ConnectIdentityFragment(), ConnectIdentityFragment.class.getSimpleName());

        BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener
                (new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        Fragment selectedFragment = null;
                        switch (item.getItemId()) {
                            case R.id.navigation_offers:
                                //titleTextView.setText("Identity");
                                //selectedFragment = new ConnectIdentityFragment();
                                changeFragment(new ConnectIdentityFragment(), ConnectIdentityFragment.class.getSimpleName());

                                break;
                            case R.id.navigation_promize:
                                //titleTextView.setText("Qualifications");
                                //selectedFragment = new ConnectQualificationListFragment();
                                changeFragment(new ConnectQualificationListFragment(), ConnectQualificationListFragment.class.getSimpleName());

                                break;
                            case R.id.navigation_fund_transfer:
                                //titleTextView.setText("Consents");
                                //selectedFragment = new ConnectConsentListFragment();
                                changeFragment(new ConnectConsentListFragment(), ConnectConsentListFragment.class.getSimpleName());

                                break;
                            case R.id.navigation_settings:
                                //titleTextView.setText("Preference");
                                //selectedFragment = new ConnectSettingsFragment();
                                changeFragment(new ConnectSettingsFragment(), ConnectSettingsFragment.class.getSimpleName());

                                break;
                        }

//                        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
//                        transaction.replace(R.id.frame_layout, selectedFragment);
//                        transaction.commit();
                        return true;
                    }
                });
    }

    public void changeFragment(Fragment fragment, String tagFragmentName) {
        FragmentManager mFragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = mFragmentManager.beginTransaction();

        Fragment currentFragment = mFragmentManager.getPrimaryNavigationFragment();
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment);
        }

        Fragment fragmentTemp = mFragmentManager.findFragmentByTag(tagFragmentName);
        if (fragmentTemp == null) {
            fragmentTemp = fragment;
            fragmentTransaction.add(R.id.frame_layout, fragmentTemp, tagFragmentName);
        } else {
            fragmentTransaction.show(fragmentTemp);
        }

        fragmentTransaction.setPrimaryNavigationFragment(fragmentTemp);
        fragmentTransaction.setReorderingAllowed(true);
        fragmentTransaction.commitNowAllowingStateLoss();
    }

    @Override
    public void onFinishTask(String string) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 200) {
                    Toast.makeText(this, "Permissions updated", Toast.LENGTH_LONG).show();

                    // reload consent fragment
                    SenzApplication.setRefreshConsents(true);
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while updating permissions.");
        }
    }

    @Override
    public void onBackPressed() {
        displayConfirmationMessageDialog("Logout", "Confirm logout from Sportificate?", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SenzApplication.setLogin(false);
                SenzApplication.cancelLogoutTimer();

                Intent intent = new Intent(HomeActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                overridePendingTransition(R.anim.right_in, R.anim.stay_in);
                finishAffinity();
            }
        });
    }

    private void handleNotificationMessage(final ConnectNotificationMessage notificationMessage) {
        ActivityUtil.cancelProgressDialog();

        String msg = notificationMessage.getTracerName() + " wants to access your account details? Do you want to allow access ?";
        displayConfirmationMessageDialog("Allow access", msg, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // request to fetch consents
                allowPermission(notificationMessage);
            }
        });
    }

    private void allowPermission(ConnectNotificationMessage notificationMessage) {
        try {
            String did = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.DID);
            String owner = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.OWNER);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "traceConfirm");
            createMap.put("execer", did + ":" + owner);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("traceId", notificationMessage.getTraceId());
            createMap.put("accountDid", did);
            createMap.put("accountOwnerDid", owner);
            createMap.put("tracerDid", notificationMessage.getTracerDid());
            createMap.put("tracerOwnerDid", notificationMessage.getTracerDid());
            createMap.put("salt", notificationMessage.getSalt());

            ActivityUtil.showProgressDialog(this, "Updating permission...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRACE_API, SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
