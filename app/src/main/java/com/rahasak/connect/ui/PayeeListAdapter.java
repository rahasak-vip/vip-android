package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Payee;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


class PayeeListAdapter extends ArrayAdapter<Payee> {
    Context context;
    private Typeface typeface;

    PayeeListAdapter(Context _context, ArrayList<Payee> payeeList) {
        super(_context, R.layout.payee_list_row_layout, R.id.user_name, payeeList);
        context = _context;
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    /**
     * Create list row view
     *
     * @param i         index
     * @param view      current list item view
     * @param viewGroup parent
     * @return view
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        final ViewHolder holder;

        final Payee payee = getItem(i);

        if (view == null) {
            //inflate sensor list row layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.payee_list_row_layout, viewGroup, false);

            //create view holder to store reference to child views
            holder = new ViewHolder();
            holder.userImageView = (ImageView) view.findViewById(R.id.user_image);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.account = (TextView) view.findViewById(R.id.account);
            holder.bank = (TextView) view.findViewById(R.id.bank);
            holder.branch = (TextView) view.findViewById(R.id.branch);

            view.setTag(holder);
        } else {
            //get view holder back_icon
            holder = (ViewHolder) view.getTag();
        }

        setUpRow(payee, holder);

        return view;
    }

    private void setUpRow(Payee payee, ViewHolder viewHolder) {
        viewHolder.name.setTypeface(typeface, Typeface.BOLD);
        viewHolder.account.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.bank.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.branch.setTypeface(typeface, Typeface.NORMAL);

        // load contact image
        Picasso.with(context)
                .load(R.drawable.checkdb)
                .placeholder(R.drawable.checkdb)
                .into(viewHolder.userImageView);

        // text
        viewHolder.name.setText(payee.getName());
        viewHolder.account.setText("Account - " + payee.getAccountNo());
        viewHolder.bank.setText(payee.getBankName());
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    private static class ViewHolder {
        ImageView userImageView;
        TextView name;
        TextView account;
        TextView bank;
        TextView branch;
    }

}

