package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class QualificationQuestionActivity extends BaseActivity implements IContractExecutorListener {

    // UI fields
    private TextView question1Text;
    private TextView question2Text;
    private TextView question3Text;
    private TextView question4Text;
    private TextView question5Text;
    private TextView question6Text;
    private TextView question7Text;
    private EditText question1;
    private EditText question2;
    private EditText question3;
    private EditText question4;
    private EditText question5;
    private EditText question6;
    private EditText question7;

    private Account account;
    private String answer1;
    private String answer2;
    private String answer3;
    private String answer4;
    private String answer5;
    private String answer6;
    private String answer7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qualification_question_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        //account = getIntent().getParcelableExtra("ACCOUNT");
        account = PreferenceUtil.getAccount(this);
    }

    private void initUi() {
        question1Text = (TextView) findViewById(R.id.question1_text);
        question2Text = (TextView) findViewById(R.id.question2_text);
        question3Text = (TextView) findViewById(R.id.question3_text);
        question4Text = (TextView) findViewById(R.id.question4_text);
        question5Text = (TextView) findViewById(R.id.question5_text);
        question6Text = (TextView) findViewById(R.id.question6_text);
        question7Text = (TextView) findViewById(R.id.question7_text);
        question1 = (EditText) findViewById(R.id.question1);
        question2 = (EditText) findViewById(R.id.question2);
        question3 = (EditText) findViewById(R.id.question3);
        question4 = (EditText) findViewById(R.id.question4);
        question5 = (EditText) findViewById(R.id.question5);
        question6 = (EditText) findViewById(R.id.question6);
        question7 = (EditText) findViewById(R.id.question7);

        question1Text.setTypeface(typeface, Typeface.NORMAL);
        question2Text.setTypeface(typeface, Typeface.NORMAL);
        question3Text.setTypeface(typeface, Typeface.NORMAL);
        question4Text.setTypeface(typeface, Typeface.NORMAL);
        question5Text.setTypeface(typeface, Typeface.NORMAL);
        question6Text.setTypeface(typeface, Typeface.NORMAL);
        question7Text.setTypeface(typeface, Typeface.NORMAL);
        question1.setTypeface(typeface, Typeface.NORMAL);
        question2.setTypeface(typeface, Typeface.NORMAL);
        question3.setTypeface(typeface, Typeface.NORMAL);
        question4.setTypeface(typeface, Typeface.NORMAL);
        question5.setTypeface(typeface, Typeface.NORMAL);
        question6.setTypeface(typeface, Typeface.NORMAL);
        question7.setTypeface(typeface, Typeface.NORMAL);

        Button yes = (Button) findViewById(R.id.register_btn);
        yes.setTypeface(typeface, Typeface.BOLD);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateNext();
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Professional Qualifications");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void onClickSave() {
        // validate input
        answer1 = question1.getText().toString().trim().toLowerCase();
        answer2 = question2.getText().toString().trim().toLowerCase();
        answer3 = question3.getText().toString().trim().toLowerCase();

        if (answer1.isEmpty() || answer2.isEmpty() || answer3.isEmpty()) {
            displayInformationMessageDialog("Error", "You need to answer for all questions to complete the profile.");
        } else {
            ActivityUtil.hideSoftKeyboard(this);
            if (NetworkUtil.isAvailableNetwork(QualificationQuestionActivity.this)) {
                doUpdate(account, answer1, answer2, answer3);
            } else {
                Toast.makeText(QualificationQuestionActivity.this, "No network connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void doUpdate(Account account, String answer1, String answer2, String answer3) {
        try {
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "updateAnswers");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("answer1", answer1);
            createMap.put("answer2", answer2);
            createMap.put("answer3", answer3);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String string) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 200) {
                    Toast.makeText(this, "Successfully saved answers", Toast.LENGTH_LONG).show();
                    saveAnswers();
                    navigateToHome();
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while request.");
        }
    }

    private void navigateToSaltConfirmInfo() {
        Intent intent = new Intent(this, SaltConfirmInfoActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

    private void saveAnswers() {
        // save answers
        PreferenceUtil.put(QualificationQuestionActivity.this, PreferenceUtil.QUESTION1, answer1);
        PreferenceUtil.put(QualificationQuestionActivity.this, PreferenceUtil.QUESTION2, answer2);
        PreferenceUtil.put(QualificationQuestionActivity.this, PreferenceUtil.QUESTION3, answer3);
    }

    private void navigateToHome() {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        Intent intent = new Intent(this, HomeActivity.class);
        this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

    private void navigateNext() {
        Intent intent = new Intent(QualificationQuestionActivity.this, OlResultActivity.class);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}

