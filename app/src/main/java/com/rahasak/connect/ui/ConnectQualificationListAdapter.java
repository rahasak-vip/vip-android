package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Transaction;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class ConnectQualificationListAdapter extends ArrayAdapter<Transaction> {
    private Context context;
    private Typeface typeface;

    ConnectQualificationListAdapter(Context _context, ArrayList<Transaction> transactionList) {
        super(_context, R.layout.connect_consent_list_row_layout, R.id.user_name, transactionList);
        context = _context;
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    /**
     * Create list row view
     *
     * @param i         index
     * @param view      current list item view
     * @param viewGroup parent
     * @return view
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        final ViewHolder holder;

        final Transaction transaction = getItem(i);

        if (view == null) {
            //inflate sensor list row layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.connect_consent_list_row_layout, viewGroup, false);

            //create view holder to store reference to child views
            holder = new ViewHolder();
            holder.userImageView = (ImageView) view.findViewById(R.id.user_image);
            holder.account = (TextView) view.findViewById(R.id.account);
            holder.amount = (TextView) view.findViewById(R.id.amount);
            holder.date = (TextView) view.findViewById(R.id.date);
            holder.bank = (TextView) view.findViewById(R.id.bank);

            view.setTag(holder);
        } else {
            //get view holder back_icon
            holder = (ViewHolder) view.getTag();
        }

        setUpRow(transaction, holder);

        return view;
    }

    private void setUpRow(Transaction transaction, ViewHolder viewHolder) {
        viewHolder.account.setTypeface(typeface, Typeface.BOLD);
        viewHolder.date.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.amount.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.bank.setTypeface(typeface, Typeface.NORMAL);

        // set text
        viewHolder.account.setText(transaction.getUser());
        if (!transaction.getDate().isEmpty())
            viewHolder.date.setText(transaction.getDate());
        viewHolder.amount.setText(transaction.getAmount());
        viewHolder.bank.setText(transaction.getDescription());

        if (transaction.getDescription().equalsIgnoreCase("Pending")) {
            viewHolder.bank.setBackgroundResource(R.drawable.text_bgy);
            Picasso.with(context)
                    .load(R.drawable.waiting)
                    .placeholder(R.drawable.waiting)
                    .into(viewHolder.userImageView);
            viewHolder.bank.setText("Qualification not verified yet");
        } else {
            viewHolder.bank.setBackgroundResource(R.drawable.text_bg4);
            Picasso.with(context)
                    .load(R.drawable.checkg)
                    .placeholder(R.drawable.checkg)
                    .into(viewHolder.userImageView);
            viewHolder.bank.setText("Qualification verified");
        }

        // load image
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    private static class ViewHolder {
        ImageView userImageView;
        TextView account;
        TextView amount;
        TextView date;
        TextView bank;
    }

}

