package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidAccountException;
import com.rahasak.connect.exceptions.InvalidAnswerException;
import com.rahasak.connect.exceptions.InvalidPhoneNumberException;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.pojo.TokenReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class PasswordResetAccountDetailsActivity extends BaseActivity implements IContractExecutorListener {

    // ui controls
    private Button registerBtn;
    private TextView nicText;
    private TextView phoneText;
    private TextView accountText;
    private TextView question1Text;
    private TextView question2Text;
    private TextView question3Text;
    private EditText question1;
    private EditText question2;
    private EditText question3;
    private EditText editTextPhone;
    private EditText editTextNic;
    private EditText editTextAccount;

    private Account account;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_reset_account_details_activity);

        initUi();
        initToolbar();
        initActionBar();
        initPrefs();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Reset password");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        nicText = (TextView) findViewById(R.id.nic_info);
        phoneText = (TextView) findViewById(R.id.phone_info);
        accountText = (TextView) findViewById(R.id.account_info);
        question1Text = (TextView) findViewById(R.id.question1_text);
        question2Text = (TextView) findViewById(R.id.question2_text);
        question3Text = (TextView) findViewById(R.id.question3_text);

        editTextPhone = (EditText) findViewById(R.id.registering_phone_no);
        editTextNic = (EditText) findViewById(R.id.registering_nic);
        editTextAccount = (EditText) findViewById(R.id.registering_account);
        question1 = (EditText) findViewById(R.id.question1);
        question2 = (EditText) findViewById(R.id.question2);
        question3 = (EditText) findViewById(R.id.question3);

        question1Text.setTypeface(typeface, Typeface.NORMAL);
        question2Text.setTypeface(typeface, Typeface.NORMAL);
        question3Text.setTypeface(typeface, Typeface.NORMAL);
        nicText.setTypeface(typeface, Typeface.NORMAL);
        phoneText.setTypeface(typeface, Typeface.NORMAL);
        accountText.setTypeface(typeface, Typeface.NORMAL);

        question1.setTypeface(typeface, Typeface.NORMAL);
        question2.setTypeface(typeface, Typeface.NORMAL);
        question3.setTypeface(typeface, Typeface.NORMAL);
        editTextPhone.setTypeface(typeface, Typeface.NORMAL);
        editTextNic.setTypeface(typeface, Typeface.NORMAL);
        editTextAccount.setTypeface(typeface, Typeface.NORMAL);

        registerBtn = (Button) findViewById(R.id.register_btn);
        registerBtn.setTypeface(typeface, Typeface.BOLD);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickReset();
            }
        });
    }

    private void initPrefs() {
        Account acc = PreferenceUtil.getAccount(this);
        if (!acc.getNic().isEmpty()) {
            editTextNic.setText(acc.getNic());
            editTextNic.setVisibility(View.GONE);
            nicText.setVisibility(View.GONE);
        } else {
            editTextNic.setFocusable(true);
            editTextNic.setFocusableInTouchMode(true);
            editTextNic.setEnabled(true);
            editTextNic.setVisibility(View.VISIBLE);
            nicText.setVisibility(View.VISIBLE);
        }
    }

    private void onClickReset() {
        ActivityUtil.hideSoftKeyboard(this);

        // crate account
        final String nic = editTextNic.getText().toString().trim().toUpperCase();
        final String phn = editTextPhone.getText().toString().trim();
        final String acc = editTextAccount.getText().toString().trim();
        final String answer1 = question1.getText().toString().trim().toLowerCase();
        final String answer2 = question2.getText().toString().trim().toLowerCase();
        final String answer3 = question3.getText().toString().trim().toLowerCase();

        try {
            ActivityUtil.isValidResetPasswordInfo(nic, acc, phn, answer1, answer2, answer3);
            String formattedPhone = "+94" + phn.substring(1);
            account = new Account();
            account.setId(nic);
            account.setNic(nic);
            account.setNo(acc);
            account.setPhone(formattedPhone);
            doConfirm(account, answer1, answer2, answer3);
        } catch (InvalidPhoneNumberException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid phone no. Phone no should contain 10 digits and start with 07");
        } catch (InvalidAccountException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill account no to reset the password.");
        } catch (InvalidAnswerException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Please answer for two security questions to reset the Password");
        }
    }

    private void doConfirm(Account account, String a1, String a2, String a3) {
        try {
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "resetPassword");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("accountNo", account.getNo());
            createMap.put("accountNic", account.getNic());
            createMap.put("accountPhone", account.getPhone());
            createMap.put("answer1", a1);
            createMap.put("answer2", a2);
            createMap.put("answer3", a3);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                if (response.getStatus() == 200) {
                    TokenReply tokenReply = JsonUtil.toTokenReply(response.getPayload());
                    navigateToPasswordResetInfo(tokenReply, account);
                } else {
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while request.");
        }
    }

    private void navigateToPasswordResetInfo(TokenReply tokenReply, Account account) {
        Intent intent = new Intent(this, PasswordResetConfirmInfoActivity.class);
        intent.putExtra("ACCOUNT", account);
        intent.putExtra("TOKEN", tokenReply.getToken());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

}
