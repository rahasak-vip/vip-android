package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidPasswordException;
import com.rahasak.connect.exceptions.MisMatchFieldException;
import com.rahasak.connect.exceptions.SamePasswordException;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.TokenReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class PasswordChangeActivity extends BaseActivity implements IContractExecutorListener {

    private static final String TAG = PasswordChangeActivity.class.getName();

    private EditText current_password;
    private EditText new_password;
    private EditText new_confirm_password;
    private Button update_btn;

    private Account account;
    private String currentPassword;
    private String newPassword;
    private String newHashedPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_change);

        initUi();
        initToolbar();
        initActionBar();
    }

    private void initUi() {
        // text views
        current_password = (EditText) findViewById(R.id.current_password);
        new_password = (EditText) findViewById(R.id.new_password);
        new_confirm_password = (EditText) findViewById(R.id.new_confirm_password);
        current_password.setTypeface(typeface, Typeface.NORMAL);
        new_password.setTypeface(typeface, Typeface.NORMAL);
        new_confirm_password.setTypeface(typeface, Typeface.NORMAL);

        // buttons
        update_btn = (Button) findViewById(R.id.update_btn);
        update_btn.setTypeface(typeface, Typeface.BOLD);
        update_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickPasswordChange();
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Change Password");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void onClickPasswordChange() {
        try {
            currentPassword = current_password.getText().toString().trim();
            String currentHashedPassword = CryptoUtil.hashSha256(currentPassword);

            newPassword = new_password.getText().toString().trim();
            newHashedPassword = CryptoUtil.hashSha256(newPassword);
            String newConfirmPassword = new_confirm_password.getText().toString().trim();

            account = PreferenceUtil.getAccount(this);
            if (currentHashedPassword.equals(account.getPassword())) {
                ActivityUtil.isValidChangePassword(currentPassword, newPassword, newConfirmPassword);
                doChangePass(account.getId(), currentPassword, newPassword);
            } else {
                displayInformationMessageDialog("Error", "Current password is incorrect");
            }
        } catch (InvalidPasswordException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid password. Password should contain minimum 7 characters with special character");
        } catch (MisMatchFieldException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Your password does not matched with the confirm password");
        } catch (SamePasswordException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Your old password and new password are same, please choose a different new password");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid current password ");
        }
    }

    private void doChangePass(String accountId, String oldPassword, String newPassword) {
        try {
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "changePassword");
            createMap.put("execer", accountId);
            createMap.put("id", accountId + System.currentTimeMillis());
            createMap.put("accountId", accountId);
            createMap.put("oldPassword", CryptoUtil.hashSha256(oldPassword));
            createMap.put("newPassword", CryptoUtil.hashSha256(newPassword));

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            TokenReply tokenReply = JsonUtil.toTokenReply(response.getPayload());
            if (tokenReply.getStatus() == 200) {
                doneChangePassword(tokenReply);
            } else {
                ActivityUtil.cancelProgressDialog();
                displayInformationMessageDialog("Error", "Something went wrong while changing password.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while changing password.");
        }
    }

    private void doneChangePassword(final TokenReply tokenReply) {
        displayInformationMessageDialogConfirm("Password changed", "Password is changed successfully", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SenzApplication.setLogin(false);

                // save prefs
                PreferenceUtil.put(PasswordChangeActivity.this, PreferenceUtil.ACCOUNT_PASSWORD, newHashedPassword);
                PreferenceUtil.put(PasswordChangeActivity.this, PreferenceUtil.TOKEN, tokenReply.getToken());

                navigateToLogin();
            }
        });
    }

    private void navigateToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

}
