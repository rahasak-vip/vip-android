package com.rahasak.connect.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Transaction;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class PromizeDetailsActivity extends BaseActivity {

    // UI fields
    private TextView promizeAccountText;
    private TextView promizeTypeText;
    private TextView payeeNameText;
    private TextView payeeAccountText;
    private TextView transactionAmountText;
    private TextView transactionTimeText;
    private TextView transactionRefText;
    private EditText promizeAccount;
    private EditText promizeType;
    private EditText payeeName;
    private EditText payeeAccount;
    private EditText transactionAmount;
    private EditText transactionTime;
    private EditText transactionRef;

    private Transaction transaction;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promize_details_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        transaction = getIntent().getParcelableExtra("TRANSACTION");
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        promizeAccountText = (TextView) findViewById(R.id.promize_account_text);
        promizeTypeText = (TextView) findViewById(R.id.promize_type_text);
        payeeNameText = (TextView) findViewById(R.id.payee_name_text);
        payeeAccountText = (TextView) findViewById(R.id.payee_account_text);
        transactionAmountText = (TextView) findViewById(R.id.transaction_amount_text);
        transactionTimeText = (TextView) findViewById(R.id.transaction_time_text);
        transactionRefText = (TextView) findViewById(R.id.transaction_ref_text);

        promizeAccount = findViewById(R.id.promize_account);
        promizeType = findViewById(R.id.promize_type);
        payeeName = findViewById(R.id.payee_name);
        payeeAccount = findViewById(R.id.payee_account);
        transactionAmount = findViewById(R.id.transaction_amount);
        transactionTime = findViewById(R.id.transaction_time);
        transactionRef = findViewById(R.id.transaction_ref);

        promizeAccountText.setTypeface(typeface, Typeface.NORMAL);
        payeeNameText.setTypeface(typeface, Typeface.NORMAL);
        promizeTypeText.setTypeface(typeface, Typeface.NORMAL);
        payeeAccountText.setTypeface(typeface, Typeface.NORMAL);
        transactionAmountText.setTypeface(typeface, Typeface.NORMAL);
        transactionTimeText.setTypeface(typeface, Typeface.NORMAL);
        transactionRefText.setTypeface(typeface, Typeface.NORMAL);

        promizeAccount.setTypeface(typeface, Typeface.NORMAL);
        payeeName.setTypeface(typeface, Typeface.NORMAL);
        promizeType.setTypeface(typeface, Typeface.NORMAL);
        payeeAccount.setTypeface(typeface, Typeface.NORMAL);
        transactionAmount.setTypeface(typeface, Typeface.NORMAL);
        transactionTime.setTypeface(typeface, Typeface.NORMAL);
        transactionRef.setTypeface(typeface, Typeface.NORMAL);

        promizeAccount.setText(transaction.getFrom());
        promizeType.setText(transaction.getDescription());
        payeeName.setText(transaction.getUser());
        if (transaction.getType().equalsIgnoreCase("Pay")) {
            // hide payee account
            payeeAccountText.setVisibility(View.GONE);
            payeeAccount.setVisibility(View.GONE);
        } else {
            // show account
            payeeAccountText.setVisibility(View.VISIBLE);
            payeeAccount.setVisibility(View.VISIBLE);

            payeeAccount.setText(transaction.getAccount().replaceAll("(?!^).(?!$)","*"));
        }
        transactionAmount.setText(transaction.getAmount());
        transactionTime.setText(transaction.getDate());

        if (transaction.getId() != null && transaction.getId().length() > 10) {
            transactionRef.setText(transaction.getId().substring(transaction.getId().length() - 10));
        }
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Promize details");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

}

