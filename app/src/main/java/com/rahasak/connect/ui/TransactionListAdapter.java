package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Transaction;
import com.rahasak.connect.util.PhoneBookUtil;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

class TransactionListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Transaction> transactionList;
    private Typeface typeface;

    TransactionListAdapter(Context _context, ArrayList<Transaction> transactionList) {
        this.context = _context;
        this.transactionList = transactionList;

        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    @Override
    public int getCount() {
        return transactionList.size();
    }

    @Override
    public Object getItem(int position) {
        return transactionList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Create list row view
     *
     * @param i         index
     * @param view      current list item view
     * @param viewGroup parent
     * @return view
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final ViewHolder holder;
        final Transaction transaction = (Transaction) getItem(i);

        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.transaction_list_row_layout, viewGroup, false);

            holder = new ViewHolder();
            holder.amount = (TextView) view.findViewById(R.id.amount);
            holder.sender = (TextView) view.findViewById(R.id.sender);
            holder.sentTime = (TextView) view.findViewById(R.id.sent_time);
            holder.userImage = (ImageView) view.findViewById(R.id.user_image);
            holder.selected = (ImageView) view.findViewById(R.id.selected);
            holder.unreadCount = (FrameLayout) view.findViewById(R.id.unread_msg_count);
            holder.unreadText = (TextView) view.findViewById(R.id.unread_msg_text);
            holder.depositText = (TextView) view.findViewById(R.id.deposit);
            holder.type = (TextView) view.findViewById(R.id.type);

            holder.sender.setTypeface(typeface, Typeface.BOLD);
            holder.amount.setTypeface(typeface, Typeface.NORMAL);
            holder.sentTime.setTypeface(typeface, Typeface.BOLD);
            holder.unreadText.setTypeface(typeface, Typeface.BOLD);
            holder.depositText.setTypeface(typeface);
            holder.type.setTypeface(typeface);

            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        setUpRow(transaction, holder);
        return view;
    }

    private void setUpRow(Transaction transaction, ViewHolder viewHolder) {
        // set username/name
        viewHolder.sender.setText(PhoneBookUtil.getContactName(context, transaction.getUser()));
        viewHolder.amount.setText("Rs " + transaction.getAmount());
        if (!transaction.getDate().isEmpty())
            viewHolder.sentTime.setText(transaction.getDate().substring(0, transaction.getDate().length() - 7));
        viewHolder.type.setText(transaction.getDescription());

        // load contact image
        Picasso.with(context)
                .load(R.drawable.checkdb)
                .placeholder(R.drawable.checkdb)
                .into(viewHolder.userImage);
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    private static class ViewHolder {
        TextView amount;
        TextView sender;
        TextView sentTime;
        ImageView userImage;
        ImageView selected;
        FrameLayout unreadCount;
        TextView unreadText;
        TextView depositText;
        TextView type;
    }
}
