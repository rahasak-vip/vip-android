package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.Transaction;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class ConnectActivityListFragment extends Fragment implements AdapterView.OnItemClickListener, IContractExecutorListener {

    private ArrayList<Transaction> promizeList;
    private ConnectActivityListAdapter adapter;
    private ListView listView;
    private RelativeLayout emptyView;
    private TextView emptyText;
    private Account account;

    protected Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connect_list_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initListView(view);
        //fetchActivities("0", "20");

        mockList();
    }

    @Override
    public void onStart() {
        super.onStart();

        if (SenzApplication.isRefreshPromize()) {
            fetchActivities("0", "20");
        }
    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (!hidden) {
//            if (SenzApplication.isRefreshPromize()) {
//                fetchTrans("0", "20");
//            }
//        }
//    }

    private void initListView(View view) {
        listView = (ListView) view.findViewById(R.id.cheque_list_view);
        listView.setOnItemClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);

        promizeList = new ArrayList<>();
        adapter = new ConnectActivityListAdapter(getActivity(), promizeList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    private void refreshView(ArrayList<Transaction> list) {
        // transactions
        //promizeList.addAll(list);
        promizeList = list;
        if (promizeList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            listView.setEmptyView(emptyView);
            emptyText.setText("No activity trace records found with your account.");
        } else {
            emptyView.setVisibility(View.GONE);
            adapter = new ConnectActivityListAdapter(getActivity(), promizeList);
            adapter.notifyDataSetChanged();
            listView.setAdapter(adapter);
        }
    }

    private void fetchActivities(String offset, String limit) {
        if (SenzApplication.isLogin()) {
            try {
                account = PreferenceUtil.getAccount(this.getActivity());
                String did = PreferenceUtil.get(getActivity(), PreferenceUtil.DID);
                String owner = PreferenceUtil.get(getActivity(), PreferenceUtil.OWNER);

                HashMap<String, Object> createMap = new HashMap<>();
                createMap.put("id", account.getId() + System.currentTimeMillis());
                createMap.put("execer", did + ":" + owner);
                createMap.put("messageType", "searchTrace");
                createMap.put("did", did);
                createMap.put("owner", owner);
                createMap.put("nameTerm", "");
                createMap.put("idTerm", "");
                createMap.put("didTerm", "");
                createMap.put("nic", "");
                createMap.put("phone", "");
                createMap.put("offset", offset);
                createMap.put("limit", limit);
                createMap.put("sort", "descending");

                SenzApplication.setRefreshPromize(false);

                ActivityUtil.showProgressDialog(getActivity(), "Fetching activities...");
                AccountContractExecutor task = new AccountContractExecutor(createMap, this);
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRACE_API, PreferenceUtil.get(getActivity(), PreferenceUtil.TOKEN));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //final Transaction transaction = promizeList.get(position);

        //Intent intent = new Intent(this.getActivity(), PromizeDetailsActivity.class);
        //intent.putExtra("TRANSACTION", transaction);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //startActivity(intent);
        //getActivity().overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response != null && response.getStatus() == 200) {
                ArrayList<Transaction> list = JsonUtil.toActivityResponse(response.getPayload());
                refreshView(list);
                SenzApplication.setRefreshPromize(false);
            } else {
                ActivityUtil.cancelProgressDialog();
                Toast.makeText(getActivity(), "Fail to fetch activities", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(getActivity(), "Fail to fetch activities", Toast.LENGTH_LONG).show();
        }
    }

    private void mockList() {
        // add sample list items
        ArrayList<Transaction> transactions = new ArrayList<>();
        //for (int i = 0; i < 10; i++) {
            final Transaction transaction1 = new Transaction();
            transaction1.setUser("Sampath bank");
            transaction1.setDate("2020/06/01");
            transaction1.setAmount("Kollupitiya, colombo 03");
            transaction1.setDescription("Verified");
            transaction1.setStatus("1");

            final Transaction transaction2 = new Transaction();
            transaction2.setUser("Colombo fort");
            transaction2.setDate("Pettah, colombo 01");
            transaction2.setAmount("2020/06/02");
            transaction2.setDescription("Verified");
            transaction2.setStatus("2");

        final Transaction transaction3 = new Transaction();
        transaction3.setUser("Colombo fort");
        transaction3.setDate("Pettah, colombo 01");
        transaction3.setAmount("2020/06/02");
        transaction3.setDescription("Verified");
        transaction3.setStatus("3");

            transactions.add(transaction1);
            transactions.add(transaction2);
        transactions.add(transaction3);
        //}

        refreshView(transactions);
    }
}
