package com.rahasak.connect.ui;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.NotificationMessage;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class SendPromizeInfoActivity extends BaseActivity implements IContractExecutorListener {

    private static final String TAG = SendPromizeInfoActivity.class.getName();

    // UI fields
    private TextView header;
    private TextView hi;
    private TextView message;

    private NotificationMessage notificationMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_promize_info_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        header = (TextView) findViewById(R.id.header_text);
        hi = (TextView) findViewById(R.id.hi_message);
        message = (TextView) findViewById(R.id.welcome_message);
        header.setTypeface(typeface, Typeface.BOLD);
        hi.setTypeface(typeface, Typeface.NORMAL);
        message.setTypeface(typeface, Typeface.NORMAL);

        // set mess
        String cur = String.format("%,.2f", Double.parseDouble(notificationMessage.getPromizeAmount()));
        hi.setText("Please confirm to complete Rs. " + cur + " Promize Transaction");

        Button yes = (Button) findViewById(R.id.yes);
        yes.setTypeface(typeface, Typeface.BOLD);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // post promize
                if (NetworkUtil.isAvailableNetwork(SendPromizeInfoActivity.this)) {
                    postPromize();
                } else {
                    Toast.makeText(SendPromizeInfoActivity.this, "No network connection", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button no = (Button) findViewById(R.id.no);
        no.setTypeface(typeface, Typeface.BOLD);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initPrefs() {
        notificationMessage = getIntent().getParcelableExtra("NOTIFICATION_MESSAGE");
        Log.i(TAG, "Notification message : " + notificationMessage);
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Confirm transaction");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setVisibility(View.INVISIBLE);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void postPromize() {
        try {
            Account account = PreferenceUtil.getAccount(this);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "approve");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("promizeId", notificationMessage.getPromizeId());
            createMap.put("promizeFrom", account.getId());
            createMap.put("promizeTo", notificationMessage.getPromizeUser());
            createMap.put("promizeAmount", notificationMessage.getPromizeAmount());
            createMap.put("promizeSalt", notificationMessage.getPromizeSalt());

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.PROMIZE_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 200) {
                    displayInformationMessageDialogConfirm("Successful", "Transaction is completed successfully", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SenzApplication.setRefreshWallet(true);
                            SenzApplication.setRefreshPromize(true);
                            SendPromizeInfoActivity.this.finish();
                        }
                    });
                } else {
                    ActivityUtil.cancelProgressDialog();
                    //finish();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            finish();
            displayInformationMessageDialog("Error", "Failed to do the transaction.");
        }
    }

}

