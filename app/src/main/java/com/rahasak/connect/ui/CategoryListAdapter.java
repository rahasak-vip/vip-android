package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Category;

import java.util.ArrayList;

class CategoryListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<Category> categoryList;

    private Typeface typeface;

    CategoryListAdapter(Context context, ArrayList<Category> list) {
        this.context = context;
        this.categoryList = list;

        this.typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    @Override
    public int getCount() {
        return categoryList.size();
    }

    @Override
    public Object getItem(int position) {
        return categoryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        final ViewHolder holder;
        final Category category = (Category) getItem(position);

        if (convertView == null) {
            //inflate sensor list row layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.category_list_row_layout, parent, false);

            //create view holder to store reference to child views
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);

            convertView.setTag(holder);
        } else {
            //get view holder back_icon
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(category.getName());
        holder.name.setTypeface(typeface);

        return convertView;
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    static class ViewHolder {
        TextView name;
    }

}
