package com.rahasak.connect.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class SendPromizeActivity extends BaseActivity {

    // UI fields
    private TextView amount1;
    private TextView amount2;
    private TextView amount5;
    private TextView amount10;
    private TextView amount20;
    private TextView amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.send_promize_layout);

        initUi();
        initToolbar();
        initActionBar();
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        amount1 = (TextView) findViewById(R.id.dollar10_text);
        amount2 = (TextView) findViewById(R.id.dollar20_text);
        amount5 = (TextView) findViewById(R.id.dollar50_text);
        amount10 = (TextView) findViewById(R.id.dollar100_text);
        amount20 = (TextView) findViewById(R.id.dollar200_text);
        amount = (TextView) findViewById(R.id.amount_text);
        amount1.setTypeface(typeface, Typeface.NORMAL);
        amount2.setTypeface(typeface, Typeface.NORMAL);
        amount5.setTypeface(typeface, Typeface.NORMAL);
        amount10.setTypeface(typeface, Typeface.NORMAL);
        amount20.setTypeface(typeface, Typeface.NORMAL);
        amount.setTypeface(typeface, Typeface.NORMAL);

        RelativeLayout button1 = (RelativeLayout) findViewById(R.id.dollar10_button);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SendPromizeActivity.this, PromizeQrCodeActivity.class);
                intent.putExtra("EXTRA", "500");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button2 = (RelativeLayout) findViewById(R.id.dollar20_button);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SendPromizeActivity.this, PromizeQrCodeActivity.class);
                intent.putExtra("EXTRA", "1000");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button3 = (RelativeLayout) findViewById(R.id.dollar50_button);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SendPromizeActivity.this, PromizeQrCodeActivity.class);
                intent.putExtra("EXTRA", "2000");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button4 = (RelativeLayout) findViewById(R.id.dollar100_button);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SendPromizeActivity.this, PromizeQrCodeActivity.class);
                intent.putExtra("EXTRA", "5000");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button5 = (RelativeLayout) findViewById(R.id.dollar200_button);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SendPromizeActivity.this, PromizeQrCodeActivity.class);
                intent.putExtra("EXTRA", "10000");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button6 = (RelativeLayout) findViewById(R.id.amount);
        button6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askAmount();
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Choose amount");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    public void askAmount() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_amount_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        // texts
        TextView title = (TextView) dialog.findViewById(R.id.title);
        final EditText amountText = (EditText) dialog.findViewById(R.id.amount);
        title.setTypeface(typeface, Typeface.BOLD);
        amountText.setTypeface(typeface, Typeface.NORMAL);

        // set ok button
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setTypeface(typeface, Typeface.BOLD);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String amount = amountText.getText().toString().trim();
                // TODO validate amount

                if (!amount.isEmpty()) {
                    Intent intent = new Intent(SendPromizeActivity.this, PromizeQrCodeActivity.class);
                    intent.putExtra("EXTRA", amount);
                    startActivity(intent);
                    overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);

                    dialog.cancel();
                } else {
                    Toast.makeText(SendPromizeActivity.this, "Invalid amount", Toast.LENGTH_LONG).show();
                }
            }
        });

        // cancel button
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setTypeface(typeface, Typeface.BOLD);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

}

