package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.connect.R;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class PaymentTypeActivity extends BaseActivity {

    // UI fields
    private TextView amount1;
    private TextView amount2;
    private TextView amount5;
    private TextView amount10;
    private TextView amount20;
    private TextView amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_type_layout);

        initUi();
        initToolbar();
        initActionBar();
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        amount1 = (TextView) findViewById(R.id.dollar10_text);
        amount2 = (TextView) findViewById(R.id.dollar20_text);
        amount5 = (TextView) findViewById(R.id.dollar50_text);
        amount10 = (TextView) findViewById(R.id.dollar100_text);
        amount20 = (TextView) findViewById(R.id.dollar200_text);
        amount = (TextView) findViewById(R.id.amount_text);
        amount1.setTypeface(typeface, Typeface.NORMAL);
        amount2.setTypeface(typeface, Typeface.NORMAL);
        amount5.setTypeface(typeface, Typeface.NORMAL);
        amount10.setTypeface(typeface, Typeface.NORMAL);
        amount20.setTypeface(typeface, Typeface.NORMAL);
        amount.setTypeface(typeface, Typeface.NORMAL);

        RelativeLayout button1 = (RelativeLayout) findViewById(R.id.dollar10_button);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentTypeActivity.this, PaymentTypeListActivity.class);
                intent.putExtra("BILL_TYPE", "Electercity");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button2 = (RelativeLayout) findViewById(R.id.dollar20_button);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentTypeActivity.this, PaymentTypeListActivity.class);
                intent.putExtra("BILL_TYPE", "Water");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button3 = (RelativeLayout) findViewById(R.id.dollar50_button);
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentTypeActivity.this, PaymentTypeListActivity.class);
                intent.putExtra("BILL_TYPE", "Internet");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button4 = (RelativeLayout) findViewById(R.id.dollar100_button);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentTypeActivity.this, PaymentTypeListActivity.class);
                intent.putExtra("BILL_TYPE", "Telephone");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });

        RelativeLayout button5 = (RelativeLayout) findViewById(R.id.dollar200_button);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PaymentTypeActivity.this, PaymentTypeListActivity.class);
                intent.putExtra("BILL_TYPE", "Television");
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Choose payment type");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setVisibility(View.VISIBLE);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

}

