package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.exceptions.InvalidPasswordException;
import com.rahasak.connect.exceptions.MisMatchFieldException;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class PasswordResetActivity extends BaseActivity implements IContractExecutorListener {

    // UI fields
    private EditText editTextSalt;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private Button resetButton;

    private Account account;
    private String token;
    private int retry = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.password_reset_salt_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        account = getIntent().getParcelableExtra("ACCOUNT");
        token = getIntent().getStringExtra("TOKEN");
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Reset password");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextSalt = (EditText) findViewById(R.id.verification_code);
        editTextPassword = (EditText) findViewById(R.id.password);
        editTextConfirmPassword = (EditText) findViewById(R.id.confirm_password);

        editTextSalt.setTypeface(typeface, Typeface.NORMAL);
        editTextPassword.setTypeface(typeface, Typeface.NORMAL);
        editTextConfirmPassword.setTypeface(typeface, Typeface.NORMAL);

        resetButton = (Button) findViewById(R.id.reset_btn);
        resetButton.setTypeface(typeface, Typeface.BOLD);
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (retry >= 3) {
                    displayInformationMessageDialogConfirm("Error", "Maximum no of retry attempts exceeded", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            navigateLogin();
                        }
                    });
                } else {
                    ActivityUtil.hideSoftKeyboard(PasswordResetActivity.this);
                    onClickReset();
                }
            }
        });
    }

    private void onClickReset() {
        ActivityUtil.hideSoftKeyboard(this);
        String password = editTextPassword.getText().toString().trim();
        String confirmPassword = editTextConfirmPassword.getText().toString().trim();
        String salt = editTextSalt.getText().toString().trim();
        try {
            ActivityUtil.isValidResetPassword(password, confirmPassword, salt);

            if (NetworkUtil.isAvailableNetwork(PasswordResetActivity.this)) {
                doReset(account, password, salt);
            } else {
                Toast.makeText(PasswordResetActivity.this, "No network connection", Toast.LENGTH_LONG).show();
            }
        } catch (InvalidPasswordException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid password. Password should contain minimum 7 characters with special character");
        } catch (MisMatchFieldException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Your password is not matched with the confirm password");
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Please fill all the fields to proceed the password reset.");
        }
    }

    private void doReset(Account account, String password, String salt) {
        try {
            password = CryptoUtil.hashSha256(password);
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "resetPasswordConfirm");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("newPassword", password);
            createMap.put("salt", salt);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, token);
            retry++;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 200) {
                    displayInformationMessageDialogConfirm("Password changed", "Password is changed Successfully.", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            resetDone();
                        }
                    });
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while request.");
        }
    }

    private void resetDone() {
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_ID, account.getId());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PASSWORD, account.getPassword());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NIC, account.getNic());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PHONE, account.getPhone());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NO, account.getNo());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_STATE, "VERIFIED");

        // navigate to login
        navigateLogin();
    }

    private void navigateLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

}

