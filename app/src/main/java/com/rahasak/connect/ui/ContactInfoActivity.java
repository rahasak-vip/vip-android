package com.rahasak.connect.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.exceptions.InvalidEmailException;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.exceptions.InvalidPhoneNumberException;
import com.rahasak.connect.pojo.Category;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ContactInfoActivity extends BaseActivity {

    // ui controls
    private EditText phone;
    private EditText email;
    private EditText province;
    private EditText district;
    private EditText address;
    private Button nextButton;

    private Identity identity;
    private Map provinces = new HashMap<String, ArrayList<String>>();
    private String selectedProvince = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_info_layout);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
        initProvinces();
    }

    private void initPrefs() {
        this.identity = getIntent().getParcelableExtra("IDENTITY");
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Contact details");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        phone = (EditText) findViewById(R.id.phone);
        email = (EditText) findViewById(R.id.email);
        province = (EditText) findViewById(R.id.province);
        district = (EditText) findViewById(R.id.district);
        address = (EditText) findViewById(R.id.address);

        phone.setTypeface(typeface, Typeface.NORMAL);
        email.setTypeface(typeface, Typeface.NORMAL);
        province.setTypeface(typeface, Typeface.NORMAL);
        district.setTypeface(typeface, Typeface.NORMAL);
        address.setTypeface(typeface, Typeface.NORMAL);

        province.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusProvince();
            }
        });
        province.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    onFocusProvince();
                }
            }
        });

        district.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusDistrict();
            }
        });
        district.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    onFocusDistrict();
                }
            }
        });

        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setTypeface(typeface, Typeface.BOLD);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickNext();
            }
        });
    }

    private void initProvinces() {
        InputStream is = getResources().openRawResource(R.raw.province);
        try {
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            String json = new String(buffer, "UTF-8");
            provinces = JsonUtil.toProvinces(json);
            System.out.println(provinces);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void onFocusProvince() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("Central", ""));
        list.add(new Category("Western", ""));
        list.add(new Category("Southern", ""));
        list.add(new Category("Northern", ""));
        list.add(new Category("Eastern", ""));
        list.add(new Category("NorthWestern", ""));
        list.add(new Category("NorthEastern", ""));
        list.add(new Category("NorthCentral", ""));
        list.add(new Category("Uwa", ""));
        list.add(new Category("Sabaragamuwa", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                province.setText(category.getName());
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Select Province");

        dialog.show();
    }

    private void onFocusDistrict() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("Colombo", ""));
        list.add(new Category("Gampaha", ""));
        list.add(new Category("Kaluthara", ""));
        list.add(new Category("Galle", ""));
        list.add(new Category("Mathara", ""));
        list.add(new Category("Hambanthota", ""));
        list.add(new Category("Kandy", ""));
        list.add(new Category("Matale", ""));
        list.add(new Category("Nuwara Eliya", ""));
        list.add(new Category("Badulla", ""));
        list.add(new Category("Monaragala", ""));
        list.add(new Category("Jaffna", ""));
        list.add(new Category("Batticalo", ""));
        list.add(new Category("Mulativ", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                district.setText(category.getName());
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Select District");

        dialog.show();
    }

    private void onClickNext() {
        ActivityUtil.hideSoftKeyboard(this);

        final String phn = phone.getText().toString().trim().toUpperCase();
        final String eml = email.getText().toString().trim();
        final String pro = province.getText().toString().trim();
        final String dist = district.getText().toString().trim();
        final String addr = address.getText().toString().trim();

        try {
            ActivityUtil.validateContactInfo(phn, eml, pro, dist, addr);
            String formattedPhone = "+94" + phn.substring(1);
            identity.setPhone(phn);
            identity.setEmail(eml);
            identity.setProvince(pro);
            identity.setDistrict(dist);
            identity.setAddress(addr);

            navigateNext(identity);
        } catch (InvalidPhoneNumberException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid phone no. Phone no should contain 10 digits and start with 0");
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all fields to complete the registration.");
        } catch (InvalidEmailException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error",e.toString());
        }
    }

    private void navigateNext(Identity identity) {
        Intent intent = new Intent(ContactInfoActivity.this, CapturePhotoInfoActivity.class);
        intent.putExtra("IDENTITY", identity);
        ContactInfoActivity.this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}
