package com.rahasak.connect.ui;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Category;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.pojo.Qualification;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class ProfessionalQualificationQuestionActivity extends BaseActivity implements IContractExecutorListener {

    // UI fields
    private TextView question3Text;
    private TextView question4Text;
    private TextView question5Text;
    private TextView question6Text;
    private TextView question7Text;

    private EditText question3;
    private EditText question4;
    private EditText question5;
    private EditText question6;
    private EditText question7;

    private String answer3;
    private String answer4;
    private String answer5;
    private String answer6;
    private String answer7;

    private Identity identity;
    private Qualification qualification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profession_qualification_question_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        identity = getIntent().getParcelableExtra("ACCOUNT");
        this.qualification = getIntent().getParcelableExtra("QUALIFICATION");
    }

    private void initUi() {
        question3Text = (TextView) findViewById(R.id.question3_text);
        question4Text = (TextView) findViewById(R.id.question4_text);
        question5Text = (TextView) findViewById(R.id.question5_text);
        question6Text = (TextView) findViewById(R.id.question6_text);
        question7Text = (TextView) findViewById(R.id.question7_text);

        question3 = (EditText) findViewById(R.id.question3);
        question4 = (EditText) findViewById(R.id.question4);
        question5 = (EditText) findViewById(R.id.question5);
        question6 = (EditText) findViewById(R.id.question6);
        question7 = (EditText) findViewById(R.id.question7);

        question3Text.setTypeface(typeface, Typeface.NORMAL);
        question4Text.setTypeface(typeface, Typeface.NORMAL);
        question5Text.setTypeface(typeface, Typeface.NORMAL);
        question6Text.setTypeface(typeface, Typeface.NORMAL);
        question7Text.setTypeface(typeface, Typeface.NORMAL);

        question3.setTypeface(typeface, Typeface.NORMAL);
        question4.setTypeface(typeface, Typeface.NORMAL);
        question5.setTypeface(typeface, Typeface.NORMAL);
        question6.setTypeface(typeface, Typeface.NORMAL);
        question7.setTypeface(typeface, Typeface.NORMAL);

        question3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusProfessionalExperience();
            }
        });
        question3.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusProfessionalExperience();
                }
            }
        });

        question5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusDuration();
            }
        });
        question5.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusDuration();
                }
            }
        });

        question6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusProfessionArea();
            }
        });
        question6.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusProfessionArea();
                }
            }
        });

        question7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFocusProfessionName();
            }
        });
        question7.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // show date picker
                    onFocusProfessionName();
                }
            }
        });

        Button yes = (Button) findViewById(R.id.register_btn);
        yes.setTypeface(typeface, Typeface.BOLD);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickSave();
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Professional Qualifications");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void onFocusProfessionalExperience() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("Yes", ""));
        list.add(new Category("No", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                question3.setText(category.getName());
                if (category.getName().equalsIgnoreCase("No")) {
                    question4.setEnabled(false);
                    question4Text.setEnabled(false);
                    question4.setText("NA");
                } else {
                    question4.setText("");
                    question4.setEnabled(true);
                    question4Text.setEnabled(true);
                }
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Professional Experience");

        dialog.show();
    }

    private void onFocusDuration() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("0-3 Months", ""));
        list.add(new Category("4-6 Months", ""));
        list.add(new Category("7-12 Months", ""));
        list.add(new Category("1-2 Years", ""));
        list.add(new Category("3-4 Years", ""));
        list.add(new Category("More than 4 Years", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                question5.setText(category.getName());
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Select Duration");

        dialog.show();
    }

    private void onFocusProfessionArea() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        final ArrayList list = new ArrayList<>();
        list.add(new Category("NA", ""));
        list.add(new Category("Sports Journalism", ""));
        list.add(new Category("Health Physical Fitness", ""));
        list.add(new Category("Recreational Sports", ""));
        list.add(new Category("Sporting Goods", ""));
        list.add(new Category("Sports Venue", ""));
        list.add(new Category("Tourism", ""));
        list.add(new Category("Sports Analyst", ""));
        list.add(new Category("Sports Team", ""));
        list.add(new Category("Other", ""));

        // list
        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                question7.setText("");
                question6.setText(category.getName());
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Profession Area");

        dialog.show();
    }

    private void onFocusProfessionName() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.category_list_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(true);
        dialog.setCancelable(true);

        ListView bankListView = (ListView) dialog.findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);
        ArrayList list = new ArrayList<>();
        final CategoryListAdapter adapter = new CategoryListAdapter(this, list);
        bankListView.setAdapter(adapter);

        if (question6.getText().toString().equalsIgnoreCase("Sports Journalism")) {
            list.clear();
            list.add(new Category("Editor", ""));
            list.add(new Category("Publisher", ""));
            list.add(new Category("Newspaper Sports Writer", ""));
            list.add(new Category("Online Sports Journalist", ""));
            list.add(new Category("Internet", ""));
            list.add(new Category("Sports Journalist", ""));
            list.add(new Category("Sports Photographer", ""));
            list.add(new Category("Radio Sports Producer", ""));
            list.add(new Category("Television Sports Producer", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Health Physical Fitness")) {
            list.clear();
            list.add(new Category("Physical Therapist", ""));
            list.add(new Category("Medical Assistant", ""));
            list.add(new Category("Sports Medicine Aide", ""));
            list.add(new Category("Trainer", ""));
            list.add(new Category("Assistant Trainer", ""));
            list.add(new Category("Physical Therapist Assistant", ""));
            list.add(new Category("Sports Massage Therapist", ""));
            list.add(new Category("Sports Physiotherapist", ""));
            list.add(new Category("Sports Nutritionist", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Recreational Sports")) {
            list.clear();
            list.add(new Category("Youth Sports Administrator", ""));
            list.add(new Category("Recreational Manager", ""));
            list.add(new Category("Director", ""));
            list.add(new Category("Recreational Aid", ""));
            list.add(new Category("Recreational Specialist", ""));
            list.add(new Category("Recreational Therapist", ""));
            list.add(new Category("Outdoor Recreational Planner", ""));
            list.add(new Category("Teen Coordinator", ""));
            list.add(new Category("Instructor", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Sporting Goods")) {
            list.clear();
            list.add(new Category("Manufacturer’s Representative", ""));
            list.add(new Category("Sporting Goods Store Manager", ""));
            list.add(new Category("Sporting Goods Salesperson", ""));
            list.add(new Category("Sports Store Business Owner", ""));
            list.add(new Category("Team Dealer", ""));
            list.add(new Category("Other", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Sports Venue")) {
            list.clear();
            list.add(new Category("Relations With Other Organizations", ""));
            list.add(new Category("Finance manager", ""));
            list.add(new Category("Marketing manager", ""));
            list.add(new Category("Legal Affairs", ""));
            list.add(new Category("Technology Operator", ""));
            list.add(new Category("Operations", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Tourism")) {
            list.clear();
            list.add(new Category("Online Adjunct Professor", ""));
            list.add(new Category("Other", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Sports Analyst")) {
            list.clear();
            list.add(new Category("Web Analytics Supervisor", ""));
            list.add(new Category("Associate Data Processor", ""));
            list.add(new Category("Supervising Editor", ""));
            list.add(new Category("Social Media Specialist", ""));
            list.add(new Category("Legislative Analyst", ""));
            list.add(new Category("Analytics Engagement Manager", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Sports Team")) {
            list.clear();
            list.add(new Category("Sports Coach", ""));
            list.add(new Category("Assistant Coach", ""));
            list.add(new Category("Physiotherapist", ""));
            list.add(new Category("Sports Marketing", ""));
            list.add(new Category("Trainer", ""));
            list.add(new Category("Assistant Trainer", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("Other")) {
            list.clear();
            list.add(new Category("PE Teacher", ""));
            list.add(new Category("Sports Lawyer", ""));
            list.add(new Category("Associate Athletic Director", ""));
            list.add(new Category("Physical Education Instructor", ""));
            list.add(new Category("Marketing and Promotions Coordinator", ""));
            list.add(new Category("Sports Program Development Director", ""));
            adapter.notifyDataSetChanged();
        } else if (question6.getText().toString().equalsIgnoreCase("NA")) {
            list.clear();
            list.add(new Category("NA", ""));
            adapter.notifyDataSetChanged();
        }

        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Category category = (Category) adapter.getItem(position);
                question7.setText(category.getName());
                dialog.cancel();
            }
        });

        // title
        TextView title = (TextView) dialog.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Select Profession area first");

        dialog.show();
    }

    private void onClickSave() {
        // validate input
        answer3 = question3.getText().toString().trim();
        answer4 = (answer3.equalsIgnoreCase("yes")) ? question4.getText().toString().trim() : "NA";
        answer5 = question5.getText().toString().trim();
        answer6 = question6.getText().toString().trim();
        answer7 = question7.getText().toString().trim();

        if (answer3.isEmpty() || (answer3.equalsIgnoreCase("yes") && answer4.isEmpty()) || answer5.isEmpty() || answer6.isEmpty() || answer7.isEmpty()) {
            displayInformationMessageDialog("Error", "You need to answer for all questions to complete the profile.");
        } else {
            qualification.setHasExperience(answer3.equalsIgnoreCase("yes"));
            qualification.setNameExperience(answer4);
            qualification.setDurationExperience(answer5);
            qualification.setProfessionArea(answer6);
            qualification.setProfessionName(answer7);

            ActivityUtil.hideSoftKeyboard(this);
            if (NetworkUtil.isAvailableNetwork(ProfessionalQualificationQuestionActivity.this)) {
                Log.d("Msg", "Netowrk availabe");
                createQualification();
            } else {
                Toast.makeText(ProfessionalQualificationQuestionActivity.this, "No network connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void createQualification() {
        try {
            String did = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.DID);
            String owner = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.OWNER);
            String excer = did + ":" + owner;
            String id = did + System.currentTimeMillis();

            HashMap<String, Object> olResult = new HashMap<>();
            olResult.put("olMathematics", qualification.getOlMethamatics());
            olResult.put("olEnglish", qualification.getOlEnglishg());
            olResult.put("olSinhala", qualification.getOlSinhala());
            olResult.put("olTamil", qualification.getOlTamil());
            olResult.put("olScience", qualification.getOlScience());
            olResult.put("olHealthScience", qualification.getOlHealthScience());
            olResult.put("olReligion", qualification.getOlReligion());
            olResult.put("olArt", qualification.getOlArt());
            olResult.put("olHistory", qualification.getOlHistory());

            HashMap<String, Object> edu = new HashMap<>();
            edu.put("hasDiploma", qualification.getHasDiploma());
            edu.put("nameDiploma", qualification.getNameDiploma());

            HashMap<String, Object> prof = new HashMap<>();
            prof.put("hasExperience", qualification.getHasExperience());
            prof.put("nameExperience", qualification.getNameExperience());
            prof.put("durationExperience", qualification.getDurationExperience());
            prof.put("professionArea", qualification.getProfessionArea());
            prof.put("professionName", qualification.getProfessionName());

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("execer", excer);
            createMap.put("id", id);
            createMap.put("messageType", "createQualification");
            createMap.put("qualificationId", id);
            createMap.put("qualificationHolder", did);
            createMap.put("olResult", olResult);
            createMap.put("educationalQualification", edu);
            createMap.put("professionalQualification", prof);

            String digis = CryptoUtil.getDigitalSignature(JsonUtil.toOrderedString(createMap), CryptoUtil.getPrivateKey(this));
            createMap.put("msgsig", digis);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.QUALIFICATION_API, SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String string) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 201) {
                    Toast.makeText(this, "Successfully added qualification", Toast.LENGTH_LONG).show();
                    navigateToHome();
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while request.");
        }
    }

    private void navigateToHome() {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        Intent intent = new Intent(this, HomeActivity.class);
        this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

}

