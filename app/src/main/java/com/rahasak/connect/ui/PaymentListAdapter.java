package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Biller;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


class PaymentListAdapter extends ArrayAdapter<Biller> {
    Context context;
    private Typeface typeface;

    PaymentListAdapter(Context _context, ArrayList<Biller> billerList) {
        super(_context, R.layout.payment_list_row_layout, R.id.user_name, billerList);
        context = _context;
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    /**
     * Create list row view
     *
     * @param i         index
     * @param view      current list item view
     * @param viewGroup parent
     * @return view
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        final ViewHolder holder;

        final Biller biller = getItem(i);

        if (view == null) {
            //inflate sensor list row layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.payment_list_row_layout, viewGroup, false);

            //create view holder to store reference to child views
            holder = new ViewHolder();
            holder.userImageView = (CircularImageView) view.findViewById(R.id.user_image);
            holder.selected = (ImageView) view.findViewById(R.id.selected);
            holder.usernameView = (TextView) view.findViewById(R.id.user_name);
            holder.phoneNoView = (TextView) view.findViewById(R.id.phoneno);
            holder.statusView = (TextView) view.findViewById(R.id.status);

            view.setTag(holder);
        } else {
            //get view holder back_icon
            holder = (ViewHolder) view.getTag();
        }

        setUpRow(biller, holder);

        return view;
    }

    private void setUpRow(Biller biller, ViewHolder viewHolder) {
        viewHolder.usernameView.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.phoneNoView.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.statusView.setTypeface(typeface, Typeface.BOLD);

        // load contact image
        Picasso.with(context)
                .load(R.drawable.copy)
                .placeholder(R.drawable.copy)
                .into(viewHolder.userImageView);

        // text
        viewHolder.usernameView.setText(biller.getBillType());
        viewHolder.phoneNoView.setText(biller.getBillerName());
        //viewHolder.statusView.setText(account.getBank());
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    private static class ViewHolder {
        CircularImageView userImageView;
        ImageView selected;
        TextView usernameView;
        TextView phoneNoView;
        TextView statusView;
    }

}

