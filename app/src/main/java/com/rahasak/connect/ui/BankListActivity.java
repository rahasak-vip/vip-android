package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Bank;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class BankListActivity extends BaseActivity implements IContractExecutorListener {

    private LinearLayout search;
    private EditText searchView;
    private ListView bankListView;
    private BankListAdapter adapter;
    private static ArrayList<Bank> bankList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);

        setupToolbar();
        setupActionBar();
        setupSearchView();
        initListView();
        getBanks();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Choose bank");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setupSearchView() {
        search = (LinearLayout) findViewById(R.id.search_view);
        searchView = (EditText) findViewById(R.id.inputSearch);
        searchView.setTypeface(typeface);
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initListView() {
        bankListView = (ListView) findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);

        // click listener
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Bank bank = (Bank) adapter.getItem(position);
                onSelectBank(bank);
            }
        });
    }

    private void refreshListView(ArrayList<Bank> banks) {
        bankList = banks;
        Collections.sort(bankList, new Comparator<Bank>() {
            public int compare(Bank o1, Bank o2) {
                return o1.getBankName().compareTo(o2.getBankName());
            }
        });

        if (bankList.size() > 0) {
            adapter = new BankListAdapter(this, bankList);
            bankListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            search.setVisibility(View.VISIBLE);
        } else {
            search.setVisibility(View.GONE);
        }
    }

    private void onSelectBank(final Bank bank) {
        if (bank.getBankCode().equalsIgnoreCase("7898")) {
            navigateToFundTransfer(bank);
        } else {
            String message = "When you transfer money to other bank accounts a charge of Rs 50.00 will be debited from your account as the commission. Transfer will be credited to given account at the end of the day as a SLIPS transfer";
            displayConfirmationMessageDialog("Confirm", message, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    navigateToBranchList(bank);
                }
            });
        }
    }

    private void getBanks() {
        try {
            Account account = PreferenceUtil.getAccount(this);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "getBanks");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());

            ActivityUtil.showProgressDialog(BankListActivity.this, "Fetching bank details...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRANSFER_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                Toast.makeText(BankListActivity.this, "Something went wrong while connecting", Toast.LENGTH_LONG).show();
            } else {
                if (response.getStatus() == 200) {
                    refreshListView(JsonUtil.toBankList(response.getPayload()));
                } else {
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    ActivityUtil.cancelProgressDialog();
                    Toast.makeText(BankListActivity.this, "Failed to fetch bank details", Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(BankListActivity.this, "Failed to fetch bank details", Toast.LENGTH_LONG).show();
        }
    }

    private void navigateToBranchList(final Bank bank) {
        Intent intent = new Intent(BankListActivity.this, BranchListActivity.class);
        intent.putExtra("ACCOUNT_BANK", bank);
        startActivity(intent);
        BankListActivity.this.finish();
    }

    private void navigateToFundTransfer(final Bank bank) {
        Intent intent = new Intent(BankListActivity.this, FundTransferMbslActivity.class);
        intent.putExtra("ACCOUNT_BANK", bank);
        startActivity(intent);
        BankListActivity.this.finish();
    }

}
