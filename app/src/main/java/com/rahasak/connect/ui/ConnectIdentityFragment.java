package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.ImageUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;
import java.util.Map;

public class ConnectIdentityFragment extends BaseFragment implements IContractExecutorListener {

    private static final String TAG = ConnectIdentityFragment.class.getName();

    TextView nic;
    TextView nicv;
    TextView name;
    TextView namev;
    TextView phone;
    TextView phonev;
    TextView email;
    TextView emailv;
    TextView status;
    TextView statusv;
    ImageView image;

    private Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_prof, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUi(view);
        initToolbar(view);
        if (SenzApplication.isRefreshWallet()) {
            fetchIdentity();
        } else {
            updateIdentityInfo(SenzApplication.getIdentity());
        }
        //initQrCodeContent();
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            // fragment will show
            if (SenzApplication.isRefreshWallet()) {
                fetchIdentity();
            }
        }
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        if (SenzApplication.isRefreshWallet()) {
//            fetchIdentity();
//        }
//    }

    private void initUi(View view) {
        nic = (TextView) view.findViewById(R.id.nic);
        nicv = (TextView) view.findViewById(R.id.nicv);
        name = (TextView) view.findViewById(R.id.name);
        namev = (TextView) view.findViewById(R.id.namev);
        phone = (TextView) view.findViewById(R.id.phone);
        phonev = (TextView) view.findViewById(R.id.phonev);
        email = (TextView) view.findViewById(R.id.email);
        emailv = (TextView) view.findViewById(R.id.emailv);
        status = (TextView) view.findViewById(R.id.tax);
        statusv = (TextView) view.findViewById(R.id.taxv);

        // set font
        nic.setTypeface(typeface, Typeface.NORMAL);
        nicv.setTypeface(typeface, Typeface.NORMAL);
        name.setTypeface(typeface, Typeface.NORMAL);
        namev.setTypeface(typeface, Typeface.NORMAL);
        phone.setTypeface(typeface, Typeface.NORMAL);
        phonev.setTypeface(typeface, Typeface.NORMAL);
        email.setTypeface(typeface, Typeface.NORMAL);
        emailv.setTypeface(typeface, Typeface.NORMAL);
        status.setTypeface(typeface, Typeface.NORMAL);
        statusv.setTypeface(typeface, Typeface.NORMAL);

        image = getActivity().findViewById(R.id.chat_cam);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SenzApplication.getIdentity() != null) {
                    Intent intent = new Intent(getActivity(), PhotoPreviewActivity.class);
                    intent.putExtra("IDENTITY", SenzApplication.getIdentity());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }
            }
        });
    }

    private void initToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        View header = getLayoutInflater().inflate(R.layout.profile_header, null);
        toolbar.setContentInsetsAbsolute(0, 0);
        toolbar.addView(header);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        TextView title = (TextView) header.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Identity");

        ImageView backImageView = (ImageView) header.findViewById(R.id.back_btn);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // exit
            }
        });
    }

    private void fetchIdentity() {
        try {
            String did = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.DID);
            String owner = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.OWNER);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "get");
            createMap.put("execer", did + ":" + owner);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("did", did);
            createMap.put("owner", owner);

            String digis = CryptoUtil.getDigitalSignature(JsonUtil.toOrderedString(createMap), CryptoUtil.getPrivateKey(getActivity()));
            createMap.put("msgsig", digis);

            ActivityUtil.showProgressDialog(getActivity(), "Fetching settings...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String string) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                Toast.makeText(getActivity(), "Something went wrong while connecting", Toast.LENGTH_LONG).show();
            } else {
                if (response.getStatus() == 200) {
                    SenzApplication.setIdentity(JsonUtil.toIdentityResponse(response.getPayload()));
                    SenzApplication.setRefreshWallet(false);
                    updateIdentityInfo(SenzApplication.getIdentity());
                } else if (response.getStatus() == 401) {
                    ((HomeActivity) getActivity()).displayInformationMessageDialogConfirm("Session timeout", "Your session has expired, please login again", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getActivity(), LoginActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            getActivity().overridePendingTransition(R.anim.right_in, R.anim.stay_in);
                            getActivity().finishAffinity();
                        }
                    });
                } else {
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    ActivityUtil.cancelProgressDialog();
                    Toast.makeText(getActivity(), "Failed to fetch identity details", Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(getActivity(), "Failed to fetch identity details", Toast.LENGTH_LONG).show();
        }
    }

    private void updateIdentityInfo(Identity identity) {
        // set ui fields
        nicv.setText(identity.getNic());
        namev.setText(identity.getName());
        phonev.setText(identity.getPhone());

        if (identity.isVerified()) {
            statusv.setText("Identity verified");
        } else {
            statusv.setText("Verification pending");
        }

        // set image
        Bitmap bitmap = ImageUtil.decodeBmp(identity.getBlob());
        image.setImageBitmap(bitmap);
    }

    private void initQrCodeContent() {
        String did = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.DID);
        String owner = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.OWNER);
        String qrtext = did + ":" +
                owner + ":" +
                "salt" + ":" +
                "signature";
        generateQrCode(qrtext);
    }

    private void generateQrCode(String qrCodeContent) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            Map<EncodeHintType, Object> hintMap = new HashMap<EncodeHintType, Object>();
            hintMap.put(EncodeHintType.MARGIN, new Integer(1));
            BitMatrix bitMatrix = writer.encode(qrCodeContent, BarcodeFormat.QR_CODE, 512, 512, hintMap);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }

            ((ImageView) getActivity().findViewById(R.id.qr_code)).setImageBitmap(bmp);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

}
