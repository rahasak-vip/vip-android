package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.Branch;

import java.util.ArrayList;

class BranchListAdapter extends BaseAdapter implements Filterable {

    private Context context;
    private ListFilter listFilter;
    private ArrayList<Branch> branchList;
    private ArrayList<Branch> filteredBranchList;

    private Typeface typeface;

    BranchListAdapter(Context context, ArrayList<Branch> branchList) {
        this.context = context;
        this.branchList = branchList;
        this.filteredBranchList = branchList;

        this.typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    @Override
    public int getCount() {
        return filteredBranchList.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredBranchList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        final ViewHolder holder;

        final Branch branch = (Branch) getItem(position);

        if (convertView == null) {
            //inflate sensor list row layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.branch_list_row_layout, parent, false);

            //create view holder to store reference to child views
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.code = (TextView) convertView.findViewById(R.id.code);

            convertView.setTag(holder);
        } else {
            //get view holder back_icon
            holder = (ViewHolder) convertView.getTag();
        }

        holder.name.setText(branch.getBranchName());
        holder.code.setText(branch.getBranchCode());
        holder.name.setTypeface(typeface);
        holder.code.setTypeface(typeface);

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (listFilter == null) {
            listFilter = new ListFilter();
        }

        return listFilter;
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    static class ViewHolder {
        TextView name;
        TextView code;
    }

    /**
     * Custom filter for contact list
     * Filter content in contact list according to the search text
     */
    private class ListFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {
                ArrayList<Branch> tempList = new ArrayList<>();

                // search content in friend list
                for (Branch branch : branchList) {
                    if (branch.getBranchName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        tempList.add(branch);
                    }
                }

                filterResults.count = tempList.size();
                filterResults.values = tempList;
            } else {
                filterResults.count = branchList.size();
                filterResults.values = branchList;
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            filteredBranchList = (ArrayList<Branch>) results.values;
            notifyDataSetChanged();
        }
    }

}
