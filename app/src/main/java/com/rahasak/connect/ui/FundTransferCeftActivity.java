package com.rahasak.connect.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.db.PayeeSource;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.exceptions.MisMatchFieldException;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Bank;
import com.rahasak.connect.pojo.Branch;
import com.rahasak.connect.pojo.Payee;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class FundTransferCeftActivity extends BaseActivity implements IContractExecutorListener {

    private static final String TAG = FundTransferCeftActivity.class.getName();

    private EditText editTextBank;
    private EditText editTextBranch;
    private EditText editTextName;
    private EditText editTextAccount;
    private EditText editTextConfirmAccount;
    private EditText editTextAmount;

    private Bank bank;
    private Branch branch;
    private Payee payee;
    private String transferId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fund_transfer_layout);

        initUi();
        initPrefs();
        initToolbar();
        initActionBar();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Transfer money");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextBank = (EditText) findViewById(R.id.bank);
        editTextBranch = (EditText) findViewById(R.id.branch);
        editTextName = (EditText) findViewById(R.id.name);
        editTextAccount = (EditText) findViewById(R.id.account);
        editTextConfirmAccount = (EditText) findViewById(R.id.confirm_account);
        editTextAmount = (EditText) findViewById(R.id.amount);

        editTextBank.setTypeface(typeface, Typeface.NORMAL);
        editTextBranch.setTypeface(typeface, Typeface.NORMAL);
        editTextName.setTypeface(typeface, Typeface.NORMAL);
        editTextAccount.setTypeface(typeface, Typeface.NORMAL);
        editTextConfirmAccount.setTypeface(typeface, Typeface.NORMAL);
        editTextAmount.setTypeface(typeface, Typeface.NORMAL);

        // ui controls
        Button redeem = (Button) findViewById(R.id.done);
        redeem.setTypeface(typeface, Typeface.BOLD);
        redeem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActivityUtil.hideSoftKeyboard(FundTransferCeftActivity.this);
                onClickTransfer();
            }
        });
    }

    private void initPrefs() {
        if (getIntent().hasExtra("ACCOUNT_BANK")) {
            this.bank = getIntent().getParcelableExtra("ACCOUNT_BANK");
            this.branch = getIntent().getParcelableExtra("ACCOUNT_BRANCH");

            editTextBank.setText(this.bank.getBankName());
            editTextBranch.setText(this.branch.getBranchName());
        } else if (getIntent().hasExtra("PAYEE")) {
            this.payee = getIntent().getParcelableExtra("PAYEE");
            this.bank = new Bank(payee.getBankCode(), payee.getBankName());
            this.branch = new Branch(payee.getBranch(), payee.getBranchCode());

            editTextBank.setText(this.bank.getBankName());
            editTextBranch.setText(this.branch.getBranchName());
            editTextName.setText(this.payee.getName());
            editTextAccount.setText(payee.getAccountNo());
            editTextConfirmAccount.setText(payee.getAccountNo());

            // disabled edit
            editTextBank.setEnabled(false);
            editTextBank.setFocusable(false);
            editTextBank.setFocusableInTouchMode(false);
            editTextBranch.setEnabled(false);
            editTextBranch.setFocusable(false);
            editTextBranch.setFocusableInTouchMode(false);
            editTextName.setEnabled(false);
            editTextName.setFocusable(false);
            editTextName.setFocusableInTouchMode(false);
            editTextAccount.setEnabled(false);
            editTextAccount.setFocusable(false);
            editTextAccount.setFocusableInTouchMode(false);
            editTextConfirmAccount.setEnabled(false);
            editTextConfirmAccount.setFocusable(false);
            editTextConfirmAccount.setFocusableInTouchMode(false);
        }
    }

    private void onClickTransfer() {
        // crate account
        String toAcc = editTextAccount.getText().toString().trim();
        String confirmToAcc = editTextConfirmAccount.getText().toString().trim();
        String amount = editTextAmount.getText().toString().trim();
        String name = editTextName.getText().toString().trim();
        try {
            ActivityUtil.isValidFundTrans(bank.getBankCode(), toAcc, confirmToAcc, amount, name);
            if (NetworkUtil.isAvailableNetwork(FundTransferCeftActivity.this)) {
                String cur = String.format("%,.2f", Double.parseDouble(amount));
                displayConfirmationMessageDialog("Confirm transfer", "Please confirm transfer Rs " + cur + " to " + name, new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        askPassword();
                    }
                });
            } else {
                Toast.makeText(FundTransferCeftActivity.this, "No network connection", Toast.LENGTH_LONG).show();
            }
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all the fields to proceed this transaction..");
        } catch (MisMatchFieldException e) {
            displayInformationMessageDialog("Error", "Your Account number is not matched with Confirm account number.");
            e.printStackTrace();
        }
    }

    public void askPassword() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_password_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        // texts
        TextView title = (TextView) dialog.findViewById(R.id.title);
        final EditText password = (EditText) dialog.findViewById(R.id.password);
        title.setTypeface(typeface, Typeface.BOLD);
        password.setTypeface(typeface, Typeface.NORMAL);

        // set ok button
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setTypeface(typeface, Typeface.BOLD);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String givenPassword = password.getText().toString().trim();
                    String hashPass = CryptoUtil.hashSha256(givenPassword);
                    if (hashPass.equalsIgnoreCase(PreferenceUtil.getAccount(FundTransferCeftActivity.this).getPassword())) {
                        dialog.cancel();
                        ActivityUtil.hideSoftKeyboard(FundTransferCeftActivity.this);
                        transfer();
                    } else {
                        Toast.makeText(FundTransferCeftActivity.this, "Password is Incorrect", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(FundTransferCeftActivity.this, "Password is Incorrect", Toast.LENGTH_LONG).show();
                }
            }
        });

        // cancel button
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setTypeface(typeface, Typeface.BOLD);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void transfer() {
        try {
            Account account = PreferenceUtil.getAccount(this);
            String toAcc = editTextAccount.getText().toString().trim();
            String amount = editTextAmount.getText().toString().trim();
            String toName = editTextName.getText().toString().trim();
            transferId = account.getId() + System.currentTimeMillis();

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "slipsTransfer");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("transferId", transferId);
            createMap.put("fromAcc", account.getNo());
            createMap.put("toAcc", toAcc);
            createMap.put("toBank", bank.getBankCode());
            createMap.put("toBankName", bank.getBankName());
            createMap.put("toBranch", branch.getBranchCode());
            createMap.put("toBranchName", branch.getBranchName());
            createMap.put("toName", toName);
            createMap.put("amount", amount);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRANSFER_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        ActivityUtil.hideSoftKeyboard(this);
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 201) {
                    navigateToTransferConfirm();
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Failed to complete the transaction.");
        }
    }

    @Override
    public void onFinishTask(String response) {

    }

    private void navigateToTransferConfirm() {
        Payee payee = new Payee();
        payee.setName(editTextName.getText().toString().trim());
        payee.setAccountNo(editTextAccount.getText().toString().trim());
        payee.setBankName(bank.getBankName());
        payee.setBankCode(bank.getBankCode());
        payee.setBranch(branch.getBranchName());
        payee.setBranchCode(branch.getBranchCode());

        Intent intent = new Intent(this, FundTransferConfirmInfoActivity.class);
        intent.putExtra("TRANSFER_ID", transferId);
        intent.putExtra("PAYEE", payee);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

    private void createPayee() {
        Payee payee = new Payee();
        payee.setName(editTextName.getText().toString().trim());
        payee.setAccountNo(editTextAccount.getText().toString().trim());
        payee.setBankName(bank.getBankName());
        payee.setBankCode(bank.getBankCode());
        payee.setBranch(branch.getBranchName());
        payee.setBranchCode(branch.getBranchCode());

        try {
            PayeeSource.createPayee(this, payee);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
