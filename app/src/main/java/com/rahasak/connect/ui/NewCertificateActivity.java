package com.rahasak.connect.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.exceptions.InvalidNicNumberException;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.util.ActivityUtil;

public class NewCertificateActivity extends BaseActivity {

    // ui controls
    private EditText editTextPhone;
    private EditText editTextName;
    private EditText editTextNic;
    private EditText editTextEmail;
    private EditText editTextAddress;
    private Button nextButton;

    private Identity identity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_certificate_activity_layout);

        initUi();
        initToolbar();
        initActionBar();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Add certificate");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextNic = (EditText) findViewById(R.id.registering_nic);
        editTextPhone = (EditText) findViewById(R.id.registering_phone_no);
        editTextName = (EditText) findViewById(R.id.registering_name);
        editTextEmail = (EditText) findViewById(R.id.registering_email);
        editTextAddress = (EditText) findViewById(R.id.registering_address);

        editTextPhone.setTypeface(typeface, Typeface.NORMAL);
        editTextName.setTypeface(typeface, Typeface.NORMAL);
        editTextNic.setTypeface(typeface, Typeface.NORMAL);
        editTextEmail.setTypeface(typeface, Typeface.NORMAL);
        editTextAddress.setTypeface(typeface, Typeface.NORMAL);

        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setTypeface(typeface, Typeface.BOLD);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //onClickNext();
                navigateNext();
            }
        });
    }

    private void onClickNext() {
        ActivityUtil.hideSoftKeyboard(this);

        final String nic = editTextNic.getText().toString().trim().toUpperCase();
        final String name = editTextName.getText().toString().trim();
        final String phn = editTextPhone.getText().toString().trim();
        final String email = editTextEmail.getText().toString().trim();
        final String address = editTextAddress.getText().toString().trim();

        try {
            ActivityUtil.validatePersonalInfo(nic, name, phn, email, address);
            String formattedPhone = "+94" + phn.substring(1);
            identity = new Identity();
            identity.setDid(nic);
            identity.setOwner(AccountContractExecutor.OWNER);
            identity.setNic(nic);
            identity.setName(name);
            identity.setPhone(formattedPhone);
            identity.setDob("");
            identity.setEmail(email);
            identity.setAddress(address);

            navigateNext();
        } catch (NumberFormatException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid phone no. Phone no should contain 10 digits and start with 0");
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all fields to complete the registration.");
        }catch (InvalidNicNumberException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error",e.toString()) ;
        }
    }

    private void navigateNext() {
        Intent intent = new Intent(NewCertificateActivity.this, NewPromizeActivity.class);
        NewCertificateActivity.this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}
