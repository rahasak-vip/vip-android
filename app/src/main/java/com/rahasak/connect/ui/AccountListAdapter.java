package com.rahasak.connect.ui;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.BankAccount;

import java.util.ArrayList;

class AccountListAdapter extends ArrayAdapter<BankAccount> {
    private Context context;
    private Typeface typeface;

    AccountListAdapter(Context _context, ArrayList<BankAccount> accountList) {
        super(_context, R.layout.card_layout, R.id.user_name, accountList);
        context = _context;
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/GeosansLight.ttf");
    }

    /**
     * Create list row view
     *
     * @param i         index
     * @param view      current list item view
     * @param viewGroup parent
     * @return view
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        // A ViewHolder keeps references to children views to avoid unnecessary calls
        // to findViewById() on each row.
        final ViewHolder holder;

        final BankAccount account = getItem(i);

        if (view == null) {
            //inflate sensor list row layout
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.card_layout, viewGroup, false);

            //create view holder to store reference to child views
            holder = new ViewHolder();
            holder.accountLayout = (RelativeLayout) view.findViewById(R.id.account_layout);
            holder.aBalanceLayout = (LinearLayout) view.findViewById(R.id.alabalce_layout);
            holder.curveView = view.findViewById(R.id.curve_view);
            holder.cardIcon = (ImageView) view.findViewById(R.id.card_icon);
            holder.nameText = (TextView) view.findViewById(R.id.name_text);
            holder.name = (TextView) view.findViewById(R.id.name);
            holder.accountText = (TextView) view.findViewById(R.id.account_text);
            holder.account = (TextView) view.findViewById(R.id.account);
            holder.aBalanceText = (TextView) view.findViewById(R.id.abalance_text);
            holder.aBalance = (TextView) view.findViewById(R.id.abalance);
            holder.balance = (TextView) view.findViewById(R.id.balance);
            holder.accountType = (TextView) view.findViewById(R.id.account_type);
            holder.promizeAccount = (TextView) view.findViewById(R.id.promize_account);

            view.setTag(holder);
        } else {
            //get view holder back_icon
            holder = (ViewHolder) view.getTag();
        }

        setUpRow(account, holder);

        return view;
    }

    private void setUpRow(BankAccount account, ViewHolder viewHolder) {
        viewHolder.balance.setTypeface(typeface, Typeface.BOLD);
        viewHolder.accountText.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.account.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.nameText.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.name.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.aBalanceText.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.aBalance.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.accountType.setTypeface(typeface, Typeface.NORMAL);
        viewHolder.promizeAccount.setTypeface(typeface, Typeface.NORMAL);

        // set text
        viewHolder.account.setText(account.getNo());
        viewHolder.name.setText(account.getName());
        if (account.getAvailableBalance().isEmpty()) {
            viewHolder.aBalanceLayout.setVisibility(View.GONE);
        } else {
            viewHolder.aBalanceLayout.setVisibility(View.VISIBLE);
            viewHolder.aBalance.setText("Rs " + account.getAvailableBalance());
        }
        viewHolder.balance.setText("Rs " + account.getCurrentBalance());
        viewHolder.accountType.setText(account.getTyp());

        // promize account
        if (account.isPromizeAccount()) {
            viewHolder.accountLayout.setBackgroundResource(R.drawable.account_list_row_primary_background);
            viewHolder.cardIcon.setImageResource(R.drawable.mastercardw);
            viewHolder.promizeAccount.setVisibility(View.VISIBLE);
            viewHolder.balance.setTextColor(Color.WHITE);

            viewHolder.nameText.setTextColor(Color.WHITE);
            viewHolder.aBalanceText.setTextColor(Color.WHITE);
            viewHolder.accountText.setTextColor(Color.WHITE);
            viewHolder.name.setTextColor(Color.WHITE);
            viewHolder.aBalance.setTextColor(Color.WHITE);
            viewHolder.account.setTextColor(Color.WHITE);
            viewHolder.accountType.setTextColor(Color.WHITE);
            viewHolder.promizeAccount.setTextColor(Color.WHITE);
            viewHolder.curveView.setVisibility(View.VISIBLE);

//            viewHolder.nameText.setTextColor(Color.parseColor("#D0D0D2"));
//            viewHolder.aBalanceText.setTextColor(Color.parseColor("#D0D0D2"));
//            viewHolder.accountText.setTextColor(Color.parseColor("#D0D0D2"));
//            viewHolder.name.setTextColor(Color.parseColor("#D0D0D2"));
//            viewHolder.aBalance.setTextColor(Color.parseColor("#D0D0D2"));
//            viewHolder.account.setTextColor(Color.parseColor("#D0D0D2"));
//            viewHolder.accountType.setTextColor(Color.parseColor("#D0D0D2"));
//            viewHolder.promizeAccount.setTextColor(Color.parseColor("#D0D0D2"));
            //viewHolder.accountType.setBackgroundResource(R.drawable.text_bg);
        } else {
            viewHolder.accountLayout.setBackgroundResource(R.drawable.account_list_row_white_background);
            viewHolder.cardIcon.setImageResource(R.drawable.mastercard_black);
            viewHolder.promizeAccount.setVisibility(View.GONE);
            viewHolder.balance.setTextColor(Color.BLACK);
            viewHolder.curveView.setVisibility(View.GONE);

            viewHolder.nameText.setTextColor(Color.BLACK);
            viewHolder.aBalanceText.setTextColor(Color.BLACK);
            viewHolder.accountText.setTextColor(Color.BLACK);
            viewHolder.name.setTextColor(Color.BLACK);
            viewHolder.aBalance.setTextColor(Color.BLACK);
            viewHolder.account.setTextColor(Color.BLACK);
            viewHolder.accountType.setTextColor(Color.BLACK);
            viewHolder.promizeAccount.setTextColor(Color.BLACK);
            //viewHolder.accountType.setBackgroundResource(R.drawable.text_bg);
        }
    }

    /**
     * Keep reference to children view to avoid unnecessary calls
     */
    private static class ViewHolder {
        RelativeLayout accountLayout;
        LinearLayout aBalanceLayout;
        FrameLayout curveView;
        ImageView cardIcon;
        TextView nameText;
        TextView name;
        TextView accountText;
        TextView account;
        TextView aBalanceText;
        TextView aBalance;
        TextView balance;
        TextView accountType;
        TextView promizeAccount;
    }

}

