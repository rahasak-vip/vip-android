package com.rahasak.connect.ui;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Bank;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class PromizePayActivity extends BaseActivity implements IContractExecutorListener {

    private static final String TAG = PromizePayActivity.class.getName();

    private EditText editTextBank;
    private EditText editTextName;
    private EditText editTextAccount;
    private EditText editTextConfirmAccount;
    private EditText editTextAmount;

    private Bank bank;
    private String payee;
    private String account;
    private String amount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promize_pay_layout);

        initUi();
        initPrefs();
        initToolbar();
        initActionBar();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Promize pay");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextBank = (EditText) findViewById(R.id.bank);
        editTextName = (EditText) findViewById(R.id.name);
        editTextAccount = (EditText) findViewById(R.id.account);
        editTextConfirmAccount = (EditText) findViewById(R.id.confirm_account);
        editTextAmount = (EditText) findViewById(R.id.amount);

        editTextBank.setTypeface(typeface, Typeface.BOLD);
        editTextName.setTypeface(typeface, Typeface.BOLD);
        editTextAccount.setTypeface(typeface, Typeface.BOLD);
        editTextConfirmAccount.setTypeface(typeface, Typeface.BOLD);
        editTextAmount.setTypeface(typeface, Typeface.BOLD);

        // ui controls
        Button pay = (Button) findViewById(R.id.done);
        pay.setTypeface(typeface, Typeface.BOLD);
        pay.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickPay();
            }
        });
    }

    private void initPrefs() {
        String extra = getIntent().getStringExtra("EXTRA");
        if (extra != null || !extra.isEmpty()) {
            String[] args = extra.split(":");
            this.payee = args[0].trim();
            this.account = args[1].trim();
            this.amount = args[2].trim();

            editTextName.setText(payee);
            editTextAccount.setText(account);

            Log.i(TAG, "Payee: " + payee);
            Log.i(TAG, "Account: " + account);
            Log.i(TAG, "Amount: " + amount);
        }
    }

    private void onClickPay() {
        // crate account
        amount = editTextAmount.getText().toString().trim();
        try {
            ActivityUtil.isValidPay(account, amount);
            if (NetworkUtil.isAvailableNetwork(PromizePayActivity.this)) {
                askPassword();
            } else {
                Toast.makeText(PromizePayActivity.this, "No network connection", Toast.LENGTH_LONG).show();
            }
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all the fields to proceed this transaction.");
        }
    }

    public void askPassword() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_password_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        // texts
        TextView title = (TextView) dialog.findViewById(R.id.title);
        final EditText password = (EditText) dialog.findViewById(R.id.password);
        title.setTypeface(typeface, Typeface.BOLD);
        password.setTypeface(typeface, Typeface.NORMAL);

        // set ok button
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setTypeface(typeface, Typeface.BOLD);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String givenPassword = password.getText().toString().trim();
                    String hashPass = CryptoUtil.hashSha256(givenPassword);
                    if (hashPass.equalsIgnoreCase(PreferenceUtil.getAccount(PromizePayActivity.this).getPassword())) {
                        dialog.cancel();
                        ActivityUtil.hideSoftKeyboard(PromizePayActivity.this);
                        pay();
                    } else {
                        Toast.makeText(PromizePayActivity.this, "Password is Incorrect", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(PromizePayActivity.this, "Password is Incorrect", Toast.LENGTH_LONG).show();
                }
            }
        });

        // cancel button
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setTypeface(typeface, Typeface.BOLD);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void pay() {
        ActivityUtil.hideSoftKeyboard(this);
        try {
            Account acc = PreferenceUtil.getAccount(this);
            String amount = editTextAmount.getText().toString().trim();

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "pay");
            createMap.put("execer", acc.getId());
            createMap.put("id", acc.getId() + System.currentTimeMillis());
            createMap.put("promizeId", acc.getId() + System.currentTimeMillis());
            createMap.put("accountId", acc.getId());
            createMap.put("toAcc", account);
            createMap.put("toName", payee);
            createMap.put("amount", amount);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.PROMIZE_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        ActivityUtil.hideSoftKeyboard(this);
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() == 201) {
                    displayInformationMessageDialogConfirm("Successful", "Transaction is completed successfully", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SenzApplication.setRefreshWallet(true);
                            SenzApplication.setRefreshPromize(true);
                            PromizePayActivity.this.finish();
                        }
                    });
                } else {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while request.");
        }
    }

    @Override
    public void onFinishTask(String response) {

    }
}
