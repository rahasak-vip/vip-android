package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.exceptions.InvalidPhoneNumberException;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.util.ActivityUtil;

public class OccupationInfoActivity extends BaseActivity {

    // ui controls
    private EditText editTextTaxId;
    private EditText editTextOccupation;
    private EditText editTextEmployeeName;
    private EditText editTextEmployeePhone;
    private EditText editTextEmployeeAddress;
    private Button nextButton;

    private Identity identity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_occupation_info_layout);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        this.identity = getIntent().getParcelableExtra("IDENTITY");
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Sport details");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextTaxId = (EditText) findViewById(R.id.tax_id);
        editTextOccupation = (EditText) findViewById(R.id.occupation);
        editTextEmployeeName = (EditText) findViewById(R.id.employee_name);
        editTextEmployeePhone = (EditText) findViewById(R.id.employee_phone);
        editTextEmployeeAddress = (EditText) findViewById(R.id.employee_address);

        editTextTaxId.setTypeface(typeface, Typeface.NORMAL);
        editTextOccupation.setTypeface(typeface, Typeface.NORMAL);
        editTextEmployeeName.setTypeface(typeface, Typeface.NORMAL);
        editTextEmployeePhone.setTypeface(typeface, Typeface.NORMAL);
        editTextEmployeeAddress.setTypeface(typeface, Typeface.NORMAL);

        nextButton = (Button) findViewById(R.id.next_button);
        nextButton.setTypeface(typeface, Typeface.BOLD);
        nextButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickNext();
            }
        });
    }

    private void onClickNext() {
        ActivityUtil.hideSoftKeyboard(this);

        final String taxId = editTextTaxId.getText().toString().trim().toUpperCase();
        final String occupation = editTextOccupation.getText().toString().trim();
        final String employeeName = editTextEmployeeName.getText().toString().trim();
        final String employeePhone = editTextEmployeePhone.getText().toString().trim();
        final String employeeAddress = editTextEmployeeAddress.getText().toString().trim();

        try {
            ActivityUtil.validateOccupationInfo(taxId, occupation, employeeName, employeePhone, employeeAddress);
            String formattedPhone = "+94" + employeePhone.substring(1);
            identity.setTaxNo(taxId);
            identity.setOccupation(occupation);
            identity.setEmployeeName(employeeName);
            identity.setEmployeeAddress(employeeAddress);

            navigateNext(identity);
        } catch (InvalidPhoneNumberException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid phone no. Phone no should contain 10 digits and start with 0");
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all fields to complete the registration.");
        }
    }

    private void navigateNext(Identity identity) {
        Intent intent = new Intent(OccupationInfoActivity.this, CapturePhotoInfoActivity.class);
        intent.putExtra("IDENTITY", identity);
        OccupationInfoActivity.this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}
