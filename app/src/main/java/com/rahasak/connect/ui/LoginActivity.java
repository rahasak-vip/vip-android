package com.rahasak.connect.ui;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.pojo.TokenReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

import org.json.JSONException;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class LoginActivity extends BaseActivity implements IContractExecutorListener {

    // UI fields
    private EditText editTextAccount;
    private EditText editTextPassword;
    private Button loginButton;
    private TextView link1;
    private TextView link2;

    // account
    private String did;
    private String owner;
    private String password;
    private String givenDid;
    private String givenPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_layout);

        initPrefs();
        initUi();
        initAccount();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        if (SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.OWNER).equalsIgnoreCase("")) {
            SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.OWNER, SenzApplication.getIdentityOwner());
        }
        did = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.DID);
        owner = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.OWNER);
        password = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.ACCOUNT_PASSWORD);
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Login");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @SuppressLint("RestrictedApi")
    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        editTextAccount = (EditText) findViewById(R.id.login_account_no);
        editTextPassword = (EditText) findViewById(R.id.login_password);
        loginButton = (Button) findViewById(R.id.login_btn);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickLogin();
            }
        });

        editTextAccount.setTypeface(typeface, Typeface.NORMAL);
        editTextPassword.setTypeface(typeface, Typeface.NORMAL);
        loginButton.setTypeface(typeface, Typeface.BOLD);

        link1 = (TextView) findViewById(R.id.register);
        link1.setPaintFlags(link1.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        link1.setTypeface(typeface, Typeface.BOLD);
        link1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (did.isEmpty())
                    navigateToRegister();
                //else
                //navigateForgotPasswordInfo();
            }
        });

        link2 = (TextView) findViewById(R.id.forgot_password);
        link2.setPaintFlags(link2.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        link2.setTypeface(typeface, Typeface.BOLD);
        link2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //navigateForgotPasswordInfo();
            }
        });
    }

    private void initAccount() {
        if (did.isEmpty()) {
            // no registered user for this device
            // enable edit
            editTextAccount.setFocusable(true);
            editTextAccount.setFocusableInTouchMode(true);
            editTextAccount.setEnabled(true);

            // enable register, forgot password link
            link1.setText("Don't have an account, register?");
            link2.setText("Forgot password?");
            link2.setVisibility(View.VISIBLE);
        } else {
            // have registered user for this device
            editTextAccount.setText(did);
            editTextPassword.setText(password);

//            StringBuilder buf = new StringBuilder(did);
//            String star = new String(new char[did.length() - 4]).replace("\0", "*");
//            buf.replace(2, did.length() - 2, star);
//            editTextAccount.setText(buf.toString());

            // disable edit
            editTextAccount.setFocusable(false);
            editTextAccount.setFocusableInTouchMode(false);
            editTextAccount.setEnabled(false);

            // disable register link
            link1.setText("Forgot password?");
            link2.setVisibility(View.GONE);
        }
    }

    private void onClickLogin() {
        ActivityUtil.hideSoftKeyboard(this);

        try {
            // validate given credentials
            givenDid = editTextAccount.getText().toString().toUpperCase().trim();
            givenPassword = editTextPassword.getText().toString().trim();
            ActivityUtil.isValidLoginFields(givenDid, givenPassword);

            //create key pair for users signedup from web
            String publickey = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.PUBLIC_KEY);
            Log.d("Msg", "PubKey:" + publickey);
            if (publickey.isEmpty()) {
                Log.d("Msg", "PubKey:No PubKey");
                try {
                    CryptoUtil.initKeys(this);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
            }

            // send request if network available
            if (NetworkUtil.isAvailableNetwork(this)) {
                doLogin(owner, givenDid, givenPassword);
            } else {
                Toast.makeText(this, "No network connection", Toast.LENGTH_LONG).show();
            }
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Please enter NIC and Password to log in");
        }
    }

    private void doLogin(String owner, String did, String password) {
        try {
            // set accountId and hash 256 password
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "connect");
            createMap.put("execer", did + ":" + owner);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("did", did);
            createMap.put("owner", owner);
            createMap.put("password", CryptoUtil.hashSha256(password));

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateDeviceToken() {
        try {
            String did = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.DID);
            String owner = SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.OWNER);
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "updateDevice");
            createMap.put("execer", did + ":" + owner);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("did", did);
            createMap.put("owner", owner);
            createMap.put("deviceType", "android");
            createMap.put("deviceToken", SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.FIREBASE_TOKEN));

            String digis = CryptoUtil.getDigitalSignature(JsonUtil.toOrderedString(createMap), CryptoUtil.getPrivateKey(this));
            createMap.put("msgsig", digis);

            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                if (response.getStatus() == 200) {
                    if (response.getPayload().contains("Updated")) {
                        // device token updated means full login success
                        ActivityUtil.cancelProgressDialog();
                        Toast.makeText(this, "Login success", Toast.LENGTH_LONG).show();

                        SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.UPDATE_FIREBASE_TOKEN, "no");
                        navigateToHome();
                    } else {
                        // this should be login auth token reply
                        TokenReply tokenReply = JsonUtil.toTokenReply(response.getPayload());
                        if (tokenReply.getStatus() == 200) {
                            loginDone(tokenReply);

                            // update device token
                            if (SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.UPDATE_FIREBASE_TOKEN).equalsIgnoreCase("yes")) {
                                updateDeviceToken();
                            } else {
                                ActivityUtil.cancelProgressDialog();
                                navigateToHome();
                            }
                        } else if (tokenReply.getStatus() == 402) {
                            // means account not activated
                            ActivityUtil.cancelProgressDialog();
                            //displayInformationMessageDialog("Error", "Your account is not activated, please try to reset password and activate the account.");
                            displayInformationMessageDialog("Error", "Your account is not activated.");
                        } else if (tokenReply.getStatus() == 405) {
                            // means need to add qualification
                            navigateToQualificationInfo();
                        } else {
                            ActivityUtil.cancelProgressDialog();
                            displayInformationMessageDialog("Error", "Something went wrong while login.");
                        }
                    }
                } else {
                    ActivityUtil.cancelProgressDialog();
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while login.");
        }
    }

    private void loginDone(TokenReply tokenReply) {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        // save prefs
        SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.DID, givenDid);
        SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.ACCOUNT_PASSWORD, givenPassword);
        SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.TOKEN, tokenReply.getToken());
    }

    private void navigateToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    private void navigateToRegister() {
        Intent intent = new Intent(this, PersonalInfoActivity.class);
        startActivity(intent);
        finish();
    }

    private void navigateToSaltConfirmInfo() {
        Intent intent = new Intent(this, SaltConfirmInfoActivity.class);
        this.startActivity(intent);
        this.finish();
    }

    public void navigateToQuestionInfo() {
        Intent intent = new Intent(this, RegistrationQuestionInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void navigateToQualificationInfo() {
        Intent intent = new Intent(this, QualificationQuestionInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

    public void navigateForgotPasswordInfo() {
        Intent intent = new Intent(this, PasswordResetInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

}

