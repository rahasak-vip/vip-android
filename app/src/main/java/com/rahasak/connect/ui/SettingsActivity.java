package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class SettingsActivity extends BaseActivity implements IContractExecutorListener {

    private static final String TAG = SettingsActivity.class.getName();

    private TextView phone;
    private TextView phonev;
    private TextView nic;
    private TextView nicv;
    private TextView account;
    private TextView accountv;
    private TextView password;
    private TextView terms;
    private TextView logout;

    private Button accountChangeBtn;
    private Button passChangeBtn;
    private Button passResetBtn;
    private Button termsBtn;
    private Button logoutBtn;

    private Account userAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        initUi();
        initPrefs();
        initToolbar();
        initActionBar();
        getAccount(userAccount.getId());
    }

    private void initUi() {
        // text views
        phone = (TextView) findViewById(R.id.phone);
        phonev = (TextView) findViewById(R.id.phonev);
        nic = (TextView) findViewById(R.id.nic);
        nicv = (TextView) findViewById(R.id.nicv);
        account = (TextView) findViewById(R.id.account);
        accountv = (TextView) findViewById(R.id.accountv);
        password = (TextView) findViewById(R.id.password);
        terms = (TextView) findViewById(R.id.terms);
        logout = (TextView) findViewById(R.id.logout);

        phone.setTypeface(typeface, Typeface.NORMAL);
        phonev.setTypeface(typeface, Typeface.NORMAL);
        nic.setTypeface(typeface, Typeface.NORMAL);
        nicv.setTypeface(typeface, Typeface.NORMAL);
        account.setTypeface(typeface, Typeface.NORMAL);
        accountv.setTypeface(typeface, Typeface.NORMAL);
        password.setTypeface(typeface, Typeface.NORMAL);
        terms.setTypeface(typeface, Typeface.NORMAL);
        logout.setTypeface(typeface, Typeface.NORMAL);

        accountChangeBtn = (Button) findViewById(R.id.account_change);
        accountChangeBtn.setTypeface(typeface, Typeface.BOLD);
        accountChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToAccountChange();
            }
        });

        passChangeBtn = (Button) findViewById(R.id.pass_change_btn);
        passChangeBtn.setTypeface(typeface, Typeface.BOLD);
        passChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToPasswordChange();
            }
        });

        passResetBtn = (Button) findViewById(R.id.pass_reset_btn);
        passResetBtn.setTypeface(typeface, Typeface.BOLD);
        passResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToPasswordReset();
            }
        });

        termsBtn = (Button) findViewById(R.id.terms_btn);
        termsBtn.setTypeface(typeface, Typeface.BOLD);
        termsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTerms();
            }
        });

        logoutBtn = (Button) findViewById(R.id.logout_button);
        logoutBtn.setTypeface(typeface, Typeface.BOLD);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SenzApplication.setLogin(false);
                SenzApplication.cancelLogoutTimer();
                navigateToLogin();
            }
        });
    }

    private void initPrefs() {
        userAccount = PreferenceUtil.getAccount(this);

        phonev.setText(userAccount.getId());
        nicv.setText("");
        accountv.setText("");
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Settings");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void getAccount(String accountId) {
        try {
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "get");
            createMap.put("execer", userAccount.getId());
            createMap.put("id", userAccount.getId() + System.currentTimeMillis());
            createMap.put("accountId", accountId);

            ActivityUtil.showProgressDialog(this, "Downloading settings...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response != null && response.getStatus() == 200) {
                Account accountReply = JsonUtil.toAccountReply(response.getPayload());
                updateAccountInfo(accountReply);
            } else {
                ActivityUtil.cancelProgressDialog();
                displayInformationMessageDialog("Error", "Failed to get account details.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Failed to get account details.");
        }
    }

    private void updateAccountInfo(Account accountReply) {
        // set ui fields
        phonev.setText(accountReply.getId());
        nicv.setText(accountReply.getNic());
        accountv.setText(accountReply.getNo());
    }

    private void navigateToAddAccount() {
        if (userAccount.getNo().isEmpty()) {
            // account verify
            Intent intent = new Intent(this, ReceivePromizeInfoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.stay_in);
            finish();
        } else if (userAccount.getState().equalsIgnoreCase("PENDING")) {
            // salt confirm
            Intent intent = new Intent(this, SaltConfirmInfoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.stay_in);
            finish();
        } else {
            Intent intent = new Intent(SettingsActivity.this, ReceivePromizeInfoActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            overridePendingTransition(R.anim.right_in, R.anim.stay_in);
            finish();
        }
    }

    private void navigateToAccountChange() {
        Intent intent = new Intent(SettingsActivity.this, AccountListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finish();
    }

    private void navigateToPasswordChange() {
        Intent intent = new Intent(SettingsActivity.this, PasswordChangeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finish();
    }

    private void navigateToPasswordReset() {
        Intent intent = new Intent(SettingsActivity.this, PasswordResetInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finish();
    }

    private void navigateToTerms() {
        Intent intent = new Intent(SettingsActivity.this, TermsOfUseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finish();
    }

    private void navigateToLogin() {
        Intent intent = new Intent(SettingsActivity.this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
