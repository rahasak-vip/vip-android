package com.rahasak.connect.ui;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.Transaction;
import com.rahasak.connect.pojo.TransactionResponse;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class DepositTransactionListFragment extends Fragment implements AdapterView.OnItemClickListener, IContractExecutorListener {

    private ArrayList<Transaction> transList;
    private TransactionListAdapter adapter;
    private ListView listView;
    private RelativeLayout emptyView;
    private TextView emptyText;
    private boolean transLoaded = false;

    protected Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.transaction_list_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        initListView(view);
        fetchTrans("0", "10");

        return view;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser && !transLoaded) {
            transLoaded = true;
            fetchTrans("0", "10");
        }
    }

    private void initListView(View view) {
        listView = (ListView) view.findViewById(R.id.cheque_list_view);
        listView.setOnItemClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);

        transList = new ArrayList<>();
        adapter = new TransactionListAdapter(getActivity(), transList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    private void refreshView(TransactionResponse transactionResponse) {
        TextView balanceText = ((TextView) getActivity().findViewById(R.id.balance_text));
        AppBarLayout appBarLayout = (AppBarLayout) getActivity().findViewById(R.id.app_bar_layout);

        // balance
        if (transactionResponse.getBalance().isEmpty()) {
            balanceText.setText("Not available");
        } else {
            balanceText.setText("Rs " + transactionResponse.getBalance() + "/=");
        }

        // transactions
        if (transactionResponse.getTransactions().size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            listView.setEmptyView(emptyView);
            emptyText.setText("Fail to fetch transactions history at this time, please try again short time");
            appBarLayout.setExpanded(false, true);
        } else {
            emptyView.setVisibility(View.GONE);
            appBarLayout.setExpanded(true, true);

            transList.addAll(transactionResponse.getTransactions());
            adapter.notifyDataSetChanged();
        }
    }

    private void fetchTrans(String offset, String limit) {
        // create senz
        try {
            Account account = PreferenceUtil.getAccount(this.getActivity());

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "history");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("offset", offset);
            createMap.put("limit", limit);

            ActivityUtil.showProgressDialog(getActivity(), "Fetching transactions...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(getActivity(), PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Transaction transaction = transList.get(position);
    }

    @Override
    public void onFinishTask(String response) {
        ActivityUtil.cancelProgressDialog();
        try {
            TransactionResponse transactionResponse = JsonUtil.toTransactionResponse(response);
            refreshView(transactionResponse);
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(getActivity(), "Fail to fetch history", Toast.LENGTH_LONG).show();
        }

        // add sample list items
//        ArrayList<Transaction> transactions = new ArrayList<>();
//        for (int i = 0; i < 10; i++) {
//            final Transaction transaction = new Transaction();
//            transaction.setDate("2019-10-21");
//            transaction.setAmount("320" + i);
//            transaction.setDescription("Deposit");
//            transactions.add(transaction);
//        }

        //refreshList(transactions);
    }

    @Override
    public void onFinishTask(Response response) {

    }
}
