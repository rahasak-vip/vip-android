package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.util.RootUtil;

public class SplashActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
        setContentView(R.layout.splash_layout1);

        initUi();
        //initPromize();
        initNavigation();
    }

    private void initPromize() {
        if (RootUtil.isRooted(SplashActivity.this)) {
            displayInformationMessageDialogConfirm("Rooted device", "Connect application not allowed to install on rooted devices", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
        } else if (RootUtil.onEmulator()) {
            displayInformationMessageDialogConfirm("Android emulator", "Connect application not allowed to install on Android Emulator", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
//        } else if (!RootUtil.isValidAppSignature(this)) {
//            displayInformationMessageDialogConfirm("Invalid app signature", "We have noticed that the app signature has changed", new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    finish();
//                }
//            });
        } else {
            initNavigation();
        }
    }

    private void initUi() {
        ((TextView) findViewById(R.id.splash_name)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.desc)).setTypeface(typeface, Typeface.BOLD);
        ((TextView) findViewById(R.id.powerd)).setTypeface(typeface, Typeface.BOLD);

        // set status bar color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.black, this.getTheme()));
        } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(getResources().getColor(R.color.black));
        }
    }

    private void initNavigation() {
        if (SenzApplication.isLogin()) {
            navigateToHome();
        } else {
            // stay 3 seconds in splash
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    navigateToLogin();
//                    SenzApplication.setLogin(true);
//                    navigateToHome();
                }
            }, 3000);
        }
    }

    public void navigateToRegistration() {
        Intent intent = new Intent(this, PersonalInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    public void navigateToSaltConfim() {
        Intent intent = new Intent(this, SaltConfirmInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    public void navigateToQuestionInfo() {
        Intent intent = new Intent(this, RegistrationQuestionInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    public void navigateToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashActivity.this.finish();
    }

    public void navigateToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        SplashActivity.this.finish();
    }

}