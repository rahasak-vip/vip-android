package com.rahasak.connect.ui;

import android.app.Dialog;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.db.BillerSource;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Bank;
import com.rahasak.connect.pojo.Biller;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

public class PaymentActivity extends BaseActivity implements IContractExecutorListener {

    private static final String TAG = PaymentActivity.class.getName();

    // ui controls
    private Button redeem;
    private EditText editTextBillType;
    private EditText editTextBillerName;
    private EditText editTextAmount;
    private EditText amount;
    private Toolbar toolbar;

    private String billType;
    private Bank biller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_layout);

        initUi();
        initPrefs();
        initToolbar();
        initActionBar();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Make payment");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextBillType = (EditText) findViewById(R.id.bill_type);
        editTextBillerName = (EditText) findViewById(R.id.biller_name);
        editTextAmount = (EditText) findViewById(R.id.amount);
        amount = (EditText) findViewById(R.id.amount);

        editTextBillType.setTypeface(typeface, Typeface.BOLD);
        editTextBillerName.setTypeface(typeface, Typeface.BOLD);
        editTextAmount.setTypeface(typeface, Typeface.BOLD);
        amount.setTypeface(typeface, Typeface.BOLD);

        redeem = (Button) findViewById(R.id.done);
        redeem.setTypeface(typeface, Typeface.BOLD);
        redeem.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                ActivityUtil.hideSoftKeyboard(PaymentActivity.this);
                onClickPay();
            }
        });
    }

    private void initPrefs() {
        this.billType = getIntent().getStringExtra("BILL_TYPE");
        this.biller = getIntent().getParcelableExtra("BILLER");
        editTextBillType.setText(this.billType);
        editTextBillerName.setText(this.biller.getBankName());
    }

    private void onClickPay() {
        // crate account
        final String accountNo = editTextBillType.getText().toString().trim();
        final String confirmAccountNo = editTextBillType.getText().toString().trim();
        if (NetworkUtil.isAvailableNetwork(PaymentActivity.this)) {
            askPassword();
            //createBiller();
        } else {
            Toast.makeText(PaymentActivity.this, "No network connection", Toast.LENGTH_LONG).show();
        }
    }

    public void askPassword() {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_password_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        // texts
        TextView title = (TextView) dialog.findViewById(R.id.title);
        final EditText password = (EditText) dialog.findViewById(R.id.password);
        title.setTypeface(typeface, Typeface.BOLD);
        password.setTypeface(typeface, Typeface.NORMAL);

        // set ok button
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setTypeface(typeface, Typeface.BOLD);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password.getText().toString().trim().equalsIgnoreCase(PreferenceUtil.getAccount(PaymentActivity.this).getPassword())) {
                    dialog.cancel();
                    ActivityUtil.hideSoftKeyboard(PaymentActivity.this);
                    //redeem();
                } else {
                    Toast.makeText(PaymentActivity.this, "Password is Incorrect", Toast.LENGTH_LONG).show();
                }
            }
        });

        // cancel button
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setTypeface(typeface, Typeface.BOLD);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    @Override
    public void onFinishTask(String response) {
        ActivityUtil.cancelProgressDialog();
        ActivityUtil.hideSoftKeyboard(this);
        try {
            StatusReply statusReply = JsonUtil.toStatusReply(response);
            if (statusReply.getCode() == 200) {
                Toast.makeText(this, "Payment done", Toast.LENGTH_LONG).show();
                createBiller();
            } else {
                ActivityUtil.cancelProgressDialog();
                displayInformationMessageDialog("Error", "Something went wrong while make payment.");
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while make payment.");
        }
    }

    @Override
    public void onFinishTask(Response response) {

    }

    private void createBiller() {
        Biller b = new Biller();
        b.setBillType(editTextBillType.getText().toString().trim());
        b.setBillerName(this.biller.getBankName());
        b.setBillerAccount(this.biller.getBankCode());
        BillerSource.createBiller(this, b);

        PaymentActivity.this.finish();
    }
}
