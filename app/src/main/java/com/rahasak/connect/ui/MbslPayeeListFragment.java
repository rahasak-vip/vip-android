package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.db.PayeeSource;
import com.rahasak.connect.enums.CustomerActionType;
import com.rahasak.connect.pojo.Payee;

import java.util.ArrayList;

public class MbslPayeeListFragment extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ArrayList<Payee> payeeList;
    private PayeeListAdapter payeeListAdapter;
    private RelativeLayout emptyView;
    private ListView accountListView;
    private Typeface typeface;

    private CustomerActionType actionType = CustomerActionType.CUSTOMER_LIST;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mbsl_payee_list_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        initNewButton(view);
        initListView(view);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        refreshList();
    }

    private void initNewButton(View view) {
        // new
        FloatingActionButton newCustomer = (FloatingActionButton) view.findViewById(R.id.new_account);
        newCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // navigate to contact list
                Intent intent = new Intent(getActivity(), BankListActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initSearchView(View view) {
        LinearLayout search = (LinearLayout) view.findViewById(R.id.search_layout);
        EditText searchView = (EditText) view.findViewById(R.id.inputSearch);
        searchView.setTypeface(typeface);
        if (payeeList.size() == 0) {
            search.setVisibility(View.GONE);
        } else {
            search.setVisibility(View.VISIBLE);
            searchView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    payeeListAdapter.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    private void initListView(View view) {
        accountListView = (ListView) view.findViewById(R.id.customer_list_view);
        accountListView.setOnItemClickListener(this);
        accountListView.setOnItemLongClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        TextView emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);
    }

    private void refreshList() {
        payeeList = PayeeSource.getPayees(getActivity());
        if (payeeList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            accountListView.setEmptyView(emptyView);
        } else {
            emptyView.setVisibility(View.GONE);
        }

        payeeListAdapter = new PayeeListAdapter(getActivity(), payeeList);
        accountListView.setAdapter(payeeListAdapter);
        payeeListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Payee account = payeeList.get(position);
        if (account.getBankCode().equalsIgnoreCase("7898")) {
            Intent intent = new Intent(getActivity(), FundTransferMbslActivity.class);
            intent.putExtra("PAYEE", account);
            startActivity(intent);
        } else {
            Intent intent = new Intent(getActivity(), FundTransferCeftActivity.class);
            intent.putExtra("PAYEE", account);
            startActivity(intent);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        if (actionType == CustomerActionType.CUSTOMER_LIST) {
            final Payee payee = payeeList.get(position);
//            displayConfirmationMessageDialog("Confirm", "Are you sure your want to remove the account?", new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    // delete item
//                    payeeList.remove(position);
//                    payeeListAdapter.notifyDataSetChanged();
//
//                    // TODO delete from db
//                }
//            });
        }

        return true;
    }

}
