package com.rahasak.connect.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.IntentProvider;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.NotificationMessage;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class ReceivePromizeInfoActivity extends BaseActivity implements IContractExecutorListener {

    private static final String TAG = ReceivePromizeInfoActivity.class.getName();

    // UI fields
    private TextView header;
    private TextView hi;
    private TextView message;

    private String payeeId;
    private String payeeName;
    private String amount;

    private BroadcastReceiver msgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("NOTIFICATION_MESSAGE")) {
                NotificationMessage msg = intent.getExtras().getParcelable("NOTIFICATION_MESSAGE");
                handleNotifiationMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.receive_promize_info_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(msgReceiver, new IntentFilter(IntentProvider.ACTION_SENZ));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (msgReceiver != null) unregisterReceiver(msgReceiver);
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        header = (TextView) findViewById(R.id.header_text);
        hi = (TextView) findViewById(R.id.hi_message);
        message = (TextView) findViewById(R.id.welcome_message);
        header.setTypeface(typeface, Typeface.BOLD);
        hi.setTypeface(typeface, Typeface.NORMAL);
        message.setTypeface(typeface, Typeface.NORMAL);

        // set hi text
        String cur = String.format("%,.2f", Double.parseDouble(amount));
        hi.setText("Please confirm to complete Rs. " + cur + " Promize Transaction");

        Button yes = (Button) findViewById(R.id.yes);
        yes.setTypeface(typeface, Typeface.BOLD);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // post promize
                if (NetworkUtil.isAvailableNetwork(ReceivePromizeInfoActivity.this)) {
                    postPromize();
                } else {
                    Toast.makeText(ReceivePromizeInfoActivity.this, "No network connection", Toast.LENGTH_LONG).show();
                }
            }
        });

        Button no = (Button) findViewById(R.id.no);
        no.setTypeface(typeface, Typeface.BOLD);
        no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initPrefs() {
        String extra = getIntent().getStringExtra("EXTRA");
        if (extra != null || !extra.isEmpty()) {
            String[] args = extra.split(":");
            this.payeeId = args[0].trim();
            this.payeeName = args[1].trim();
            this.amount = args[2].trim();

            Log.i(TAG, "Promize payee id: " + payeeId);
            Log.i(TAG, "Promize payee name: " + payeeName);
            Log.i(TAG, "Promize amount: " + amount);
        }
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Confirm transaction");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setVisibility(View.INVISIBLE);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void postPromize() {
        // create senz
        try {
            Account account = PreferenceUtil.getAccount(this);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "create");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("promizeId", account.getId() + System.currentTimeMillis());
            createMap.put("promizeFrom", payeeId);
            createMap.put("promizeTo", account.getId());
            createMap.put("promizeAmount", amount);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.PROMIZE_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                if (statusReply.getCode() != 201) {
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Failed to complete this transaction.");
        }
    }

    /**
     * Handle broadcast message receives
     * Need to handle registration success failure here
     *
     * @param msg
     */
    private void handleNotifiationMessage(NotificationMessage msg) {
        ActivityUtil.cancelProgressDialog();

        if (msg.getPromizeStatus().equalsIgnoreCase("ERROR")) {
            // fail promize
            //finish();
            displayInformationMessageDialog("Error", "Failed to complete this transaction");
        } else {
            // create promize
            displayInformationMessageDialogConfirm("Successful", "Transaction is completed successfully", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SenzApplication.setRefreshWallet(true);
                    SenzApplication.setRefreshPromize(true);
                    ReceivePromizeInfoActivity.this.finish();
                }
            });
        }
    }

}

