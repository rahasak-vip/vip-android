package com.rahasak.connect.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.rahasak.connect.R;
import com.rahasak.connect.application.IntentProvider;
import com.rahasak.connect.pojo.NotificationMessage;
import com.rahasak.connect.util.PreferenceUtil;

public class PromizeQrCodeActivity extends BaseActivity {

    private static final String TAG = PromizeQrCodeActivity.class.getName();

    private String amount;
    private TextView msg;

    private BroadcastReceiver msgReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.hasExtra("NOTIFICATION_MESSAGE")) {
                NotificationMessage msg = intent.getExtras().getParcelable("NOTIFICATION_MESSAGE");
                handleNotifiationMessage(msg);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qr_code_generate_activity);
        msg = (TextView) findViewById(R.id.scan_text);
        msg.setTypeface(typeface, Typeface.BOLD);

        initUi();
        initToolbar();
        initActionBar();
        initQrCodeContent();
    }

    @Override
    public void onResume() {
        super.onResume();
        registerReceiver(msgReceiver, new IntentFilter(IntentProvider.ACTION_SENZ));
    }

    @Override
    public void onPause() {
        super.onPause();
        if (msgReceiver != null) unregisterReceiver(msgReceiver);
    }

    private void initUi() {
        Button button = (Button) findViewById(R.id.phonebook);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO save
                Intent intent = new Intent(PromizeQrCodeActivity.this, SendPromizeInfoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
                finish();
            }
        });

    }

    private void initQrCodeContent() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            amount = bundle.getString("EXTRA");
            String qrtext = PreferenceUtil.get(this, PreferenceUtil.ACCOUNT_ID) +
                    ":" + PreferenceUtil.get(this, PreferenceUtil.ACCOUNT_NAME) +
                    ":" + amount;
            generateQrCode(qrtext);
        }

        String cur = String.format("%,.2f", Double.parseDouble(amount));
        msg.setText("Let your Friend to scan this QR code to complete Promize Transaction of Rs " + cur);
    }

    private void generateQrCode(String qrCodeContent) {
        QRCodeWriter writer = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = writer.encode(qrCodeContent, BarcodeFormat.QR_CODE, 512, 512);
            int width = bitMatrix.getWidth();
            int height = bitMatrix.getHeight();
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    bmp.setPixel(x, y, bitMatrix.get(x, y) ? Color.BLACK : Color.WHITE);
                }
            }

            ((ImageView) findViewById(R.id.qr_code)).setImageBitmap(bmp);
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    /**
     * Initialize action bar
     */
    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Send Promize");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    /**
     * Handle broadcast message receives
     * Need to handle registration success failure here
     *
     * @param msg
     */
    private void handleNotifiationMessage(NotificationMessage msg) {
        // show promize confirm
        Intent intent = new Intent(this, SendPromizeInfoActivity.class);
        intent.putExtra("NOTIFICATION_MESSAGE", msg);
        startActivity(intent);
        overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
        finish();
    }

}
