package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.pojo.TokenReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class SaltConfirmActivity extends BaseActivity implements IContractExecutorListener {

    private EditText saltEditText;
    private int retry = 0;

    private Account account;
    private String token;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.salt_confirm_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        account = getIntent().getParcelableExtra("ACCOUNT");
        token = getIntent().getStringExtra("TOKEN");
    }

    private void initUi() {
        saltEditText = (EditText) findViewById(R.id.salt);
        saltEditText.setTypeface(typeface, Typeface.NORMAL);

        Button yes = (Button) findViewById(R.id.register_btn);
        yes.setTypeface(typeface, Typeface.BOLD);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (retry >= 3) {
                    displayInformationMessageDialogConfirm("Error", "Maximum no of retry attempts exceeded", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SaltConfirmActivity.this.finish();
                        }
                    });
                } else {
                    ActivityUtil.hideSoftKeyboard(SaltConfirmActivity.this);
                    if (NetworkUtil.isAvailableNetwork(SaltConfirmActivity.this)) {
                        confirmSalt();
                    } else {
                        Toast.makeText(SaltConfirmActivity.this, "No network connection", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Confirm account");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void confirmSalt() {
        try {
            String salt = saltEditText.getText().toString().trim();

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "activate");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("accountSalt", salt);

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, token);
            retry++;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                if (response.getStatus() == 200) {
                    TokenReply tokenReply = JsonUtil.toTokenReply(response.getPayload());
                    Toast.makeText(this, "Your account has been verified", Toast.LENGTH_LONG).show();
                    regDone(tokenReply.getToken());
                } else {
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    ActivityUtil.cancelProgressDialog();
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Error while activating the account.");
        }
    }

    private void regDone(String token) {
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_ID, account.getId());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PASSWORD, account.getPassword());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NIC, account.getNic());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NAME, account.getName());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_PHONE, account.getPhone());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_NO, account.getNo());
        PreferenceUtil.put(this, PreferenceUtil.ACCOUNT_STATE, "VERIFIED");
        PreferenceUtil.put(this, PreferenceUtil.TOKEN, token);

        navigateToQuestionInfo();
    }

    private void navigateToHome() {
        Intent intent = new Intent(this, HomeActivity.class);
        this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

    private void navigateToQuestionInfo() {
        Intent intent = new Intent(this, RegistrationQuestionInfoActivity.class);
        this.startActivity(intent);
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        finishAffinity();
    }

}

