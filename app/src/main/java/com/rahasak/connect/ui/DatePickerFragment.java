package com.rahasak.connect.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;

public class DatePickerFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog d = new DatePickerDialog(getActivity(), (DatePickerDialog.OnDateSetListener) getActivity(), year, month, day);
        DatePicker dp = d.getDatePicker();
        c.add(Calendar.YEAR,-15);
        dp.setMaxDate(c.getTimeInMillis()); //set min age limit to 18
        c.add(Calendar.YEAR,-83);
        dp.setMinDate(c.getTimeInMillis()); //set max age limit to 100
        return d;
    }
}
