package com.rahasak.connect.ui;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.pojo.BankAccount;

/**
 * Activity class that handles login
 *
 * @author erangaeb@gmail.com (eranga herath)
 */
public class AccountDetailsActivity extends BaseActivity {

    // UI fields
    private TextView holderNameText;
    private TextView accountNumberText;
    private TextView aBalanceText;
    private TextView cBalanceText;
    private TextView accountTypeText;
    private EditText holderName;
    private EditText accountNumber;
    private EditText aBalance;
    private EditText cBalance;
    private EditText accountType;

    private BankAccount bankAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_details_activity);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        bankAccount = getIntent().getParcelableExtra("BANK_ACCOUNT");
    }

    /**
     * Initialize UI components,
     * Set country code text
     * set custom font for UI fields
     */
    private void initUi() {
        holderNameText = (TextView) findViewById(R.id.holder_name_text);
        accountNumberText = (TextView) findViewById(R.id.account_number_text);
        aBalanceText = (TextView) findViewById(R.id.abalance_text);
        cBalanceText = (TextView) findViewById(R.id.cbalance_text);
        accountTypeText = (TextView) findViewById(R.id.account_type_text);

        holderName = findViewById(R.id.holder_name);
        accountNumber = findViewById(R.id.account_number);
        aBalance = findViewById(R.id.abalance);
        cBalance = findViewById(R.id.cbalance);
        accountType = findViewById(R.id.account_type);

        holderNameText.setTypeface(typeface, Typeface.NORMAL);
        accountNumberText.setTypeface(typeface, Typeface.NORMAL);
        aBalanceText.setTypeface(typeface, Typeface.NORMAL);
        cBalanceText.setTypeface(typeface, Typeface.NORMAL);
        accountTypeText.setTypeface(typeface, Typeface.NORMAL);

        holderName.setTypeface(typeface, Typeface.NORMAL);
        accountNumber.setTypeface(typeface, Typeface.NORMAL);
        aBalance.setTypeface(typeface, Typeface.NORMAL);
        cBalance.setTypeface(typeface, Typeface.NORMAL);
        accountType.setTypeface(typeface, Typeface.NORMAL);

        holderName.setText(bankAccount.getName());
        accountNumber.setText(bankAccount.getNo());
        if(bankAccount.getAvailableBalance().isEmpty()) {
            aBalanceText.setVisibility(View.GONE);
            aBalance.setVisibility(View.GONE);
        } else {
            aBalanceText.setVisibility(View.VISIBLE);
            aBalance.setVisibility(View.VISIBLE);
            aBalance.setText("Rs " + bankAccount.getAvailableBalance());
        }
        cBalance.setText("Rs " + bankAccount.getCurrentBalance());
        if (bankAccount.isPromizeAccount())
            accountType.setText(bankAccount.getTyp() + ", " + "Promize account");
        else
            accountType.setText(bankAccount.getTyp());
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Account details");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

}

