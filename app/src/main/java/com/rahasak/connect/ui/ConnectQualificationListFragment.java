package com.rahasak.connect.ui;

import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Qualification;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.Transaction;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class ConnectQualificationListFragment extends Fragment implements AdapterView.OnItemClickListener, IContractExecutorListener {

    private ArrayList<Transaction> promizeList = new ArrayList<>();
    private ConnectQualificationListAdapter adapter;
    private ListView listView;
    private RelativeLayout emptyView;
    private TextView emptyText;
    private boolean transLoaded = false;

    protected Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connect_list_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initListView(view);
        initToolbar(view);

        fetchQualifications();
        //mockList();
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        if (SenzApplication.isRefreshPromize()) {
//            fetchQualifications();
//        }
//    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser && !transLoaded) {
//            transLoaded = true;
//            fetchQualifications();
//        }
//    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (!hidden) {
            if (SenzApplication.isRefreshPromize()) {
                fetchQualifications();
            }
        }
    }

    private void initToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        View header = getLayoutInflater().inflate(R.layout.profile_header, null);
        TextView title = (TextView) header.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Qualifications");

        ImageView backImageView = (ImageView) header.findViewById(R.id.back_btn);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // exit
            }
        });
        toolbar.addView(header);
    }

    private void initListView(View view) {
        listView = (ListView) view.findViewById(R.id.cheque_list_view);
        listView.setOnItemClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);

        promizeList = new ArrayList<>();
        adapter = new ConnectQualificationListAdapter(getActivity(), promizeList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    private void refreshView() {
        // transactions
        if (promizeList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            listView.setEmptyView(emptyView);
            emptyText.setText("No qualifications found with your account.");
        } else {
            emptyView.setVisibility(View.GONE);
            adapter = new ConnectQualificationListAdapter(getActivity(), promizeList);
            adapter.notifyDataSetChanged();
            listView.setAdapter(adapter);
        }
    }

    private void fetchQualifications() {
        if (SenzApplication.isLogin()) {
            try {
                String did = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.DID);
                String owner = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.OWNER);

                HashMap<String, Object> createMap = new HashMap<>();
                createMap.put("id", did + System.currentTimeMillis());
                createMap.put("execer", did + ":" + owner);
                createMap.put("messageType", "getQualifications");
                createMap.put("qualificationHolder", did);

                String digis = CryptoUtil.getDigitalSignature(JsonUtil.toOrderedString(createMap), CryptoUtil.getPrivateKey(getActivity()));
                createMap.put("msgsig", digis);

                ActivityUtil.showProgressDialog(getActivity(), "Fetching qualifications...");
                AccountContractExecutor task = new AccountContractExecutor(createMap, this);
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.QUALIFICATION_API, SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.TOKEN));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //final Transaction transaction = promizeList.get(position);

        //Intent intent = new Intent(this.getActivity(), PromizeDetailsActivity.class);
        //intent.putExtra("TRANSACTION", transaction);
        //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        //startActivity(intent);
        //getActivity().overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response != null && response.getStatus() == 200) {
                Qualification qualification = JsonUtil.toQualificationResponse(response.getPayload());
                //ArrayList<Transaction> list = JsonUtil.toActivityResponse(response.getPayload());
                promizeList = toTransactions(qualification);
                refreshView();
                SenzApplication.setRefreshPromize(false);
            } else {
                ActivityUtil.cancelProgressDialog();
                Toast.makeText(getActivity(), "Fail to fetch qualifications", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(getActivity(), "Fail to fetch qualifications", Toast.LENGTH_LONG).show();
        }
    }

    private ArrayList<Transaction> toTransactions(Qualification qualification) {
        ArrayList<Transaction> transactions = new ArrayList<>();
        final Transaction transaction1 = new Transaction();
        transaction1.setUser("Educational Qualification");
        transaction1.setDate(qualification.getTimestamp().split(" ")[0]);
        transaction1.setAmount("Ordinary Level Exam");
        transaction1.setDescription(qualification.getStatus());
        transaction1.setStatus("1");

        final Transaction transaction2 = new Transaction();
        transaction2.setUser("Diploma Qualification");
        transaction2.setDate(qualification.getTimestamp().split(" ")[0]);
        transaction2.setAmount(qualification.getNameDiploma());
        transaction2.setDescription(qualification.getStatus());
        transaction2.setStatus("2");

        final Transaction transaction3 = new Transaction();
        transaction3.setUser("Professional Qualification");
        transaction3.setDate(qualification.getTimestamp().split(" ")[0]);
        transaction3.setAmount(qualification.getNameExperience());
        transaction3.setDescription(qualification.getStatus());
        transaction3.setStatus("3");

        transactions.add(transaction1);
        transactions.add(transaction2);
        transactions.add(transaction3);

        return transactions;
    }

    private void mockList() {
        ArrayList<Transaction> transactions = new ArrayList<>();
        final Transaction transaction1 = new Transaction();
        transaction1.setUser("Educational Qualification");
        transaction1.setDate("2020/06/01");
        transaction1.setAmount("Ordinary Level Exam");
        transaction1.setDescription("Verified");
        transaction1.setStatus("1");

        final Transaction transaction2 = new Transaction();
        transaction2.setUser("Educational Qualification");
        transaction2.setDate("2020/06/02");
        transaction2.setAmount("NVQ Level 1");
        transaction2.setDescription("Verified");
        transaction2.setStatus("2");

        final Transaction transaction3 = new Transaction();
        transaction3.setUser("Professional Qualification");
        transaction3.setDate("2020/06/02");
        transaction3.setAmount("Sports Instructor");
        transaction3.setDescription("Pending");
        transaction3.setStatus("3");

        transactions.add(transaction1);
        transactions.add(transaction2);
        transactions.add(transaction3);

        promizeList = transactions;

        refreshView();
    }
}
