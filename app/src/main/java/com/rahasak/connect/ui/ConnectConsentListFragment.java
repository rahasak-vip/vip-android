package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.Transaction;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class ConnectConsentListFragment extends BaseFragment implements AdapterView.OnItemClickListener, IContractExecutorListener {

    private ArrayList<Transaction> transferList;
    private ConnectConsentListAdapter adapter;
    private ListView listView;
    private RelativeLayout emptyView;
    private TextView emptyText;
    private FloatingActionButton add;
    private Account account;

    protected Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connect_list_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        init(view);
        initListView(view);
        //fetchConsents("0", "20");
        mockList();
    }

    private void init(View view) {
        add = view.findViewById(R.id.send_promize);
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // navigate to
            }
        });
        add.setVisibility(View.GONE);
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        if (SenzApplication.isRefreshConsents()) {
//            fetchConsents("0", "20");
//        }
//    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            // fragment will show
            if (SenzApplication.isRefreshConsents()) {
                fetchConsents("0", "20");
            }
        }
    }

    private void initListView(View view) {
        listView = (ListView) view.findViewById(R.id.cheque_list_view);
        listView.setOnItemClickListener(this);

        emptyView = (RelativeLayout) view.findViewById(R.id.empty_view);
        emptyText = (TextView) view.findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);

        transferList = new ArrayList<>();
        adapter = new ConnectConsentListAdapter(getActivity(), transferList);
        adapter.notifyDataSetChanged();
        listView.setAdapter(adapter);
    }

    private void refreshView(ArrayList<Transaction> list) {
        // transactions
        //transferList.addAll(list);
        transferList = list;
        if (transferList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            listView.setEmptyView(emptyView);
            emptyText.setText("No consent records found with your account.");
        } else {
            emptyView.setVisibility(View.GONE);
            adapter = new ConnectConsentListAdapter(getActivity(), transferList);
            adapter.notifyDataSetChanged();
            listView.setAdapter(adapter);
        }
    }

    private void fetchConsents(String offset, String limit) {
        if (SenzApplication.isLogin()) {
            try {
                account = PreferenceUtil.getAccount(this.getActivity());
                String did = PreferenceUtil.get(getActivity(), PreferenceUtil.DID);
                String owner = PreferenceUtil.get(getActivity(), PreferenceUtil.OWNER);

                HashMap<String, Object> createMap = new HashMap<>();
                createMap.put("id", account.getId() + System.currentTimeMillis());
                createMap.put("execer", did + ":" + owner);
                createMap.put("messageType", "searchConsent");
                createMap.put("did", did);
                createMap.put("owner", owner);
                createMap.put("nameTerm", "");
                createMap.put("idTerm", "");
                createMap.put("didTerm", "");
                createMap.put("nic", "");
                createMap.put("phone", "");
                createMap.put("offset", offset);
                createMap.put("limit", limit);
                createMap.put("sort", "descending");

                SenzApplication.setRefreshConsents(false);

                ActivityUtil.showProgressDialog(getActivity(), "Fetching consents...");
                AccountContractExecutor task = new AccountContractExecutor(createMap, this);
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRACE_API, PreferenceUtil.get(getActivity(), PreferenceUtil.TOKEN));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void allowPermission(Transaction transaction) {
        try {
            String did = PreferenceUtil.get(getActivity(), PreferenceUtil.DID);
            String owner = PreferenceUtil.get(getActivity(), PreferenceUtil.OWNER);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "traceConfirm");
            createMap.put("execer", did + ":" + owner);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("traceId", transaction.getTraceId());
            createMap.put("accountDid", did);
            createMap.put("accountOwnerDid", owner);
            createMap.put("tracerDid", transaction.getTracerDid());
            createMap.put("tracerOwnerDid", transaction.getTracerDid());
            createMap.put("salt", "23112");

            ActivityUtil.showProgressDialog(getActivity(), "Updating permission...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRACE_API, PreferenceUtil.get(getActivity(), PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Transaction transaction = transferList.get(position);

//        if (!transaction.getDescription().equalsIgnoreCase("allowed")) {
//            String msg = transaction.getUser() + " wants to access your account details? Do you want to allow access ?";
//            displayConfirmationMessageDialog("Allow access", msg, new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    // request to fetch consents
//                    allowPermission(transaction);
//                }
//            });
//        }

        Intent intent = new Intent(this.getActivity(), TestResultActivity.class);
        intent.putExtra("TRANSACTION", transaction);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.bottom_in, R.anim.stay_in);
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response != null && response.getStatus() == 200) {
                if (response.getPayload().contains("consents")) {
                    // consent reply
                    ArrayList<Transaction> list = JsonUtil.toConsentResponse(response.getPayload());
                    refreshView(list);
                    SenzApplication.setRefreshConsents(false);
                } else {
                    // status reply
                    // refresh consents
                    fetchConsents("0", "10");
                }
            } else {
                Toast.makeText(getActivity(), "Fail to process request", Toast.LENGTH_LONG).show();
            }
            ActivityUtil.cancelProgressDialog();
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(getActivity(), "Fail to process request", Toast.LENGTH_LONG).show();
        }
    }

    private void mockList() {
        // add sample list items
        ArrayList<Transaction> transactions = new ArrayList<>();
        //for (int i = 0; i < 10; i++) {
            final Transaction transaction1 = new Transaction();
            transaction1.setUser("Covid test");
            transaction1.setAmount("Nawaloka hospital, colombo 03");
            transaction1.setDate("2020/06/01");
            transaction1.setDescription("Negative");

            final Transaction transaction2 = new Transaction();
            transaction2.setUser("Urin test");
            transaction2.setAmount("Nugegoda, colombo 05");
            transaction2.setDate("2020/06/02");
            transaction2.setDescription("Negative");

            final Transaction transaction3 = new Transaction();
            transaction3.setUser("Blood test");
            transaction3.setAmount("Nugegoda, colombo 05");
            transaction3.setDate("2020/06/02");
            transaction3.setDescription("Mild");

            transactions.add(transaction1);
            transactions.add(transaction2);
            transactions.add(transaction3);
        //}

        refreshView(transactions);
    }
}
