package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.Bank;
import com.rahasak.connect.pojo.Branch;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class BranchListActivity extends BaseActivity implements IContractExecutorListener {

    private LinearLayout search;
    private EditText searchView;
    private ListView bankListView;
    private BranchListAdapter adapter;
    private static ArrayList<Branch> branchList = new ArrayList<>();
    private Bank bank;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts_list);

        setupToolbar();
        setupActionBar();
        setupSearchView();
        initPrefs();
        initList();
        getBranches();
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void setupActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Choose branch");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setupSearchView() {
        search = (LinearLayout) findViewById(R.id.search_view);
        searchView = (EditText) findViewById(R.id.inputSearch);
        searchView.setTypeface(typeface);
        searchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapter.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void initPrefs() {
        if (getIntent().hasExtra("ACCOUNT_BANK")) {
            this.bank = getIntent().getParcelableExtra("ACCOUNT_BANK");
        }
    }

    private void initList() {
        bankListView = (ListView) findViewById(R.id.contacts_list);
        bankListView.setTextFilterEnabled(true);

        // click listener
        bankListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                final Branch branch = (Branch) adapter.getItem(position);
                onSelectBranch(branch);
            }
        });
    }

    private void refreshListView(ArrayList<Branch> branches) {
        branchList = branches;
        Collections.sort(branchList, new Comparator<Branch>() {
            public int compare(Branch o1, Branch o2) {
                return o1.getBranchName().compareTo(o2.getBranchName());
            }
        });

        if (branchList.size() > 0) {
            adapter = new BranchListAdapter(this, branchList);
            bankListView.setAdapter(adapter);
            adapter.notifyDataSetChanged();

            search.setVisibility(View.VISIBLE);
        } else {
            search.setVisibility(View.GONE);
        }
    }

    private void onSelectBranch(final Branch branch) {
        Intent intent = new Intent(BranchListActivity.this, FundTransferCeftActivity.class);
        intent.putExtra("ACCOUNT_BANK", bank);
        intent.putExtra("ACCOUNT_BRANCH", branch);
        startActivity(intent);
        BranchListActivity.this.finish();
    }

    private void getBranches() {
        try {
            Account account = PreferenceUtil.getAccount(this);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "getBranches");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("bankCode", bank.getBankCode());

            ActivityUtil.showProgressDialog(this, "Fetching branch details...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.TRANSFER_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                Toast.makeText(this, "Something went wrong while connecting", Toast.LENGTH_LONG).show();
            } else {
                if (response.getStatus() == 200) {
                    refreshListView(JsonUtil.toBranchList(response.getPayload()));
                } else {
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    ActivityUtil.cancelProgressDialog();
                    Toast.makeText(this, "Failed to fetch branch details", Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(this, "Failed to fetch branch details", Toast.LENGTH_LONG).show();
        }
    }

}
