package com.rahasak.connect.ui;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.enums.CustomerActionType;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Account;
import com.rahasak.connect.pojo.BankAccount;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class AccountListActivity extends BaseActivity implements AdapterView.OnItemClickListener, IContractExecutorListener {

    private ArrayList<BankAccount> accountList;
    private AccountListAdapter accountListAdapter;
    private RelativeLayout emptyView;
    private TextView emptyText;
    private ListView accountListView;

    private CustomerActionType actionType = CustomerActionType.CUSTOMER_LIST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_list_layout);

        initToolbar();
        initActionBar();
        initNewButton();
        initListView();
        fetchAccounts();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Choose account");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initNewButton() {
        // new
        FloatingActionButton newCustomer = (FloatingActionButton) findViewById(R.id.new_account);
        newCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // navigate to contact list
                Intent intent = new Intent(AccountListActivity.this, PaymentTypeActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initSearchView() {
        LinearLayout search = (LinearLayout) findViewById(R.id.search_layout);
        EditText searchView = (EditText) findViewById(R.id.inputSearch);
        if (accountList.size() == 0) {
            search.setVisibility(View.GONE);
        } else {
            search.setVisibility(View.VISIBLE);
            searchView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    //paymentListAdapter.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    private void initListView() {
        accountListView = (ListView) findViewById(R.id.account_list_view);
        accountListView.setOnItemClickListener(this);

        emptyView = (RelativeLayout) findViewById(R.id.empty_view);
        emptyText = (TextView) findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);
    }

    private void mockList() {
        BankAccount acc = new BankAccount();
        acc.setNo("23232323");
        acc.setAvailableBalance("4223");
        acc.setTyp("Savings");
        accountList = new ArrayList<>();
        accountList.add(acc);
        accountList.add(acc);
        accountList.add(acc);
        if (accountList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            accountListView.setEmptyView(emptyView);
        } else {
            emptyView.setVisibility(View.GONE);
        }

        accountListAdapter = new AccountListAdapter(this, accountList);
        accountListView.setAdapter(accountListAdapter);
        accountListAdapter.notifyDataSetChanged();
    }

    private void refreshView(ArrayList<BankAccount> accounts) {
        // transactions
        if (accounts.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            accountListView.setEmptyView(emptyView);
            emptyText.setText("No account found. Please contact bank administrator");
        } else {
            emptyView.setVisibility(View.GONE);

            accountList = accounts;
            accountListAdapter = new AccountListAdapter(this, accountList);
            accountListView.setAdapter(accountListAdapter);
            accountListAdapter.notifyDataSetChanged();

            //initSearchView();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final BankAccount account = accountList.get(position);

        if (account.getTyp().equalsIgnoreCase("Savings")) {
            if (account.isPromizeAccount()) {
                displayInformationMessageDialog("Cannot update", account.getNo() + " is your current promize account. Please select a different savings account.");
            } else {
                if (NetworkUtil.isAvailableNetwork(AccountListActivity.this)) {
                    displayConfirmationMessageDialog("Update account", "Do you like to change your promize account to " + account.getNo(), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            askPassword(account);
                        }
                    });
                } else {
                    Toast.makeText(AccountListActivity.this, "No network connection", Toast.LENGTH_LONG).show();
                }
            }
        } else {
            displayInformationMessageDialog("Cannot update", "Please select a Savings account as your PromiZe account.");
        }
    }

    public void askPassword(final BankAccount bankAccount) {
        final Dialog dialog = new Dialog(this);

        // set layout for dialog
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.input_password_dialog_layout);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(true);

        // texts
        TextView title = (TextView) dialog.findViewById(R.id.title);
        final EditText password = (EditText) dialog.findViewById(R.id.password);
        title.setTypeface(typeface, Typeface.BOLD);
        password.setTypeface(typeface, Typeface.NORMAL);

        // set ok button
        Button done = (Button) dialog.findViewById(R.id.done);
        done.setTypeface(typeface, Typeface.BOLD);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    String givenPassword = password.getText().toString().trim();
                    String hashPass = CryptoUtil.hashSha256(givenPassword);
                    if (hashPass.equalsIgnoreCase(PreferenceUtil.getAccount(AccountListActivity.this).getPassword())) {
                        dialog.cancel();
                        ActivityUtil.hideSoftKeyboard(AccountListActivity.this);
                        updateAccountNo(bankAccount);
                    } else {
                        Toast.makeText(AccountListActivity.this, "Password is Incorrect", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Toast.makeText(AccountListActivity.this, "Password is Incorrect", Toast.LENGTH_LONG).show();
                }
            }
        });

        // cancel button
        Button cancel = (Button) dialog.findViewById(R.id.cancel);
        cancel.setTypeface(typeface, Typeface.BOLD);
        cancel.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void fetchAccounts() {
        try {
            Account account = PreferenceUtil.getAccount(this);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "getAccounts");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());

            ActivityUtil.showProgressDialog(this, "Fetching accounts...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void updateAccountNo(BankAccount bankAccount) {
        try {
            Account account = PreferenceUtil.getAccount(this);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "updateAccountNo");
            createMap.put("execer", account.getId());
            createMap.put("id", account.getId() + System.currentTimeMillis());
            createMap.put("accountId", account.getId());
            createMap.put("accountNo", bankAccount.getNo());

            ActivityUtil.showProgressDialog(this, "Updating account...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, PreferenceUtil.get(this, PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {

    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response != null && response.getStatus() == 200) {
                if (response.getPayload().contains("Updated")) {
                    // account updated
                    displayInformationMessageDialogConfirm("Account updated", "Your PromiZe account is changed successfully.", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            SenzApplication.setRefreshWallet(true);
                            SenzApplication.setRefreshSettings(true);
                            AccountListActivity.this.finish();
                        }
                    });
                } else {
                    // fetched account list
                    ArrayList<BankAccount> accounts = JsonUtil.toAccountResponse(response.getPayload()).getAccounts();
                    refreshView(accounts);
                }
            } else {
                ActivityUtil.cancelProgressDialog();
                Toast.makeText(this, "Something went wrong while request", Toast.LENGTH_LONG).show();
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(this, "Something went wrong while request", Toast.LENGTH_LONG).show();
        }
    }


}
