package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.rahasak.connect.R;
import com.rahasak.connect.db.BillerSource;
import com.rahasak.connect.enums.CustomerActionType;
import com.rahasak.connect.pojo.Biller;

import java.util.ArrayList;

public class PaymentListActivity extends BaseActivity implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ArrayList<Biller> billerList;
    private PaymentListAdapter paymentListAdapter;
    private RelativeLayout emptyView;
    private ListView accountListView;

    private CustomerActionType actionType = CustomerActionType.CUSTOMER_LIST;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.payment_list_layout);

        initToolbar();
        initActionBar();
        initNewButton();
        initListView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshList();
        initSearchView();
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Make payment");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initNewButton() {
        // new
        FloatingActionButton newCustomer = (FloatingActionButton) findViewById(R.id.new_account);
        newCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // navigate to contact list
                Intent intent = new Intent(PaymentListActivity.this, PaymentTypeActivity.class);
                startActivity(intent);
            }
        });
    }

    private void initSearchView() {
        LinearLayout search = (LinearLayout) findViewById(R.id.search_layout);
        EditText searchView = (EditText) findViewById(R.id.inputSearch);
        if (billerList.size() == 0) {
            search.setVisibility(View.GONE);
        } else {
            search.setVisibility(View.VISIBLE);
            searchView.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    paymentListAdapter.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
        }
    }

    private void initListView() {
        accountListView = (ListView) findViewById(R.id.customer_list_view);
        accountListView.setOnItemClickListener(this);
        accountListView.setOnItemLongClickListener(this);

        emptyView = (RelativeLayout) findViewById(R.id.empty_view);
        TextView emptyText = (TextView) findViewById(R.id.empty_view_text);
        emptyText.setTypeface(typeface, Typeface.NORMAL);
    }

    private void refreshList() {
        billerList = BillerSource.getBillers(this);
        if (billerList.size() == 0) {
            emptyView.setVisibility(View.VISIBLE);
            accountListView.setEmptyView(emptyView);
        } else {
            emptyView.setVisibility(View.GONE);
        }

        paymentListAdapter = new PaymentListAdapter(this, billerList);
        accountListView.setAdapter(paymentListAdapter);
        paymentListAdapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        final Biller biller = billerList.get(position);

        // TODO navigate to func trans
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
        if (actionType == CustomerActionType.CUSTOMER_LIST) {
            final Biller biller = billerList.get(position);
            displayConfirmationMessageDialog("Confirm", "Are you sure your want to remove the biller?", new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // delete item
                    billerList.remove(position);
                    paymentListAdapter.notifyDataSetChanged();

                    // TODO delete from db
                }
            });
        }

        return true;
    }

}
