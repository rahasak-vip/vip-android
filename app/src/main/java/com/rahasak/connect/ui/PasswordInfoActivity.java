package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.exceptions.InvalidInputFieldsException;
import com.rahasak.connect.exceptions.InvalidPasswordException;
import com.rahasak.connect.exceptions.MisMatchFieldException;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.pojo.TokenReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.NetworkUtil;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

import org.json.JSONException;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;

public class PasswordInfoActivity extends BaseActivity implements IContractExecutorListener {

    // ui controls
    private Button registerBtn;
    private EditText editTextPassword;
    private EditText editTextConfirmPassword;
    private TextView termsText;
    private TextView termsLink;

    private String givenPassword;
    private Identity identity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_password_info_layout);

        initPrefs();
        initUi();
        initToolbar();
        initActionBar();
    }

    private void initPrefs() {
        this.identity = getIntent().getParcelableExtra("IDENTITY");
    }

    private void initActionBar() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.profile_header, null));
        getSupportActionBar().setDisplayOptions(android.support.v7.app.ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setDisplayShowCustomEnabled(true);

        // title
        TextView titleText = (TextView) findViewById(R.id.title);
        titleText.setTypeface(typeface, Typeface.BOLD);
        titleText.setText("Password");

        // back button
        ImageView backBtn = (ImageView) findViewById(R.id.back_btn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void initToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setCollapsible(false);
        toolbar.setOverScrollMode(Toolbar.OVER_SCROLL_NEVER);
        setSupportActionBar(toolbar);
    }

    private void initUi() {
        editTextPassword = (EditText) findViewById(R.id.password);
        editTextConfirmPassword = (EditText) findViewById(R.id.confirm_password);
        termsText = (TextView) findViewById(R.id.terms_text);
        termsLink = (TextView) findViewById(R.id.terms_link);
        termsLink.setPaintFlags(termsLink.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        editTextPassword.setTypeface(typeface, Typeface.NORMAL);
        editTextConfirmPassword.setTypeface(typeface, Typeface.NORMAL);
        termsText.setTypeface(typeface, Typeface.BOLD);
        termsLink.setTypeface(typeface, Typeface.BOLD);

        registerBtn = (Button) findViewById(R.id.register_btn);
        registerBtn.setTypeface(typeface, Typeface.BOLD);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                onClickRegister();
            }
        });
//        TODO : remove term and condition or update
        termsLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTerms();
            }
        });
    }

    private void onClickRegister() {
        ActivityUtil.hideSoftKeyboard(this);

        // crate account
        final String password = editTextPassword.getText().toString().trim();
        final String confirmPassword = editTextConfirmPassword.getText().toString().trim();

        try {
            ActivityUtil.validatePasswordInfo(password, confirmPassword);

            // generate key pair
            CryptoUtil.initKeys(PasswordInfoActivity.this);

            // save hashed password instead plain password
            final String passwordHash = CryptoUtil.hashSha256(password);
            identity.setPassword(passwordHash);
            givenPassword = password;

            String confirmationMessage = "<font color=#636363>Please confirm to register with NIC : </font> <font color=#24c69f>" + "<b>" + identity.getDid() + "</b>" + "</font> <font color=#636363> in Sportificate </font> ";
            displayConfirmationMessageDialog("Confirm", confirmationMessage, new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (NetworkUtil.isAvailableNetwork(PasswordInfoActivity.this)) {
                        doReg(identity);
                    } else {
                        Toast.makeText(PasswordInfoActivity.this, "No network connection", Toast.LENGTH_LONG).show();
                    }
                }
            });
        } catch (InvalidPasswordException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Invalid password. Password should contain minimum 7 characters with special character");
        } catch (MisMatchFieldException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "Your password is not matched with the confirm password");
        } catch (InvalidInputFieldsException e) {
            e.printStackTrace();
            displayInformationMessageDialog("Error", "You need to fill all fields to complete the registration.");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
    }

    private void doReg(Identity identity) {
        try {
            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "create");
            createMap.put("execer", identity.getDid() + ":" + identity.getOwner());
            createMap.put("id", identity.getDid() + System.currentTimeMillis());
            createMap.put("did", identity.getDid());
            createMap.put("owner", identity.getOwner());
            createMap.put("password", identity.getPassword());
            createMap.put("roles", new String[]{"user"});
            createMap.put("groups", new String[]{});
            createMap.put("pubkey", SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.PUBLIC_KEY));
            createMap.put("nic", identity.getNic());
            createMap.put("name", identity.getName());
            createMap.put("age", identity.getAge());
            createMap.put("dob", identity.getDob());
            createMap.put("phone", identity.getPhone());
            createMap.put("email", identity.getEmail());
            createMap.put("province", identity.getProvince());
            createMap.put("district", identity.getDistrict());
            createMap.put("address", identity.getAddress());
            createMap.put("blob", identity.getBlob());

            ActivityUtil.showProgressDialog(this, "Please wait...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void navigateToTerms() {
        Intent intent = new Intent(PasswordInfoActivity.this, TermsOfUseActivity.class);
        PasswordInfoActivity.this.startActivity(intent);
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                displayInformationMessageDialog("Error", "Something went wrong while connecting.");
            } else {
                if (response.getStatus() == 201) {
                    TokenReply tokenReply = JsonUtil.toTokenReply(response.getPayload());
                    Toast.makeText(this, "Your account is registered successfully", Toast.LENGTH_LONG).show();
                    registrationDone(tokenReply);
                } else {
                    ActivityUtil.cancelProgressDialog();
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    displayInformationMessageDialog("Error", statusReply.getMsg());
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            displayInformationMessageDialog("Error", "Something went wrong while registering.");
        }
    }

    private void registrationDone(TokenReply tokenReply) {
        SenzApplication.setLogin(true);
        SenzApplication.startLogoutTimer();

        // save prefs
        SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.DID, identity.getDid());
        SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.ACCOUNT_PASSWORD, givenPassword);
        SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.TOKEN, tokenReply.getToken());

        navigateToNext();
    }

    private void navigateToNext() {
        Intent intent = new Intent(PasswordInfoActivity.this, QualificationQuestionInfoActivity.class);
        PasswordInfoActivity.this.startActivity(intent);
        PasswordInfoActivity.this.finishAffinity();
        overridePendingTransition(R.anim.right_in, R.anim.stay_in);
    }

}
