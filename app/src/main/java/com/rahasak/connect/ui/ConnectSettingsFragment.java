package com.rahasak.connect.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.rahasak.connect.R;
import com.rahasak.connect.application.SenzApplication;
import com.rahasak.connect.async.AccountContractExecutor;
import com.rahasak.connect.interfaces.IContractExecutorListener;
import com.rahasak.connect.pojo.Identity;
import com.rahasak.connect.pojo.Response;
import com.rahasak.connect.pojo.StatusReply;
import com.rahasak.connect.util.ActivityUtil;
import com.rahasak.connect.util.CryptoUtil;
import com.rahasak.connect.util.JsonUtil;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

import org.json.JSONException;

import java.util.HashMap;

public class ConnectSettingsFragment extends BaseFragment implements IContractExecutorListener {

    private static final String TAG = ConnectSettingsFragment.class.getName();

    private TextView nic;
    private TextView nicv;
    private TextView name;
    private TextView namev;
    private TextView phone;
    private TextView phonev;
    private TextView email;
    private TextView emailv;
    private TextView tax;
    private TextView taxv;
    private TextView photo;
    private TextView password;
    private TextView terms;
    private TextView logout;
    private TextView contactDetails;
    private TextView contactNo;

    private Button photoViewBtn;
    private Button photoChangeBtn;
    private Button passChangeBtn;
    private Button passResetBtn;
    private Button termsBtn;
    private Button contact;
    private Button logoutBtn;

    private Typeface typeface;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.connect_setting_fragment_layout, container, false);
        typeface = Typeface.createFromAsset(getActivity().getAssets(), "fonts/GeosansLight.ttf");

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initUi(view);
        initToolbar(view);
        if (SenzApplication.isRefreshWallet()) {
            fetchIdentity();
        } else {
            updateIdentityInfo(SenzApplication.getIdentity());
        }
    }

//    @Override
//    public void onStart() {
//        super.onStart();
//
//        if (SenzApplication.isRefreshWallet()) {
//            fetchIdentity();
//        }
//    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        if (!hidden) {
            // fragment will show
            if (SenzApplication.isRefreshWallet()) {
                fetchIdentity();
            }
        }
    }

    private void initUi(View view) {
        // text views
        nic = (TextView) view.findViewById(R.id.nic);
        nicv = (TextView) view.findViewById(R.id.nicv);
        name = (TextView) view.findViewById(R.id.name);
        namev = (TextView) view.findViewById(R.id.namev);
        phone = (TextView) view.findViewById(R.id.phone);
        phonev = (TextView) view.findViewById(R.id.phonev);
        email = (TextView) view.findViewById(R.id.email);
        emailv = (TextView) view.findViewById(R.id.emailv);
        tax = (TextView) view.findViewById(R.id.tax);
        taxv = (TextView) view.findViewById(R.id.taxv);
        photo = (TextView) view.findViewById(R.id.photo);
        password = (TextView) view.findViewById(R.id.password);
        terms = (TextView) view.findViewById(R.id.terms);
        logout = (TextView) view.findViewById(R.id.logout);
        contactDetails = (TextView) view.findViewById(R.id.contact_details);
        contactNo = (TextView) view.findViewById(R.id.contact_no);

        // set font
        nic.setTypeface(typeface, Typeface.NORMAL);
        nicv.setTypeface(typeface, Typeface.NORMAL);
        name.setTypeface(typeface, Typeface.NORMAL);
        namev.setTypeface(typeface, Typeface.NORMAL);
        phone.setTypeface(typeface, Typeface.NORMAL);
        phonev.setTypeface(typeface, Typeface.NORMAL);
        email.setTypeface(typeface, Typeface.NORMAL);
        emailv.setTypeface(typeface, Typeface.NORMAL);
        tax.setTypeface(typeface, Typeface.NORMAL);
        taxv.setTypeface(typeface, Typeface.NORMAL);
        photo.setTypeface(typeface, Typeface.NORMAL);
        password.setTypeface(typeface, Typeface.NORMAL);
        terms.setTypeface(typeface, Typeface.NORMAL);
        logout.setTypeface(typeface, Typeface.NORMAL);
        contactDetails.setTypeface(typeface, Typeface.NORMAL);
        contactNo.setTypeface(typeface, Typeface.NORMAL);

        photoViewBtn = (Button) view.findViewById(R.id.photo_view_btn);
        photoViewBtn.setTypeface(typeface, Typeface.BOLD);
        photoViewBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToPhotoView();
            }
        });

        photoChangeBtn = (Button) view.findViewById(R.id.photo_change_btn);
        photoChangeBtn.setTypeface(typeface, Typeface.BOLD);
        photoChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //navigateToAccountChange();
            }
        });

        passChangeBtn = (Button) view.findViewById(R.id.pass_change_btn);
        passChangeBtn.setTypeface(typeface, Typeface.BOLD);
        passChangeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //navigateToPasswordChange();
            }
        });

        passResetBtn = (Button) view.findViewById(R.id.pass_reset_btn);
        passResetBtn.setTypeface(typeface, Typeface.BOLD);
        passResetBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToPasswordReset();
            }
        });

        termsBtn = (Button) view.findViewById(R.id.terms_btn);
        termsBtn.setTypeface(typeface, Typeface.BOLD);
        termsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToTerms();
            }
        });

        contact = (Button) view.findViewById(R.id.contact_button);
        contact.setTypeface(typeface, Typeface.BOLD);
        contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToContactDetails();
            }
        });

        logoutBtn = (Button) view.findViewById(R.id.logout_button);
        logoutBtn.setTypeface(typeface, Typeface.BOLD);
        logoutBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickLogin();
            }
        });
    }

    private void initToolbar(View view) {
        Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);

        View header = getLayoutInflater().inflate(R.layout.profile_header, null);
        TextView title = (TextView) header.findViewById(R.id.title);
        title.setTypeface(typeface, Typeface.BOLD);
        title.setText("Preference");

        ImageView backImageView = (ImageView) header.findViewById(R.id.back_btn);
        backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // exit
            }
        });
        toolbar.addView(header);
    }

    private void fetchIdentity() {
        try {
            String did = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.DID);
            String owner = SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.OWNER);

            HashMap<String, Object> createMap = new HashMap<>();
            createMap.put("messageType", "get");
            createMap.put("execer", did + ":" + owner);
            createMap.put("id", did + System.currentTimeMillis());
            createMap.put("did", did);
            createMap.put("owner", owner);

            String digis = CryptoUtil.getDigitalSignature(JsonUtil.toOrderedString(createMap), CryptoUtil.getPrivateKey(getActivity()));
            createMap.put("msgsig", digis);

            ActivityUtil.showProgressDialog(getActivity(), "Fetching settings...");
            AccountContractExecutor task = new AccountContractExecutor(createMap, this);
            task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, AccountContractExecutor.IDENTITY_API, SecurePreferenceUtil.getInstance(getActivity()).get(PreferenceUtil.TOKEN));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onFinishTask(String response) {
    }

    @Override
    public void onFinishTask(Response response) {
        ActivityUtil.cancelProgressDialog();
        try {
            if (response == null) {
                Toast.makeText(getActivity(), "Something went wrong while connecting", Toast.LENGTH_LONG).show();
            } else {
                if (response.getStatus() == 200) {
                    SenzApplication.setIdentity(JsonUtil.toIdentityResponse(response.getPayload()));
                    updateIdentityInfo(SenzApplication.getIdentity());
                } else {
                    StatusReply statusReply = JsonUtil.toStatusReply(response.getPayload());
                    ActivityUtil.cancelProgressDialog();
                    Toast.makeText(getActivity(), "Failed to fetch identity details", Toast.LENGTH_LONG).show();
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
            ActivityUtil.cancelProgressDialog();
            Toast.makeText(getActivity(), "Failed to fetch identity details", Toast.LENGTH_LONG).show();
        }
    }

    private void updateIdentityInfo(Identity identity) {
        // set ui fields
        nicv.setText(identity.getNic());
        namev.setText(identity.getName());
        phonev.setText(identity.getPhone());
        emailv.setText(identity.getEmail());
        taxv.setText(identity.getDob());
    }

    private void onClickLogin() {
        displayConfirmationMessageDialog("Logout", "Confirm logout from Sportificate?", new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SenzApplication.setLogin(false);
                SenzApplication.cancelLogoutTimer();
                navigateToLogin();
            }
        });
    }

    private void navigateToPhotoView() {
        Intent intent = new Intent(getActivity(), PhotoPreviewActivity.class);
        intent.putExtra("IDENTITY", SenzApplication.getIdentity());
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToPhotoChange() {
        Intent intent = new Intent(getActivity(), AccountListActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToPasswordChange() {
        Intent intent = new Intent(getActivity(), PasswordChangeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToPasswordReset() {
        Intent intent = new Intent(getActivity(), PasswordResetInfoActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToTerms() {
        Intent intent = new Intent(getActivity(), TermsOfUseActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToContactDetails() {
        Intent intent = new Intent(getActivity(), ContactDetailsActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    private void navigateToLogin() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        getActivity().overridePendingTransition(R.anim.right_in, R.anim.stay_in);
        getActivity().finishAffinity();
    }

}
