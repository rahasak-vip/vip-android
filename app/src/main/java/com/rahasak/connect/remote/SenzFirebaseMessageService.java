package com.rahasak.connect.remote;

import android.content.Intent;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.rahasak.connect.R;
import com.rahasak.connect.application.IntentProvider;
import com.rahasak.connect.pojo.ConnectNotificationMessage;
import com.rahasak.connect.pojo.Notifcationz;
import com.rahasak.connect.pojo.NotificationMessage;
import com.rahasak.connect.util.JsonUtil;

public class SenzFirebaseMessageService extends FirebaseMessagingService {

    private static final String TAG = SenzFirebaseMessageService.class.getName();

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            Log.e(TAG, "Data Payload: " + remoteMessage.getData());
            try {
                String dataMsg = remoteMessage.getData().get("message");
                ConnectNotificationMessage notificationMessage = JsonUtil.toConnectNotificationMessage(dataMsg);
                handleNotificationMessage(notificationMessage);
            } catch (Exception e) {
                Log.e(TAG, "Exception: " + e.getMessage());
            }
        }
    }

    private void handleNotificationMessage(ConnectNotificationMessage msg) {
        // send notification
        Notifcationz notificationz = new Notifcationz(R.drawable.mastercard, "Connect", "Request to access identity", msg.getTracerName());
        //NotificationzHandler.notifiyConnect(this, notificationz, msg);

        // broadcast message
        Intent intent = new Intent(IntentProvider.ACTION_SENZ);
        intent.putExtra("NOTIFICATION_MESSAGE", msg);
        sendBroadcast(intent);
    }

}

