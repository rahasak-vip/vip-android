package com.rahasak.connect.remote;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.rahasak.connect.util.PreferenceUtil;
import com.rahasak.connect.util.SecurePreferenceUtil;

public class SenzFirebaseIdService extends FirebaseInstanceIdService {
    private static final String TAG = SenzFirebaseIdService.class.getName();

    @Override
    public void onTokenRefresh() {
        // get updated token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "refreshed token: " + refreshedToken);

        // save firebase token in shared prefs
        if (!SecurePreferenceUtil.getInstance(this).get(PreferenceUtil.FIREBASE_TOKEN).equalsIgnoreCase(refreshedToken)) {
            SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.FIREBASE_TOKEN, refreshedToken);
            SecurePreferenceUtil.getInstance(this).put(PreferenceUtil.UPDATE_FIREBASE_TOKEN, "yes");
        }
    }

}
